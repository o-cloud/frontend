# Open Cloud user front-end

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Overview

This is the main front-end of the solution. It is used by every user. Another front-end exists from administrators.

## Development server

To run a development server you will need a running kubernetes cluster with the rest of the Open Cloud solution installed on it.

You can have your development instance requesting your cluster by modifying the [environment.ts](src/environments/environment.ts) file or by setting up a [proxy](https://angular.io/guide/build#proxying-to-a-backend-server). 

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


### Deploying to your cluster
**Prerequisites**:
* A running kubernetes cluster
* helm
* skaffold

```bash
skaffold run
```

Option *default-repo* need to be added in skaffold command for specify container registry to used.
