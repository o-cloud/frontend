### Stage 1: build ###

FROM node:15-stretch as builder

# Set working directory.
RUN mkdir /app
WORKDIR /app

# Copy app dependencies.
COPY ./package.json ./package-lock.json ./

# Install app dependencies.
RUN npm install

# Prebuild Angular stuff
RUN npm run ngcc

# Copy app files.
COPY . /app

# Build app
RUN npm run build  -- --output-path=./dist/out --prod

### Stage 2: delivery ###

FROM nginx:1.15.7-alpine

# Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# Copy output directory from builder to nginx image.
COPY --from=builder /app/dist/out /usr/share/nginx/html

# Copy nginx configuration file.
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
