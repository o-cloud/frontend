import { Component, ElementRef, OnInit, Output, ViewChild, EventEmitter, OnDestroy, HostListener } from '@angular/core';
import { WorkflowDesignerService } from './workflow-designer.service';
import { Block, BlockConstructor, GhostBlock } from './models/block.model';
import { Arrow } from './models/arrow.model';
import { Link, linkToJson } from './models/link.model';
import { DragTarget } from './models/drag-target.model';
import { WorkflowStoreService } from './store/workflow-store.service';
import { asyncScheduler } from 'rxjs';
import { distinctUntilChanged, throttleTime } from 'rxjs/operators';
import { selectNothing } from './store/workflow-store-action.model';

@Component({
  selector: 'app-workflow-designer',
  templateUrl: './workflow-designer.component.html',
})
export class WorkflowDesignerComponent implements OnInit, OnDestroy {
  blockConstructors: BlockConstructor[] = [];
  blocks: Block[] = [];
  ghostBlock?: GhostBlock;
  ghostArrow?: Arrow = { x1: 50, x2: 75, y1: 50, y2: 100 };
  links: Link[] = [];
  selectedBlock?: Block;
  selectedLink?: Link;
  svgOffsetX = 0;
  svgOffsetY = 0;
  svgScale = 1;
  isDragging = false;
  @ViewChild('container') containerElement?: ElementRef;
  workflowChangeEmitter = new EventEmitter<any>();
  @Output() workflowChange = this.workflowChangeEmitter.pipe(
    distinctUntilChanged((prev, current) => {
      return prev.blocks === current.blocks && prev.links === current.links;
    }),
    throttleTime(5000, asyncScheduler, { leading: false, trailing: true })
  );
  @Output() destroy = new EventEmitter<any>();

  constructor(public workflowDesignerService: WorkflowDesignerService, private store: WorkflowStoreService) {}

  ngOnInit(): void {
    this.store.getState().subscribe((state) => {
      this.isDragging = !!state.dragSource;
      this.workflowChangeEmitter.emit({
        blocks: state.blocks,
        links: state.links.map(linkToJson),
      });
      this.blockConstructors = state.availableBlocks;
      this.blocks = state.blocks;
      this.ghostBlock = state.ghostBlock;
      this.ghostArrow = state.ghostArrow;
      this.links = state.links;
      this.selectedBlock = undefined;
      this.selectedLink = undefined;
      if (state.selectionTarget) {
        switch (state.selectionTarget.kind) {
          case 'link':
            this.selectedLink = state.selectionTarget.link;
            break;
          case 'block':
            this.selectedBlock = state.selectionTarget.block;
            break;
        }
      }
      this.svgOffsetX = state.svgOffsetX;
      this.svgOffsetY = state.svgOffsetY;
      this.svgScale = state.svgScale;
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch(selectNothing())
    this.destroy.emit({
      blocks: this.blocks,
      links: this.links.map(linkToJson),
    });
  }

  @HostListener('window:unload', ['$event'])
  unloadHandler(): void {
    this.ngOnDestroy();
  }

  stopDrag = (target: DragTarget): void => {
    this.containerElement?.nativeElement.focus();
    this.workflowDesignerService.stopDrag(target);
  };
}
