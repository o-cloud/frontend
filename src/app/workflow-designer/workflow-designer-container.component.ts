import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Block, Port } from './models/block.model';
import { DragTarget } from './models/drag-target.model';
import { blockSource, DragSource, portSource, svgContainerSource } from './models/drag-sources.model';
import { Arrow } from './models/arrow.model';
import { Link } from './models/link.model';
import { BLOCK_HEIGHT, BLOCK_WIDTH, PORT_RADIUS } from './constants';

@Component({
  selector: 'app-workflow-designer-container',
  templateUrl: './workflow-designer-container.component.html',
  styleUrls: ['./workflow-designer-container.component.scss'],
})
export class WorkflowDesignerContainerComponent {
  @Output() dragTarget = new EventEmitter<DragTarget>();
  @Output() dragSource = new EventEmitter<DragSource>();

  @Input() ghostArrow?: Arrow;
  @Input() blocks: Block[] = [];
  @Input() links: Link[] = [];
  @Input() selectedBlock?: Block;
  @Input() selectedLink?: Link;
  @Input() offsetX = 0;
  @Input() offsetY = 0;
  @Input() scale = 1;
  portRadius = PORT_RADIUS;
  blockHeight = BLOCK_HEIGHT;
  blockWidth = BLOCK_WIDTH;

  onMouseUp = (e: MouseEvent): void => {
    e.stopPropagation();
    this.dragTarget.emit({ kind: 'svgContainer', event: e });
  };

  onDragContainer = (e: MouseEvent): void => {
    e.stopPropagation();
    this.dragSource.emit(svgContainerSource({ sourceEvent: e }));
  };

  onDropOnPort = (e: MouseEvent, block: Block, x: number, y: number, port: Port): void => {
    e.stopPropagation();
    this.dragTarget.emit({
      kind: 'port',
      block,
      port,
      x,
      y,
    });
  };

  onDropOnBlock = (e: MouseEvent, block: Block): void => {
    e.stopPropagation();
    this.dragTarget.emit({
      kind: 'block',
      event: e,
      block,
    });
  };

  onDropOnLink = (target: DragTarget): void => {
    this.dragTarget.emit(target);
  };

  onDragLink = (source: DragSource): void => {
    this.dragSource.emit(source);
  };

  onDragBlock = (e: MouseEvent, block: Block): void => {
    e.stopPropagation();
    this.dragSource.emit(blockSource({ block, sourceEvent: e }));
  };

  onDragPort = (e: MouseEvent, block: Block, x: number, y: number, port: Port): void => {
    e.stopPropagation();
    this.dragSource.emit(
      portSource({
        sourceEvent: e,
        block,
        port,
        x,
        y,
      })
    );
  };
}
