import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkflowDesignerComponent } from './workflow-designer.component';

const routes: Routes = [
  {
    path: 'workflow-designer',
    component: WorkflowDesignerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkflowDesignerRoutingModule {}
