import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowDesignerContainerComponent } from './workflow-designer-container.component';
import { WorkflowBlockComponent } from './shapes/workflow-block.component';
import { WorkflowDesignerComponent } from './workflow-designer.component';
import { WorkflowDesignerRoutingModule } from './workflow-designer-routing.module';
import { WorkflowCatalogToolbarComponent } from './workflow-catalog-toolbar.component';
import { WorkflowBlockDetailsPanelComponent } from './details-panel/workflow-block-details-panel.component';
import { WorkflowArrowComponent } from './shapes/workflow-arrow.component';
import { WorkFlowGhostArrowComponent } from './shapes/work-flow-ghost-arrow.component';
import { NbAccordionModule, NbCardModule } from '@nebular/theme';
import { HostDirective } from './details-panel/host.directive';
import { BaseDetailsPanelComponent } from './details-panel/base-details-panel-component.component';

@NgModule({
  imports: [CommonModule, WorkflowDesignerRoutingModule, NbAccordionModule, NbCardModule],
  declarations: [
    WorkflowDesignerContainerComponent,
    WorkflowBlockComponent,
    WorkflowDesignerComponent,
    WorkflowCatalogToolbarComponent,
    WorkflowBlockDetailsPanelComponent,
    WorkflowArrowComponent,
    WorkFlowGhostArrowComponent,
    HostDirective,
    BaseDetailsPanelComponent,
  ],
  exports: [WorkflowDesignerComponent],
})
export class WorkflowDesignerModule {}
