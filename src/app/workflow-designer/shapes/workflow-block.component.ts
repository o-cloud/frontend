import { Component, Input, OnInit } from '@angular/core';
import { Block, GhostBlock } from '../models/block.model';
import { BLOCK_HEIGHT, BLOCK_WIDTH } from '../constants';

@Component({
  selector: 'app-workflow-block',
  template: `
    <div
      *ngIf="block || ghostBlock"
      class="block"
      [class.selected]="selected"
      [style.width.px]="blockWidth"
      [style.height.px]="blockHeight"
    >
      <ng-template [ngIf]="block">
        <div>{{ block.name }}</div>
        <div class="caption">{{ block.payload.clusterName}}</div>
      </ng-template>
      <ng-template [ngIf]="ghostBlock">
        <div>{{ ghostBlock.blockConstructor.name }}</div>
        <div class="caption">{{ ghostBlock.blockConstructor.getPayload().clusterName}}</div>
      </ng-template>
    </div>
  `,
  styleUrls: ['./workflow-block.component.scss'],
})
export class WorkflowBlockComponent implements OnInit {
  @Input() block?: Block;
  @Input() ghostBlock?: GhostBlock;
  @Input() selected = false;
  blockWidth = BLOCK_WIDTH;
  blockHeight = BLOCK_HEIGHT;

  ngOnInit(): void {}
}
