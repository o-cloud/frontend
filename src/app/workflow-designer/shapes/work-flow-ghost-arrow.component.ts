import { Component, Input } from '@angular/core';
import { Arrow } from '../models/arrow.model';
import { PORT_RADIUS } from '../constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'g[app-workflow-ghost-arrow]',
  template: `
    <svg:defs>
      <svg:marker
        id="ghost-arrow-head"
        markerWidth="10"
        markerHeight="8"
        refX="6"
        refY="4"
        orient="auto"
        markersUnits="userSpaceOnUse"
      >
        <path d="M 0 0 L 6 4 L 0 8" stroke-width="1" />
      </svg:marker>
    </svg:defs>
    <ng-template [ngIf]="arrow">
      <svg:line
        [attr.x1]="arrow.x1"
        [attr.x2]="arrow.x2"
        [attr.y1]="arrow.y1"
        [attr.y2]="arrow.y2"
        [attr.marker-end]="arrowLength() > portRadius ? 'url(#ghost-arrow-head)' : ''"
      ></svg:line>
    </ng-template>
  `,
})
export class WorkFlowGhostArrowComponent {
  @Input() arrow?: Arrow;
  portRadius = PORT_RADIUS;
  arrowLength = (): number => {
    if (this.arrow) {
      return Math.sqrt(
        Math.pow(Math.abs(this.arrow.x1 - this.arrow.x2), 2) + Math.pow(Math.abs(this.arrow.y1 - this.arrow.y2), 2)
      );
    } else {
      return 0;
    }
  };
}
