import { Component, Input, OnChanges, OnInit, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Link } from '../models/link.model';
import { Point } from '../models/point.model';
import { DragSource, linkSource } from '../models/drag-sources.model';
import { DragTarget } from '../models/drag-target.model';
import { ARROW_POLYGON_HEIGHT, ARROW_POLYGON_WIDTH, BLOCK_HEIGHT, BLOCK_WIDTH, PORT_RADIUS } from '../constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'g[app-workflow-arrow]',
  template: `
    <svg:path [attr.d]="path" (mousedown)="onMouseDown($event)" (mouseup)="onMouseUp($event)"></svg:path>
    <svg:polygon [attr.points]="polygon"></svg:polygon>
    <ng-template [ngIf]="showControlPoint">
      <svg:circle r="5" [attr.cx]="controlPoint1.x" [attr.cy]="controlPoint1.y"></svg:circle>
      <svg:circle r="5" [attr.cx]="controlPoint2.x" [attr.cy]="controlPoint2.y"></svg:circle>
    </ng-template>
  `,
  styleUrls: ['./workflow-arrow.component.scss'],
})
export class WorkflowArrowComponent implements OnInit, OnChanges {
  @Input() link?: Link;
  @Output() dragSource = new EventEmitter<DragSource>();
  @Output() dragTarget = new EventEmitter<DragTarget>();
  showControlPoint = false;
  controlPoint1: Point = { x: 0, y: 0 };
  controlPoint2: Point = { x: 0, y: 0 };
  startPoint: Point = { x: 0, y: 0 };
  endPoint: Point = { x: 0, y: 0 };
  polygon = '';
  path = '';
  minimalControlPointsDistance = 30;

  updateArrow = () => {
    if (!this.link) {
      return;
    }
    const { from, to, portFrom, portTo } = this.link;
    this.startPoint = {
      x: from.x + (BLOCK_WIDTH / (from.outputPorts.length + 1)) * (portFrom.index + 1),
      y: from.y + BLOCK_HEIGHT + PORT_RADIUS,
    };
    this.endPoint = {
      x: to.x + (BLOCK_WIDTH / (to.inputPorts.length + 1)) * (portTo.index + 1),
      y: to.y - ARROW_POLYGON_HEIGHT - PORT_RADIUS,
    };

    let distY = Math.abs((this.endPoint.y - this.startPoint.y) / 2);
    distY = Math.max(distY, this.minimalControlPointsDistance);
    this.controlPoint1 = {
      x: this.startPoint.x,
      y: this.startPoint.y + distY,
    };
    this.controlPoint2 = {
      x: this.endPoint.x,
      y: this.endPoint.y - distY,
    };
    this.path = `M ${this.startPoint.x} ${this.startPoint.y} C ${this.controlPoint1.x} ${this.controlPoint1.y} ${this.controlPoint2.x} ${this.controlPoint2.y} ${this.endPoint.x} ${this.endPoint.y}`;

    const polygonP1 = {
      x: this.endPoint.x,
      y: to.y - ARROW_POLYGON_HEIGHT,
    };
    const polygonP2 = {
      x: this.endPoint.x - ARROW_POLYGON_WIDTH / 2,
      y: this.endPoint.y,
    };
    const polygonP3 = {
      x: this.endPoint.x + ARROW_POLYGON_WIDTH / 2,
      y: this.endPoint.y,
    };
    this.polygon = `${polygonP1.x} ${polygonP1.y}, ${polygonP2.x} ${polygonP2.y}, ${polygonP3.x} ${polygonP3.y}`;
  };

  onMouseDown = (e: MouseEvent) => {
    if (this.link) {
      e.stopPropagation();
      this.dragSource.emit(
        linkSource({
          link: this.link,
          sourceEvent: e,
        })
      );
    }
  };

  onMouseUp = (e: MouseEvent) => {
    if (this.link) {
      e.stopPropagation();
      this.dragTarget.emit({
        kind: 'link',
        link: this.link,
        event: e,
      });
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.link) {
      this.updateArrow();
    }
  }

  ngOnInit(): void {
    this.updateArrow();
  }
}
