import { Block } from './block.model';
import { Link } from './link.model';

export type SelectionTarget = BlockSelectionTarget | LinkSelectionTarget;

export type BlockSelectionTarget = {
  kind: 'block';
  block: Block;
};

export type LinkSelectionTarget = {
  kind: 'link';
  link: Link;
};
