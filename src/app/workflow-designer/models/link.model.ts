import { Block, Port } from './block.model';

export interface Link {
  from: Block;
  to: Block;
  portFrom: Port;
  portTo: Port;
}

export interface JsonLink {
  fromIndex: number;
  toIndex: number;
  portFromIndex: number;
  portToIndex: number;
  fromName: string;
  toName: string;
}

export const linkToJson = (l: Link): JsonLink => {
  return {
    fromIndex: l.from.index,
    toIndex: l.to.index,
    portFromIndex: l.portFrom.index,
    portToIndex: l.portTo.index,
    fromName: l.portFrom.name,
    toName: l.portTo.name
  };
};
