export interface Block {
  x: number;
  y: number;
  inputPorts: Port[];
  outputPorts: Port[];
  index: number;
  payload: BlockPayload;
  name: string;
}

export interface GhostBlock {
  blockConstructor: BlockConstructor;
  x: number;
  y: number;
}

export interface Port {
  selected: boolean;
  index: number;
  name: string;
}

export interface BlockCreationData {
  inputs: {
    name: string
  }[];
  outputs: {
    name: string
  }[];
}

export interface BlockConstructor {
  category: string;
  getBlockCreationData: () => BlockCreationData;
  name: string;
  getPayload: () => BlockPayload;
}

export interface BlockPayload {
  kind: string;
  clusterName: string;
}
