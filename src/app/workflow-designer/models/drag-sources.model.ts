import { Block, BlockConstructor, Port } from './block.model';
import { Link } from './link.model';
import {
  createGhostArrow,
  createGhostBlock,
  createLinkAndHideGhostArrow,
  dropGhostBlock,
  hideGhostArrow,
  hideGhostBlock,
  moveBlock,
  moveGhostArrow,
  moveGhostBlock,
  moveSvgContainer,
  WorkflowStoreAction,
} from '../store/workflow-store-action.model';
import { DragTarget } from './drag-target.model';
import { WorkflowState } from '../store/workflow-state.model';

export type DragSource = BlockConstructorSource | BlockSource | PortSource | SvgContainerSource | LinkSource;

interface BaseDragSource {
  sourceEvent: MouseEvent;
  onDragStart: () => WorkflowStoreAction | undefined;
  onMove: (dx: number, dy: number) => WorkflowStoreAction | undefined;
  onDrop: (target: DragTarget, currentState: WorkflowState) => WorkflowStoreAction | undefined;
}

export type BlockConstructorSource = BaseDragSource & {
  kind: 'block-constructor';
  blockConstructor: BlockConstructor;
};

export const blockConstructorSource = (payload: {
  blockConstructor: BlockConstructor;
  sourceEvent: MouseEvent;
}): BlockConstructorSource => ({
  kind: 'block-constructor',
  ...baseDragSource,
  ...payload,
  onDragStart: () => {
    return createGhostBlock({
      blockConstructor: payload.blockConstructor,
      x: payload.sourceEvent.x,
      y: payload.sourceEvent.y,
    });
  },
  onMove: (dx, dy) => moveGhostBlock({ dx, dy }),
  onDrop: (target, currentState) => {
    const ghost = currentState.ghostBlock;
    if (target.kind === 'svgContainer' && ghost) {
      return dropGhostBlock({
        ghostBlock: ghost,
      });
    } else {
      return hideGhostBlock();
    }
  },
});

export type BlockSource = BaseDragSource & {
  kind: 'block';
  block: Block;
};

export const blockSource = (payload: { block: Block; sourceEvent: MouseEvent }): BlockSource => ({
  kind: 'block',
  ...payload,
  ...baseDragSource,
  onMove: (dx, dy) => moveBlock({ dx, dy, block: payload.block }),
});

export type PortSource = BaseDragSource & {
  kind: 'port';
  x: number;
  y: number;
  port: Port;
  block: Block;
};

export const portSource = (payload: {
  x: number;
  y: number;
  port: Port;
  block: Block;
  sourceEvent: MouseEvent;
}): PortSource => ({
  kind: 'port',
  ...payload,
  ...baseDragSource,
  onDragStart: () => {
    return createGhostArrow({
      x1: payload.x,
      x2: payload.x,
      y1: payload.y,
      y2: payload.y,
    });
  },
  onMove: (dx, dy) => moveGhostArrow({ dx, dy }),
  onDrop: (target) => {
    if (target.kind === 'port') {
      return createLinkAndHideGhostArrow({
        link: {
          from: payload.block,
          to: target.block,
          portFrom: payload.port,
          portTo: target.port,
        },
      });
    } else if (target.kind === 'block' && target.block.inputPorts.length > 0) {
      return createLinkAndHideGhostArrow({
        link: {
          from: payload.block,
          to: target.block,
          portFrom: payload.port,
          portTo: target.block.inputPorts[0],
        },
      });
    } else {
      return hideGhostArrow();
    }
  },
});

export type SvgContainerSource = BaseDragSource & {
  kind: 'svgContainer';
};

export const svgContainerSource = (payload: { sourceEvent: MouseEvent }): SvgContainerSource => ({
  kind: 'svgContainer',
  ...payload,
  ...baseDragSource,
  onMove: (dx, dy) => moveSvgContainer({ dx, dy }),
});

export type LinkSource = BaseDragSource & {
  kind: 'link';
  link: Link;
};

export const linkSource = (payload: { sourceEvent: MouseEvent; link: Link }): LinkSource => ({
  kind: 'link',
  ...payload,
  ...baseDragSource,
});

const baseDragSource = {
  onDragStart: () => undefined,
  onMove: () => undefined,
  onDrop: () => undefined,
};
