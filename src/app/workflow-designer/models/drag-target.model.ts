import { Block, Port } from './block.model';
import { Link } from './link.model';
import { DragSource } from './drag-sources.model';

export type DragTarget = IllegalTarget | SvgContainerTarget | UnknownTarget | PortTarget | BlockTarget | LinkTarget;

export type IllegalTarget = {
  kind: 'illegal';
};

export type PortTarget = {
  kind: 'port';
  x: number;
  y: number;
  block: Block;
  port: Port;
};

export type SvgContainerTarget = {
  kind: 'svgContainer';
  event: MouseEvent;
};

export type UnknownTarget = {
  kind: 'unknown';
  event: MouseEvent;
};

export type BlockTarget = {
  kind: 'block';
  event: MouseEvent;
  block: Block;
};

export type LinkTarget = {
  kind: 'link';
  event: MouseEvent;
  link: Link;
};
