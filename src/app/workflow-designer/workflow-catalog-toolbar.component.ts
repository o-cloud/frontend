import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { blockConstructorSource, BlockConstructorSource } from './models/drag-sources.model';
import { CATALOG_TOOLBAR_WIDTH } from './constants';
import { BlockConstructor } from './models/block.model';
import { groupBy } from '../@core/utils';

@Component({
  selector: 'app-workflow-catalog-toolbar',
  templateUrl: './workflow-catalog-toolbar.component.html',
})
export class WorkflowCatalogToolbarComponent implements OnInit {
  @Input()
  set blocks(blocks: BlockConstructor[]) {
    this.internBlocks = groupBy(blocks, b => b.category)
    this.categories = Object.keys(this.internBlocks).sort()
  }
  internBlocks: {[key: string]: BlockConstructor[]} = {}
  categories: string[] = []
  toolbarWidth = CATALOG_TOOLBAR_WIDTH;

  @Output()
  startDragBlockConstructor = new EventEmitter<BlockConstructorSource>();

  ngOnInit(): void {}

  onStartDragBlockConstructor(e: MouseEvent, block: BlockConstructor): void {
    this.startDragBlockConstructor.emit(
      blockConstructorSource({
        sourceEvent: e,
        blockConstructor: block,
      })
    );
  }
}
