export const BLOCK_WIDTH = 250;
export const BLOCK_HEIGHT = 70;
export const ARROW_POLYGON_WIDTH = 10;
export const ARROW_POLYGON_HEIGHT = 10;
export const PORT_RADIUS = 10;
export const CATALOG_TOOLBAR_WIDTH = 250;
