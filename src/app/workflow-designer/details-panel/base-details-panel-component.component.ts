import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Block, BlockPayload } from '../models/block.model';

@Component({
  template: ` <div>not-implemented</div>`,
})
export class BaseDetailsPanelComponent implements OnDestroy {
  @Input() block!: Block;
  @Output() updatePayload = new EventEmitter<{ b: Block; p: BlockPayload }>();
  @Output() destroy = new EventEmitter<void>();

  ngOnDestroy(): void {
    this.destroy.emit();
  }
}
