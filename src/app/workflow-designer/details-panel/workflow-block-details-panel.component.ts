import { Component, Input, ViewChild, OnInit, OnChanges, SimpleChanges, ComponentFactoryResolver } from '@angular/core';
import { Block } from '../models/block.model';
import { HostDirective } from './host.directive';
import { WorkflowDesignerService } from '../workflow-designer.service';
import { Subscription } from 'rxjs';
import { WorkflowStoreService } from '../store/workflow-store.service';
import { setBlockPayload } from '../store/workflow-store-action.model';

@Component({
  selector: 'app-workflow-block-details-panel',
  template: `
    <nb-card style="background: white; width: 350px; height: 100%;" (mouseup)="handleClick($event)">
      <nb-card-header>{{ block?.name }}</nb-card-header>
      <nb-card-body>
        <ng-template appHost></ng-template>
      </nb-card-body>
    </nb-card>
  `,
})
export class WorkflowBlockDetailsPanelComponent implements OnInit, OnChanges {
  @Input() block?: Block;
  @Input() isDragging = false;
  @ViewChild(HostDirective, { static: true }) appHost?: HostDirective;
  childSubscription?: Subscription;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private designerService: WorkflowDesignerService,
    private store: WorkflowStoreService
  ) {}

  handleClick = (event: MouseEvent): void => {
    if (!this.isDragging) {
      // If we dont stop the propagation we will loose the focus on the inputs
      event.stopPropagation();
    }
  };

  update = (): void => {
    if (this.block && this.appHost) {
      this.appHost.viewContainerRef.clear();
      this.childSubscription?.unsubscribe();
      const componentType = this.designerService.getDetailsPanelComponentType()
      const factory = this.componentFactoryResolver.resolveComponentFactory(
        componentType.type
      );
      const comp = this.appHost.viewContainerRef.createComponent(factory);
      if (componentType.createdHook) {
        componentType.createdHook(comp.instance)
      }
      comp.instance.block = this.block;
      comp.instance.destroy.asObservable().subscribe(() => {
        this.childSubscription?.unsubscribe();
      });
      this.childSubscription = comp.instance.updatePayload.asObservable().subscribe(({ b, p }) => {
        if (this.block) {
          console.log('update payload', p, b);
          this.store.dispatch(setBlockPayload({ block: b, payload: p }));
        }
      });
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.block && changes.block.previousValue?.index !== changes.block.currentValue?.index) {
      this.update();
    }
  }

  ngOnInit(): void {
    this.update();
  }
}
