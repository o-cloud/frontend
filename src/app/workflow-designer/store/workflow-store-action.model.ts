import { Block, BlockConstructor, BlockPayload, GhostBlock } from '../models/block.model';
import { Link } from '../models/link.model';
import { DragSource } from '../models/drag-sources.model';

export type WorkflowStoreAction =
  | CreateGhostBlock
  | CreateGhostArrow
  | MoveGhostBlock
  | MoveGhostArrow
  | MoveBlock
  | MoveSvgContainer
  | SelectBlock
  | SelectLink
  | SelectNothing
  | HideGhostBlock
  | HideGhostArrow
  | DropGhostBlock
  | CreateLinkAndHideGhostArrow
  | DeleteSelectedBlock
  | DeleteSelectedLink
  | Zoom
  | SetDragSource
  | LoadWorkflow
  | SetAvailableBlocks
  | SetBlockPayload;

export type SetBlockPayload = {
  type: 'set-block-payload';
  block: Block;
  payload: BlockPayload;
};

export const setBlockPayload = (payload: { block: Block; payload: BlockPayload }): SetBlockPayload => ({
  type: 'set-block-payload',
  ...payload,
});

export type SetDragSource = {
  type: 'set-drag-source';
  source?: DragSource;
};

export const setDragSource = (payload: { source: DragSource | undefined }): SetDragSource => ({
  type: 'set-drag-source',
  ...payload,
});

export type CreateGhostBlock = {
  type: 'create-ghost-block';
  blockConstructor: BlockConstructor;
  x: number;
  y: number;
};

export const createGhostBlock = (payload: {
  blockConstructor: BlockConstructor;
  x: number;
  y: number;
}): CreateGhostBlock => ({
  type: 'create-ghost-block',
  ...payload,
});

export type CreateGhostArrow = {
  type: 'create-ghost-arrow';
  x1: number;
  x2: number;
  y1: number;
  y2: number;
};

export const createGhostArrow = (payload: { x1: number; x2: number; y1: number; y2: number }): CreateGhostArrow => ({
  type: 'create-ghost-arrow',
  ...payload,
});

export type MoveGhostBlock = {
  type: 'move-ghost-block';
  dx: number;
  dy: number;
};

export const moveGhostBlock = (payload: { dx: number; dy: number }): MoveGhostBlock => ({
  type: 'move-ghost-block',
  ...payload,
});

export type MoveGhostArrow = {
  type: 'move-ghost-arrow';
  dx: number;
  dy: number;
};

export const moveGhostArrow = (payload: { dx: number; dy: number }): MoveGhostArrow => ({
  type: 'move-ghost-arrow',
  ...payload,
});

export type MoveBlock = {
  type: 'move-block';
  dx: number;
  dy: number;
  block: Block;
};

export const moveBlock = (payload: { dx: number; dy: number; block: Block }): MoveBlock => ({
  type: 'move-block',
  ...payload,
});

export type MoveSvgContainer = {
  type: 'move-svg-container';
  dx: number;
  dy: number;
};

export const moveSvgContainer = (payload: { dx: number; dy: number }): MoveSvgContainer => ({
  type: 'move-svg-container',
  ...payload,
});

export type SelectBlock = {
  type: 'select-block';
  block: Block;
};

export const selectBlock = (payload: { block: Block }): SelectBlock => ({
  type: 'select-block',
  ...payload,
});

export type SelectLink = {
  type: 'select-link';
  link: Link;
};

export const selectLink = (payload: { link: Link }): SelectLink => ({
  type: 'select-link',
  ...payload,
});

export type SelectNothing = {
  type: 'select-nothing';
};

export const selectNothing = (): SelectNothing => ({
  type: 'select-nothing',
});

export type HideGhostBlock = {
  type: 'hide-ghost-block';
};

export const hideGhostBlock = (): HideGhostBlock => ({
  type: 'hide-ghost-block',
});

export type HideGhostArrow = {
  type: 'hide-ghost-arrow';
};

export const hideGhostArrow = (): HideGhostArrow => ({
  type: 'hide-ghost-arrow',
});

export type DropGhostBlock = {
  type: 'drop-ghost-block';
  ghostBlock: GhostBlock;
};

export const dropGhostBlock = (payload: { ghostBlock: GhostBlock }): DropGhostBlock => ({
  type: 'drop-ghost-block',
  ...payload,
});

export type CreateLinkAndHideGhostArrow = {
  type: 'create-link-and-hide-ghost-arrow';
  link: Link;
};

export const createLinkAndHideGhostArrow = (payload: { link: Link }): CreateLinkAndHideGhostArrow => ({
  type: 'create-link-and-hide-ghost-arrow',
  ...payload,
});

export type DeleteSelectedLink = {
  type: 'delete-selected-link';
};

export const deleteSelectedLink = (): DeleteSelectedLink => ({
  type: 'delete-selected-link',
});

export type DeleteSelectedBlock = {
  type: 'delete-selected-block';
};

export const deleteSelectedBlock = (): DeleteSelectedBlock => ({
  type: 'delete-selected-block',
});

export type Zoom = {
  type: 'zoom';
  delta: number;
};

export const zoom = (payload: { delta: number }): Zoom => ({
  type: 'zoom',
  ...payload,
});

export type LoadWorkflow = {
  type: 'load-workflow';
  blocks: Block[];
  links: Link[];
};

export const loadWorkflow = (payload: { blocks: Block[]; links: Link[] }): LoadWorkflow => ({
  type: 'load-workflow',
  ...payload,
});

export type SetAvailableBlocks = {
  type: 'set-available-blocks';
  blocks: BlockConstructor[];
};

export const setAvailableBlocks = (payload: { blocks: BlockConstructor[] }): SetAvailableBlocks => ({
  type: 'set-available-blocks',
  ...payload,
});
