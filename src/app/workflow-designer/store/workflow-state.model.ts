import { Block, BlockConstructor, GhostBlock } from '../models/block.model';
import { Link } from '../models/link.model';
import { Arrow } from '../models/arrow.model';
import { SelectionTarget } from '../models/selection-target';
import { DragSource } from '../models/drag-sources.model';

export interface WorkflowState {
  availableBlocks: BlockConstructor[];
  blocks: Block[];
  links: Link[];
  ghostBlock?: GhostBlock;
  ghostArrow?: Arrow;
  svgOffsetX: number;
  svgOffsetY: number;
  svgScale: number;
  selectionTarget?: SelectionTarget;
  dragSource?: DragSource;
  nextBlockIndex: number;
}
