import { Injectable } from '@angular/core';
import { WorkflowState } from './workflow-state.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { hideGhostBlock, WorkflowStoreAction } from './workflow-store-action.model';
import { blockSource } from '../models/drag-sources.model';
import { Link } from '../models/link.model';
import { Block, Port } from '../models/block.model';

@Injectable({
  providedIn: 'root',
})
export class WorkflowStoreService {
  private maxScale = 2;
  private minScale = 0.5;
  private state = new BehaviorSubject<WorkflowState>(defaultState);

  public getState = (): Observable<WorkflowState> => this.state.asObservable();

  private updateBlock = (previousBlock: Block, newBlock: Block, previousState: WorkflowState): WorkflowState => {
    const newBlockList = previousState.blocks.map((b) => (b === previousBlock ? newBlock : b));
    const newLinkList = previousState.links.map((l) => {
      if (l.to === previousBlock || l.from === previousBlock) {
        const newLink = { ...l };
        if (l.from === previousBlock) {
          newLink.from = newBlock;
        }
        if (l.to === previousBlock) {
          newLink.to = newBlock;
        }
        return newLink;
      } else {
        return l;
      }
    });
    const newState = {
      ...previousState,
      links: newLinkList,
      blocks: newBlockList,
    };
    if (previousState.dragSource?.kind === 'block' && previousState.dragSource.block === previousBlock) {
      newState.dragSource = blockSource({
        block: newBlock,
        sourceEvent: previousState.dragSource.sourceEvent,
      });
    }
    if (previousState.selectionTarget?.kind === 'block' && previousState.selectionTarget.block === previousBlock) {
      newState.selectionTarget = {
        ...previousState.selectionTarget,
        block: newBlock,
      };
    }
    return newState;
  };

  public dispatch = (action: WorkflowStoreAction): void => {
    const prevState = this.state.getValue();
    switch (action.type) {
      case 'create-ghost-block':
        const { blockConstructor, x, y } = action;
        this.state.next({
          ...prevState,
          ghostBlock: {
            x,
            y,
            blockConstructor,
          },
        });
        break;
      case 'create-ghost-arrow':
        const { x1, x2, y1, y2 } = action;
        this.state.next({
          ...prevState,
          ghostArrow: { x1, x2, y1, y2 },
        });
        break;
      case 'move-ghost-block':
        if (prevState.ghostBlock) {
          // tslint:disable-next-line:no-shadowed-variable
          const { dx, dy } = action;
          const prevGhostBlock = prevState.ghostBlock;
          this.state.next({
            ...prevState,
            ghostBlock: {
              ...prevGhostBlock,
              x: prevGhostBlock.x + dx,
              y: prevGhostBlock.y + dy,
            },
          });
        }
        break;
      case 'move-block': {
        // tslint:disable-next-line:no-shadowed-variable
        const { dx, dy } = action;
        const newBlock = {
          ...action.block,
          x: action.block.x + dx / prevState.svgScale,
          y: action.block.y + dy / prevState.svgScale,
        };
        this.state.next(this.updateBlock(action.block, newBlock, prevState));
        break;
      }
      case 'move-ghost-arrow':
        if (prevState.ghostArrow) {
          const { dx, dy } = action;
          const prevGhostArrow = prevState.ghostArrow;
          this.state.next({
            ...prevState,
            ghostArrow: {
              ...prevGhostArrow,
              x2: prevGhostArrow.x2 + dx / prevState.svgScale,
              y2: prevGhostArrow.y2 + dy / prevState.svgScale,
            },
          });
        }
        break;
      case 'move-svg-container': {
        const { dx, dy } = action;
        this.state.next({
          ...prevState,
          svgOffsetY: prevState.svgOffsetY + dy,
          svgOffsetX: prevState.svgOffsetX + dx,
        });
        break;
      }
      case 'select-block':
        this.state.next({
          ...prevState,
          selectionTarget: { kind: 'block', block: action.block },
        });
        break;
      case 'select-link':
        this.state.next({
          ...prevState,
          selectionTarget: { kind: 'link', link: action.link },
        });
        break;
      case 'select-nothing':
        this.state.next({
          ...prevState,
          selectionTarget: undefined,
        });
        break;
      case 'hide-ghost-block':
        this.state.next({
          ...prevState,
          ghostBlock: undefined,
        });
        break;
      case 'hide-ghost-arrow':
        this.state.next({
          ...prevState,
          ghostArrow: undefined,
        });
        break;
      case 'drop-ghost-block':
        const svgContainer = document.getElementById('workflow-designer-svg-container');
        if (svgContainer) {
          const blockCreationData = action.ghostBlock.blockConstructor.getBlockCreationData();
          const inputs: Port[] = blockCreationData.inputs.map((v, i) => ({
            selected: false,
            index: i,
            name: v.name
          }));
          const outputs: Port[] = blockCreationData.outputs.map((v, i) => ({
            selected: false,
            index: i,
            name: v.name
          }));
          const block: Block = {
            index: prevState.nextBlockIndex,
            inputPorts: inputs,
            outputPorts: outputs,
            payload: action.ghostBlock.blockConstructor.getPayload(),
            name: action.ghostBlock.blockConstructor.name,
            x:
              (action.ghostBlock.x - svgContainer.getBoundingClientRect().left - prevState.svgOffsetX) /
              prevState.svgScale,
            y:
              (action.ghostBlock.y - svgContainer.getBoundingClientRect().top - prevState.svgOffsetY) /
              prevState.svgScale,
          };
          this.state.next({
            ...prevState,
            blocks: [...prevState.blocks, block],
            ghostBlock: undefined,
            nextBlockIndex: prevState.nextBlockIndex + 1,
          });
        } else {
          console.error('Cannot find svg container');
          this.dispatch(hideGhostBlock());
        }
        break;
      case 'create-link-and-hide-ghost-arrow':
        const newState = {
          ...prevState,
          ghostArrow: undefined,
        };
        if (!this.linkAlreadyExists(action.link)) {
          newState.links = [...prevState.links, action.link];
        }
        this.state.next(newState);
        break;
      case 'delete-selected-block':
        if (prevState.selectionTarget?.kind === 'block') {
          const selectedBlock = prevState.selectionTarget.block;
          this.state.next({
            ...prevState,
            blocks: prevState.blocks.filter((b) => b !== selectedBlock),
            links: prevState.links.filter((l) => l.to !== selectedBlock && l.from !== selectedBlock),
            selectionTarget: undefined,
          });
        }
        break;
      case 'delete-selected-link':
        if (prevState.selectionTarget?.kind === 'link') {
          const selectedLink = prevState.selectionTarget.link;
          this.state.next({
            ...prevState,
            links: prevState.links.filter((l) => l !== selectedLink),
            selectionTarget: undefined,
          });
        }
        break;
      case 'zoom':
        let newScale = prevState.svgScale - action.delta;
        newScale = Math.min(newScale, this.maxScale);
        newScale = Math.max(newScale, this.minScale);
        this.state.next({
          ...prevState,
          svgScale: newScale,
        });
        break;
      case 'set-drag-source':
        this.state.next({
          ...prevState,
          dragSource: action.source,
        });
        break;
      case 'load-workflow': {
        this.state.next({
          ...prevState,
          blocks: action.blocks,
          links: action.links,
          nextBlockIndex: Math.max(-1, ...action.blocks.map((b) => b.index)) + 1,
        });
        break;
      }
      case 'set-available-blocks': {
        this.state.next({
          ...prevState,
          availableBlocks: action.blocks,
        });
        break;
      }
      case 'set-block-payload': {
        const newBlock = {
          ...action.block,
          payload: action.payload,
        };
        this.state.next(this.updateBlock(action.block, newBlock, prevState));
      }
    }
  };

  private linkAlreadyExists = (link: Link): boolean => {
    return !!this.state
      .getValue()
      .links.find(
        (l) => l.from === link.from && l.to === link.to && l.portFrom === link.portFrom && l.portTo === link.portTo
      );
  };
}

const defaultState: WorkflowState = {
  availableBlocks: [],
  blocks: [],
  links: [],
  svgOffsetX: 300,
  svgOffsetY: 0,
  svgScale: 1,
  nextBlockIndex: 0,
};
