import { Injectable, Type } from '@angular/core';
import { DragSource } from './models/drag-sources.model';
import { DragTarget } from './models/drag-target.model';
import { WorkflowStoreService } from './store/workflow-store.service';
import {
  deleteSelectedBlock,
  deleteSelectedLink,
  loadWorkflow,
  selectBlock,
  selectLink,
  selectNothing,
  setAvailableBlocks,
  setDragSource,
  WorkflowStoreAction,
  zoom,
} from './store/workflow-store-action.model';
import { WorkflowState } from './store/workflow-state.model';
import { CATALOG_TOOLBAR_WIDTH } from './constants';
import { Block, BlockConstructor } from './models/block.model';
import { JsonLink, Link } from './models/link.model';
import { BaseDetailsPanelComponent } from './details-panel/base-details-panel-component.component';

const CLICK_INTERVAL_MS = 200;
export interface DetailsPanelComponentType<T extends BaseDetailsPanelComponent> {
  type: Type<T>;
  createdHook?: (component: T) => void;
}
@Injectable({
  providedIn: 'root',
})
export class WorkflowDesignerService {
  private lastMousePosition: { x: number; y: number } = { x: 0, y: 0 };
  private state: WorkflowState = {} as WorkflowState;
  private detailsPanelComponentType: DetailsPanelComponentType<any> = {
    type: BaseDetailsPanelComponent,
  };

  constructor(private store: WorkflowStoreService) {
    store.getState().subscribe((s) => (this.state = s));
  }

  getDetailsPanelComponentType = <T extends BaseDetailsPanelComponent>(): DetailsPanelComponentType<T> =>
    this.detailsPanelComponentType;

  setDetailsPanelComponentType = <T extends BaseDetailsPanelComponent>(type: DetailsPanelComponentType<T>): void => {
    this.detailsPanelComponentType = type;
  };

  loadWorkflow = (w: any) => {
    console.log('load', w);
    let blocks: Block[] = [];
    let jsonLinks: JsonLink[] = [];
    if (w.blocks) {
      blocks = w.blocks;
    }
    if (w.links) {
      jsonLinks = w.links;
    }
    const links: Link[] = jsonLinks.map((l) => {
      return {
        portTo: { index: l.portToIndex, selected: false, name: l.toName },
        portFrom: { index: l.portFromIndex, selected: false, name: l.fromName },
        from: blocks.find((b) => b.index === l.fromIndex) as Block,
        to: blocks.find((b) => b.index === l.toIndex) as Block,
      };
    });
    console.log(links, blocks);
    this.dispatch(loadWorkflow({ blocks, links }));
  };

  loadBlockConstructors = (blocks: BlockConstructor[]) => {
    this.dispatch(setAvailableBlocks({ blocks }));
  };

  private dispatch = (action: WorkflowStoreAction | undefined): void => {
    if (action) {
      this.store.dispatch(action);
    }
  };

  public startDrag = (source: DragSource): void => {
    document.removeEventListener('mouseup', this.documentMouseUpListener);
    document.addEventListener('mouseup', this.documentMouseUpListener);
    this.dispatch(setDragSource({ source }));
    this.lastMousePosition.x = source.sourceEvent.x;
    this.lastMousePosition.y = source.sourceEvent.y;

    this.dispatch(source.onDragStart());
  };

  public mouseMove = (event: MouseEvent): void => {
    if (this.state.dragSource) {
      const dx = event.x - this.lastMousePosition.x;
      const dy = event.y - this.lastMousePosition.y;
      this.lastMousePosition.x = event.x;
      this.lastMousePosition.y = event.y;

      this.dispatch(this.state.dragSource.onMove(dx, dy));
    }
  };

  public stopDrag = (target: DragTarget): void => {
    if (this.state.dragSource) {
      if (this.isSimpleClick(this.state.dragSource, target)) {
        switch (target.kind) {
          case 'block':
            this.dispatch(selectBlock({ block: target.block }));
            break;
          case 'link':
            this.dispatch(selectLink({ link: target.link }));
            break;
          default:
            this.dispatch(selectNothing());
            break;
        }
      }

      if (target.kind === 'unknown') {
        target = this.inferTarget(target.event);
      }

      this.dispatch(this.state.dragSource.onDrop(target, this.state));
      this.dispatch(setDragSource({ source: undefined }));

      document.removeEventListener('mouseup', this.documentMouseUpListener);
    }
  };

  private isSimpleClick = (source: DragSource, target: DragTarget): boolean => {
    switch (source.kind) {
      case 'block':
        if (
          target.kind === 'block' &&
          source.block === target.block &&
          this.isInClickTimeInterval(source.sourceEvent, target.event)
        ) {
          return true;
        }
        break;
      case 'link':
        if (
          target.kind === 'link' &&
          source.link === target.link &&
          this.isInClickTimeInterval(source.sourceEvent, target.event)
        ) {
          return true;
        }
        break;
      case 'svgContainer':
        return target.kind === 'svgContainer';
    }
    return false;
  };

  private isInClickTimeInterval = (firstEvent: Event, secondEvent: Event): boolean => {
    return secondEvent.timeStamp - firstEvent.timeStamp < CLICK_INTERVAL_MS;
  };

  private documentMouseUpListener = () => {
    this.stopDrag({ kind: 'illegal' });
  };

  private inferTarget = (e: MouseEvent): DragTarget => {
    const svgContainer = document.getElementById('workflow-designer-svg-container');
    if (svgContainer) {
      const rect = svgContainer.getBoundingClientRect();
      if (e.x > rect.left + CATALOG_TOOLBAR_WIDTH && e.x < rect.right && e.y > rect.top && e.y < rect.bottom) {
        return { kind: 'svgContainer', event: e };
      }
    }
    return { kind: 'illegal' };
  };

  public onKeyUp = (e: KeyboardEvent): void => {
    const target = this.state.selectionTarget;
    if (e.key === 'Delete' && target?.kind === 'block') {
      this.dispatch(deleteSelectedBlock());
    } else if (e.key === 'Delete' && target?.kind === 'link') {
      this.dispatch(deleteSelectedLink());
    }
  };

  public onWheel = (e: WheelEvent): void => {
    if (this.state.dragSource) {
      return;
    }
    const delta = e.deltaY / 1000;
    this.dispatch(zoom({ delta }));
  };
}
