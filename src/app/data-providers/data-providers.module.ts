import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataProvidersComponent } from './data-providers.component';
import { DataProvidersRoutingModule } from './data-providers-routing.module';
import { NbCardModule, NbIconModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SharedModule } from '../@shared/shared.module';



@NgModule({
  declarations: [DataProvidersComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule,
    DataProvidersRoutingModule,
    NbIconModule,
    SharedModule,
  ]
})
export class DataProvidersModule { }
