import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Service } from '../../@core/models/service.model';
import { ServicesProviderService } from '../../@core/services/services-provider.service';

@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['../services-provider.component.scss'],
})
export class CreateServiceComponent implements OnInit {
  oldService: Service | null = null;
  service: Service = {
    name: '',
    version: '',
    description: '',
    cwl: 'cwlVersion: v1.0\nclass: ServiceProvider\nlabel: ',
    images: [],
  };
  bgd_img = '';
  error: any = {
    All: '',
    Name: '',
    Version: '',
    Description: '',
    Cwl: '',
    Images: '',
  };
  datasource: any;
  edit: any;
  editorOptions = {
    theme: 'vs',
    language: 'yaml',
    scrollBeyondLastLine: false,
    minimap: {
      enabled: false,
    },
  };

  constructor(
    private servicesProviderService: ServicesProviderService,
    protected dialogRef: NbDialogRef<CreateServiceComponent>
  ) {}

  ngOnInit(): void {
    if (this.edit) {
      this.oldService = Object.assign({}, this.service);
    }
  }

  create(): void {
    this.servicesProviderService.addService(this.service).subscribe(
      () => {
        this.datasource.add(this.service);
        this.datasource.refresh();
        this.close();
      },
      (err) => {
        err = err.error.errors;
        if (err.hasOwnProperty('422001')) {
          this.error.All = '';
          this.error = err['422001'];
        } else if (err.hasOwnProperty('422002')) {
          this.error.All = err['422002'];
        }
      }
    );
  }

  update(): void {
    this.servicesProviderService.updateService(this.oldService!.name, this.oldService!.version, this.service).subscribe(
      () => {
        this.datasource.update(this.oldService, this.service);
        this.datasource.refresh();
        this.close();
      },
      (err) => {
        err = err.error.errors;
        if (err.hasOwnProperty('422001')) {
          this.error.All = '';
          this.error = err['422001'];
        } else if (err.hasOwnProperty('422002')) {
          this.error.All = err['422002'];
        }
      }
    );
  }

  close(): void {
    this.dialogRef.close();
  }
}
