import { Component, OnInit } from '@angular/core';
import { ServicesProviderService } from '../@core/services/services-provider.service';
import { LocalDataSource } from 'ng2-smart-table';
import { CreateServiceComponent } from './create-service/create-service.component';
import { NbDialogService } from '@nebular/theme';
import { ActionsComponent } from '../@shared/table-layout/actions.component';
import { CwlEditorButtonViewComponent } from '../@shared/cwl-editor/cwl-editor.component';
import { ModalsService } from '../@shared/validation-modal/modals.service';
import { ImgPreviewViewComponent } from '../@shared/img-preview/img-preview.component';

@Component({
  selector: 'app-services-provider',
  templateUrl: './services-provider.component.html',
  styleUrls: ['./services-provider.component.scss'],
})
export class ServicesProviderComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  constructor(
    private modalService: ModalsService,
    private servicesProviderService: ServicesProviderService,
    protected dialogService: NbDialogService
  ) {}

  settings = {
    mode: 'external',
    hideSubHeader: true,
    noDataMessage: 'No service found...',
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Name',
        width: '300px',
      },
      version: {
        title: 'Version',
        width: '100px',
      },
      description: {
        title: 'Description',
      },
      cwl: {
        title: 'CWL',
        width: '60px',
        sort: false,
        type: 'custom',
        renderComponent: CwlEditorButtonViewComponent,
      },
      images: {
        title: 'Image',
        sort: false,
        width: '60px',
        type: 'custom',
        renderComponent: ImgPreviewViewComponent,
        valuePrepareFunction: (value: string[]) => (value[0] ? value[0] : ''),
      },
      _custom_actions: {
        title: '',
        width: '130px',
        sort: false,
        type: 'custom',
        renderComponent: ActionsComponent,
        onComponentInitFunction: this.onAction.bind(this),
      },
    },
  };

  onAction(instance: any): void {
    instance.delete.subscribe((row: any) => this.onDelete(row));
    instance.edit.subscribe((row: any) => this.onEdit(row));
  }

  onCreate(): void {
    this.dialogService.open(CreateServiceComponent, {
      closeOnBackdropClick: false,
      context: { datasource: this.source, edit: false },
    });
  }

  onEdit(row: any): void {
    this.dialogService.open(CreateServiceComponent, {
      closeOnBackdropClick: false,
      context: { datasource: this.source, service: row, edit: true },
    });
  }

  onDelete(row: any): void {
    this.modalService
      .openModalValidation(
        'The service ' + row.name + ' will be permanently deleted. Are you sure ?\n',
        'Delete the service',
        'Cancel'
      )
      .onClose.subscribe((result) => {
        if (result === true) {
          this.servicesProviderService.deleteService(row.name, row.version).subscribe(() => {
            this.source.remove(row);
          });
        }
      });
  }

  ngOnInit(): void {
    this.servicesProviderService.getServices().subscribe((data) => {
      this.source.load(data);
    });
  }
}
