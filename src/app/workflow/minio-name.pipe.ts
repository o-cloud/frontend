import { Pipe, PipeTransform } from '@angular/core';
import { Favorite } from '../@core/models/favorite.model';

@Pipe({name: 'appMinioName'})
export class MinioNamePipe implements PipeTransform {
  transform(f: Favorite): string {
    return 'MinIO in ' + (f.isLocal ? `Local(${f.originClusterName})` : f.originClusterName)
  }
}
