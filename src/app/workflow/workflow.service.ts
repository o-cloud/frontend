import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AllWorkflowResponseContainer, Workflow } from './workflow.model';
import { environment } from '../../environments/environment';
import { Provider, ProviderType } from '../@core/models/catalog.models';
import { FavoriteService } from '../@core/services/favorite.service';
import { map } from 'rxjs/operators';
import { Favorite } from '../@core/models/favorite.model';

@Injectable({ providedIn: 'root' })
export class WorkflowService {
  constructor(private httpClient: HttpClient, private favoriteService: FavoriteService) {}

  private WORKFLOW_BASE_ROUTE = environment.urlJsonServer + '/workflows';

  getAllWorkflows(): Observable<AllWorkflowResponseContainer> {
    return this.httpClient.get<AllWorkflowResponseContainer>(this.WORKFLOW_BASE_ROUTE);
  }

  getWorkflow(id: string): Observable<Workflow> {
    return this.httpClient.get<Workflow>(`${this.WORKFLOW_BASE_ROUTE}/${id}`);
  }

  createWorkflow(workflow: Omit<Workflow, 'id'>): Observable<Workflow> {
    return this.httpClient.post<Workflow>(this.WORKFLOW_BASE_ROUTE, workflow);
  }

  deleteWorkflow(id: string): Observable<void> {
    return this.httpClient.delete<void>(this.WORKFLOW_BASE_ROUTE + '/' + id);
  }

  updateWorkflow(id: string, body: any): Observable<void> {
    body = { id, ...body };
    return this.httpClient.post<void>(`${this.WORKFLOW_BASE_ROUTE}/${id}`, body);
  }

  getProviders(): Observable<{ compute: Favorite[], others: Provider[] }> {
    // TODO put data in cache
    return this.favoriteService.getFavorites().pipe(
      map(favorites => {
        const compute = favorites.filter(f => f.providerType === ProviderType.Computing);

        const others: Provider[] = favorites.filter(f => f.providerType !== ProviderType.Computing).map(f => {
          let endpoint = f.endpointLocation;
          if (!endpoint.trim().startsWith('http')) {
            endpoint = 'http://' + location.host + endpoint.trim();
          }
          return {
            name: f.name,
            providerId: f.providerId,
            description: f.description,
            type: f.providerType,
            cwl: f.workflowStructure,
            endpointLocation: endpoint,
            clusterName: f.isLocal ? `Local (${f.originClusterName})` : f.originClusterName
          };
        });
        return { compute, others };
      })
    );
  }

  startWorkflow(workflow: Workflow): Observable<any> {
    return this.httpClient.post(`${this.WORKFLOW_BASE_ROUTE}/${workflow.id}/start`, workflow);
  }
}
