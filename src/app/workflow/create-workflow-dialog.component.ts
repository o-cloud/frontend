import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FavoriteService } from '../@core/services/favorite.service';
import { Favorite } from '../@core/models/favorite.model';
import { ProviderType } from '../@core/models/catalog.models';
import { Workflow } from './workflow.model';

@Component({
  selector: 'app-create-workflow-dialog-component',
  template: `
    <nb-card style="min-width: 600px">
      <nb-card-header>Create workflow</nb-card-header>
      <nb-card-body>
        <div class="form-group">
          <label class="label">Name</label>
          <input nbInput class="input-full-width" type="text" [(ngModel)]="name" />
        </div>
        <div class="form-group">
          <label class="label">Compute provider</label>
          <nb-select
            class="input-full-width"
            [(ngModel)]="selectedComputeProvider"
            [disabled]="computeProviders.length === 0"
            placeholder="No compute provider available"
          >
            <nb-option *ngFor="let p of computeProviders" [value]="p">{{
              p.isLocal ? 'Local (' + p.originClusterName + ')' : p.originClusterName
            }}</nb-option>
          </nb-select>
        </div>
      </nb-card-body>
      <nb-card-footer style="display: flex; gap: 1em">
        <app-button (click)="submit()">Create</app-button>
        <app-button (click)="close()">Cancel</app-button>
      </nb-card-footer>
    </nb-card>
  `,
  styles: [
    `
      h2 {
        font-size: 1em;
      }

      label {
        display: inline-block;
        margin-bottom: 0.5em;
        font-size: 0.9em !important;
      }

      .input-full-width {
        display: block;
      }

      .form-group {
        margin-bottom: 1em;
      }
    `,
  ],
})
export class CreateWorkflowDialogComponent implements OnInit {
  computeProviders: Favorite[] = [];
  name = '';
  selectedComputeProvider: Favorite | undefined = undefined;

  constructor(protected ref: NbDialogRef<CreateWorkflowDialogComponent>, protected favoriteService: FavoriteService) {}

  ngOnInit(): void {
    this.favoriteService.getFavorites().subscribe((favorites) => {
      this.computeProviders = favorites.filter((f) => f.providerType === ProviderType.Computing);
      if (this.computeProviders.length) {
        const localProvider = this.computeProviders.find((p) => p.isLocal);
        if (localProvider) {
          this.selectedComputeProvider = localProvider;
        } else {
          this.selectedComputeProvider = this.computeProviders[0];
        }
      }
    });
  }

  close(): void {
    this.ref.close();
  }

  submit(): void {
    if (!this.selectedComputeProvider) {
      this.ref.close();
    } else {
      const r: Omit<Workflow, 'id'> = {
        name: this.name,
        blocks: [],
        runConfig: {
          computeProvider: this.selectedComputeProvider,
        },
      };
      this.ref.close(r);
    }
  }
}
