import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { WorkflowResolver } from './workflow.resolver';
import { WorkflowDetailsComponent } from './workflow-details/workflow-details.component';

const routes: Routes = [
  {
    path: 'workflow/:id',
    component: WorkflowDetailsComponent,
    resolve: {
      workflow: WorkflowResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkflowRoutingModule {}
