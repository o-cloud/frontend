import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { WorkflowService } from './workflow.service';
import { Workflow } from './workflow.model';
import { DetailsPanelComponentType, WorkflowDesignerService } from '../workflow-designer/workflow-designer.service';
import { TreatmentPayload } from './treatment-payload.model';
import { DetailsPanelComponent } from './details-panel.component';
import { InputExecutionType } from '../@core/models/cwl-document/cwl-command-line-tool';

@Component({
  selector: 'app-designer-wrapper-component',
  template: `
    <app-workflow-designer
      (workflowChange)="onWorkflowChanged($event)"
      (destroy)="onWorkflowChanged($event)"
    ></app-workflow-designer>
  `,
  styles: [`
    :host {
      display: block;
      margin-left: -36px;
      margin-right: -36px;
      height: calc(100% + 36px);
    }
  `]
})
export class DesignerWrapperComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  @Input()
  workflow!: Workflow;
  @Output()
  workflowChange = new EventEmitter<Workflow>()

  constructor(
    private route: ActivatedRoute,
    private workflowService: WorkflowService,
    private designerService: WorkflowDesignerService
  ) {

  }

  ngOnInit(): void {
    const providers = this.workflowService.getProviders();
    this.subscriptions.add(
      providers.subscribe(result => {
        for (const block of this.workflow.blocks){
         const provider = result.compute.find(p => p.providerId === (block.payload as TreatmentPayload).computeProvider.providerId)
         if (provider){
           provider.costLimitCluster = (block.payload as TreatmentPayload).computeProvider.costLimitCluster
         }

        }
        this.designerService.loadWorkflow(this.workflow)
        const blocks = result.others.map((provider) => ({
          category: provider.type.toString(),
          getBlockCreationData: () => ({
            inputs: Object.keys(provider.cwl.inputs || {}).filter(key => provider.cwl.inputs[key].inputType === InputExecutionType.pipeline)
              .map(key => ({ name: key })),
            outputs: (Object.keys(provider.cwl.outputs || {}) ).map(key => ({ name: key})),
          }),
          name: provider.name,
          getPayload: (): TreatmentPayload => ({
            kind: 'treatment-payload',
            cwl: provider.cwl,
            id: -1,
            parameters: {},
            providerType: provider.type.toString(),
            endpointLocation: provider.endpointLocation,
            computeProvider: result.compute.find(p => p.providerId === this.workflow.runConfig.computeProvider.providerId) || this.workflow.runConfig.computeProvider,
            costLimitBlock: 0,
            costLimitCluster: 0,
            s3Provider: this.workflow.runConfig.computeProvider,
            clusterName: provider.clusterName,
          }),
        }));
        this.designerService.loadBlockConstructors(blocks);
        const detailsPanel: DetailsPanelComponentType<DetailsPanelComponent> = {
          type: DetailsPanelComponent,
          createdHook: (c: DetailsPanelComponent) => {
            c.computeProviders = result.compute
          }
        }
        this.designerService.setDetailsPanelComponentType(detailsPanel);
      })
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onWorkflowChanged(p: any): void {
    if (this.workflow) {
      const toSave = { ...this.workflow, ...p };
      this.workflowChange.emit(toSave)
      console.log('save', toSave);
      this.workflowService.updateWorkflow(this.workflow.id, toSave).subscribe();
    }
  }
}
