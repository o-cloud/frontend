import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommandInputType } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-float-input-field-component',
  template: `
    <div class="form-group">
      <label class="label" *ngIf="name.length">
        {{ name }}
        <span class="label-type">({{ type.type }})</span>
      </label>
      <input
        nbInput
        class="input-full-width"
        type="number"
        [value]="value"
        (change)="onChange($event)"
        (keypress)="onKeyPressed($event)"
      />
    </div>
  `,
  styleUrls: ['./styles.scss'],
})
export class FloatInputFieldComponent implements OnInit {
  @Input() type!: CommandInputType;
  @Input() value!: any;
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();

  onChange(e: any): void {
    this.valueChanged.emit(e.target.value);
  }

  onKeyPressed(e: KeyboardEvent): boolean {
    return !isNaN(parseInt(e.key, 10)) || e.key === '.';
  }

  constructor() {}

  ngOnInit(): void {
  }
}
