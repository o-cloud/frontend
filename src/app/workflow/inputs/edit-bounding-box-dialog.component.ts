import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import 'leaflet-draw'
import * as L from 'leaflet'
import { NbDialogRef } from '@nebular/theme';
@Component({
  selector: 'app-edit-bounding-box-dialog-component',
  template: `
    <nb-card style="min-width: 80vw">
      <nb-card-header>Edit Bounding Box</nb-card-header>
      <nb-card-body>
        <div style="display: flex; gap: 1rem">
          <div class="form-group">
            <label class="label">Latitude Point 1</label>
            <input nbInput type="number" [value]="lat1" (change)="setLat1($event)">
          </div>
          <div class="form-group">
            <label class="label">Longitude Point 1</label>
            <input nbInput type="number" [value]="lng1" (change)="setLng1($event)">
          </div>
          <div class="form-group">
            <label class="label">Longitude Point 2</label>
            <input nbInput type="number" [value]="lat2" (change)="setLat2($event)">
          </div>
          <div class="form-group">
            <label class="label">Longitude Point 2</label>
            <input nbInput type="number" [value]="lng2" (change)="setLng2($event)">
          </div>
        </div>
        <div #map class="map">
        </div>
      </nb-card-body>
      <nb-card-footer style="display: flex; gap: 1em">
        <button nbButton (click)="saveAndClose()">Apply</button>
        <button nbButton (click)="close()">Cancel</button>
      </nb-card-footer>
    </nb-card>
  `,
  styleUrls: ['styles.scss'],
  styles: [`
    .map {
      width: 100%;
      height: 65vh;
      position: relative;
    }
    input {
      display: block;
    }
  `]
})

export class EditBoundingBoxDialogComponent implements OnInit, AfterViewInit {
  @ViewChild('map') mapElement!: ElementRef
  private map!: L.Map
  private rectangle!: L.Rectangle
  lat1 = 0
  lat2 = 0
  lng1 = 0
  lng2 = 0


  constructor(protected ref: NbDialogRef<EditBoundingBoxDialogComponent>) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.map = L.map(this.mapElement.nativeElement, {
      center: new L.LatLng(51.505, -0.04),
      zoom: 13,
      zoomControl: false
    })

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
    const bounds = new L.LatLngBounds(new L.LatLng(this.lat1, this.lng1), new L.LatLng(this.lat2, this.lng2))
    this.rectangle = new L.Rectangle(bounds);
    // @ts-ignore
    this.rectangle.editing.enable();

    this.rectangle.on('edit', (e) => {
      const b = e.target._bounds
      console.log(b)
      this.lat2 = b._northEast.lat
      this.lng2 = b._northEast.lng
      this.lat1 = b._southWest.lat
      this.lng1 = b._southWest.lng
    })

    this.map.addLayer(this.rectangle);
    this.map.fitBounds(bounds);
    this.map.zoomOut();
  }

  setLat1(e: Event): void {
    this.lat1 = parseFloat((e.target as HTMLInputElement).value)
    this.refreshRectanglePosition()
  }

  setLat2(e: Event): void {
    this.lat2 = parseFloat((e.target as HTMLInputElement).value)
    this.refreshRectanglePosition()
  }

  setLng1(e: Event): void {
    this.lng1 = parseFloat((e.target as HTMLInputElement).value)
    this.refreshRectanglePosition()
  }

  setLng2(e: Event): void {
    this.lng2 = parseFloat((e.target as HTMLInputElement).value)
    this.refreshRectanglePosition()
  }

  refreshRectanglePosition(): void {
    const bounds = new L.LatLngBounds(new L.LatLng(this.lat1, this.lng1), new L.LatLng(this.lat2, this.lng2))
    this.rectangle.setBounds(bounds)
    this.rectangle.redraw()

    // @ts-ignore
    this.rectangle.editing.disable();
    // @ts-ignore
    this.rectangle.editing.enable();
  }

  close(): void {
    this.ref.close()
  }

  saveAndClose(): void {
    this.ref.close({
      lat1: this.lat1,
      lng1: this.lng1,
      lat2: this.lat2,
      lng2: this.lng2
    })
  }
}
