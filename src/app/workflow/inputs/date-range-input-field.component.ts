import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbCalendarRange } from '@nebular/theme';

@Component({
  selector: 'app-date-range-input-field-component',
  template: `
    <div class="form-group">
      <label class="label" *ngIf="name.length">Date Range</label>
      <input nbInput placeholder="Pick Date Range" [nbDatepicker]="formpicker"  [value]="dateStr">
      <nb-rangepicker #formpicker (rangeChange)="onChange($event)" [range]="date"></nb-rangepicker>
    </div>
  `,

  styleUrls: ['./inputs.component.scss']
})
export class DateRangeInputFieldComponent {
  date: NbCalendarRange<any> = {
    start: new Date(),
    end: new Date()
  }
  dateStr = ''
  @Input()
  set value(value: any) {
    console.log('range value: ', value)
    if (value && value.end && value.start) {
      this.date = {
        start: new Date(value.start),
        end: new Date(value.end)
      }
      const start = this.date.start.toDateString().split(' ')
      const end = this.date.end.toDateString().split(' ')
      this.dateStr = `${start[1]} ${parseInt(start[2], 10)}, ${start[3]} - ${end[1]} ${parseInt(end[2], 10)}, ${end[3]}`
    }
    console.log('date value', this.date)
  }
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();

  onChange(e: any): void {
    if (e.start && e.end) {
      console.log('range changed', e.start.toISOString())
      this.valueChanged.emit(e);
    }
  }

  constructor() {

  }
}
