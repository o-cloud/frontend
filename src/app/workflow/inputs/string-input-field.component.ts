import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-string-input-field-component',
  template: `
    <div class="form-group">
      <label class="label" *ngIf="name.length">{{ name }} <span class="label-type">(string)</span></label>
      <input nbInput class="input-full-width" type="text" [value]="value" (change)="onChange($event)" />
    </div>
  `,
  styleUrls: ['styles.scss']
})
export class StringInputFieldComponent {
  @Input() value!: any;
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();

  onChange(e: any): void {
    this.valueChanged.emit(e.target.value);
  }

  constructor() {

  }
}
