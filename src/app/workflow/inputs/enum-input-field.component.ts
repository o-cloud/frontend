import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommandInputEnumSchema } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-enum-input-field-component',
  template: `
    <div class="form-group">
      <label class="label" *ngIf="name.length">{{ name }} <span class="label-type">(enum)</span></label>
      <nb-select class="input-full-width" (selectedChange)="onChange($event)" [selected]="value">
        <nb-option *ngFor="let x of type.symbols" [value]="x">{{x}}</nb-option>
      </nb-select>
    </div>
  `,
  styleUrls: ['styles.scss'],
})
export class EnumInputFieldComponent {
  @Input()
  type!: CommandInputEnumSchema;

  @Input() value!: any;
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();

  onChange(e: any): void {
    console.log(e)
    this.valueChanged.emit(e);
  }

  constructor() {}
}
