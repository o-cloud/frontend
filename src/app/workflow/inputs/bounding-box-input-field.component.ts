import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import * as L from 'leaflet'
import { NbDialogService } from '@nebular/theme';
import { EditBoundingBoxDialogComponent } from './edit-bounding-box-dialog.component';
@Component({
  selector: 'app-bounding-box-input-field-component',
  template: `
    <div class="form-group">
      <label class="label" *ngIf="name.length">{{ name }}</label>
<!--      <input nbInput class="input-full-width" type="text" [value]="value" (change)="onChange($event)" />-->
      <div #map class="map">
        <button nbButton style="z-index: 1000; position: absolute; bottom: 5px; left: 5px" (click)="openEditDialog()">
          <nb-icon icon="edit-outline" nbTooltip="Edit bounding box" ></nb-icon>
        </button>
      </div>
    </div>
  `,
  styleUrls: ['styles.scss'],
  styles: [`
    .map {
      width: 100%;
      height: 200px;
      position: relative;
    }
  `]
})
export class BoundingBoxInputFieldComponent implements AfterViewInit{
  @Input() value!: any;
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();
  @ViewChild('map') mapElement!: ElementRef
  private map!: L.Map
  private rectangle!: L.Rectangle
  lat1 = 43.1
  lng1 = 3.1
  lat2 = 43.2
  lng2 = 3.2

  constructor(private dialogService: NbDialogService) {
  }

  ngAfterViewInit(): void {
    if (this.value && this.value.lat1 && this.value.lng1 && this.value.lat2 && this.value.lng2) {
      this.lat1 = this.value.lat1
      this.lat2 = this.value.lat2
      this.lng1 = this.value.lng1
      this.lng2 = this.value.lng2
    } else {
      this.valueChanged.emit({lat1: this.lat1, lat2: this.lat2, lng1: this.lng1, lng2: this.lng2})
    }
    const bounds = L.latLngBounds(new L.LatLng(this.lat1, this.lng1), new L.LatLng(this.lat2, this.lng2))

    this.map = L.map(this.mapElement.nativeElement, {
      center: [ 39.8282, -98.5795 ],
      zoom: 3,
      zoomControl: false
    })

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
    this.rectangle = new L.Rectangle(bounds).addTo(this.map)

    this.map.fitBounds(bounds)
    this.map.zoomOut()
  }

  openEditDialog(): void {
    this.dialogService.open(EditBoundingBoxDialogComponent, {
      closeOnBackdropClick: false,
      context: {
        lat1: this.lat1,
        lat2: this.lat2,
        lng1: this.lng1,
        lng2: this.lng2
      }
    }).onClose.subscribe((res: {lat1: number, lat2: number, lng1: number, lng2: number} | undefined) => {
      if (res && res.lat1 && res.lat2 && res.lng1 && res.lng2) {
        this.lat1 = res.lat1
        this.lat2 = res.lat2
        this.lng1 = res.lng1
        this.lng2 = res.lng2
        const bounds = L.latLngBounds(new L.LatLng(this.lat1, this.lng1), new L.LatLng(this.lat2, this.lng2))
        this.rectangle.setBounds(bounds)
        this.map.fitBounds(bounds)
        this.map.zoomOut()
        this.valueChanged.emit({lat1: this.lat1, lat2: this.lat2, lng1: this.lng1, lng2: this.lng2})
      }
    })
  }
}
