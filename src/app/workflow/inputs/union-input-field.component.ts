import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommandInputUnionSchema } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-union-input-field-component',
  template: `
    <nb-accordion>
      <nb-accordion-item>
        <nb-accordion-item-header>{{ name }}</nb-accordion-item-header>
        <nb-accordion-item-body>
          <div class="form-group">
            <label class="label">Type</label>
            <nb-select
              class="input-full-width"
              [selected]="internValue.typeIndex "
              (selectedChange)="onChangeType($event)"
            >
              <nb-option *ngFor="let t of type.types; let index = index" [value]="index">{{ t.type }}</nb-option>
            </nb-select>
          </div>
          <app-input-field-component
            [value]="internValue.value"
            [name]="'value'"
            [type]="type.types[internValue.typeIndex]"
            (valueChanged)="onChangeValue($event)"
          ></app-input-field-component>
        </nb-accordion-item-body>
      </nb-accordion-item>
    </nb-accordion>
  `,
  styleUrls: ['styles.scss'],
})
export class UnionInputFieldComponent {
  @Input() type!: CommandInputUnionSchema;

  @Input()
  set value(value: UnionValue | undefined) {
    if (!value) {
      this.internValue = {
        typeIndex: 0,
        value: null,
      };
    } else {
      this.internValue = value;
    }
  }

  internValue!: UnionValue;

  @Input() name!: string;
  @Input() accordion = true;
  @Output() valueChanged = new EventEmitter<any>();

  onChangeValue(e: any): void {
    this.internValue.value = e;
    this.valueChanged.emit(this.internValue);
  }

  onChangeType(i: number): void {
    this.internValue.typeIndex = i;
    this.internValue.value = null;
    this.valueChanged.emit(this.internValue);
  }

  constructor() {}
}

interface UnionValue {
  typeIndex: number;
  value: any;
}
