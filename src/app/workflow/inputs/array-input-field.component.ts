import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommandInputArraySchema } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-array-input-field-component',
  templateUrl: './array-input-field.component.html',
  styleUrls: ['styles.scss'],
})
export class ArrayInputFieldComponent {
  @Input() type!: CommandInputArraySchema;

  @Input()
  set value(value: any) {
    if (!value) {
      this.internValue = [];
    } else {
      this.internValue = value;
    }
  }

  internValue: ArrayValue[] = [];

  @Input() name!: string;
  @Input() accordion = true;
  @Output() valueChanged = new EventEmitter<any>();

  onChangeValue(index: number, value: any): void {
    const find = this.internValue.find((v) => v.index === index);
    if (find) {
      find.value = value;
    }
    this.valueChanged.emit(this.internValue);
  }

  addValue(): void {
    this.internValue = [
      ...this.internValue,
      { index: Math.max(...this.internValue.map((v) => v.index), -1) + 1, value: null },
    ];
    this.valueChanged.emit(this.internValue);
  }

  deleteValue(index: number): void {
    this.internValue = this.internValue.filter((v) => v.index !== index);
    this.valueChanged.emit(this.internValue);
  }

  constructor() {}
}

interface ArrayValue {
  index: number;
  value: any;
}
