import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommandInputRecordSchema } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-record-input-field-component',
  templateUrl: './record-input-field.component.html',
  styleUrls: ['styles.scss'],
})
export class RecordInputFieldComponent {
  @Input() type!: CommandInputRecordSchema;

  @Input()
  set value(value: any) {
    if (!value) {
      const values: FieldValue[] = [];
      Object.keys(this.type.fields).map((key) => {
        values.push({
          name: key,
          value: null,
        })
      });
      this.internValue = values
    } else {
      this.internValue = value;
    }
  }

  internValue: FieldValue[] = [];

  @Input() name!: string;
  @Input() accordion = true;
  @Output() valueChanged = new EventEmitter<any>();

  onChangeValue(fieldName: string, value: any): void {
    const field = this.internValue.find(f => f.name === fieldName);
    if (field) {
      field.value = value
    }
    this.valueChanged.emit(this.internValue)
  }

  constructor() {}
}

interface FieldValue {
  name: string;
  value: any;
}
