import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-boolean-input-field-component',
  template: `
    <div class="form-group">
      <nb-checkbox [checked]="value" (checkedChange)="onChange($event)">{{name}}</nb-checkbox>
<!--      <label class="label">{{ name }}</label>-->
<!--      <input nbInput class="input-full-width" type="text" [value]="value" (change)="onChange($event)" />-->
    </div>
  `,
  styleUrls: ['styles.scss'],
})
export class BooleanInputFieldComponent {
  @Input() value!: any;
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();

  onChange(e: boolean): void {
    this.valueChanged.emit(e);
  }

  constructor() {}
}
