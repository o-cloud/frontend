import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommandInputType } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-input-field-component',
  templateUrl: './input-field.component.html',
})
export class InputFieldComponent {
  @Input() type?: CommandInputType;
  @Input() value!: any;
  @Input() name!: string;
  @Output() valueChanged = new EventEmitter<any>();

  constructor() {}

  onValueChanged(value: any): void {
    this.valueChanged.emit(value);
  }
}
