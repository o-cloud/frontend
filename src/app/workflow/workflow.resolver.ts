import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Workflow } from './workflow.model';
import { Observable } from 'rxjs';
import { WorkflowService } from './workflow.service';

@Injectable()
export class WorkflowResolver implements Resolve<Workflow> {
  constructor(private workflowService: WorkflowService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Workflow> {
    const id = route.paramMap.get('id') as string;
    return this.workflowService.getWorkflow(id);
  }
}
