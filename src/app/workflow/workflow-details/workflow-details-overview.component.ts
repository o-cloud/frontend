import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-workflow-details-overview',
  template: `
    <div style="display: grid; grid-template-columns: repeat(3, 1fr); margin-top: 36px; gap: 10px;">
      <app-executions-status-chart-card [workflowId]="workflowId"></app-executions-status-chart-card>
      <app-recent-failures [workflowId]="workflowId"></app-recent-failures>
      <app-executions-duration-chart-card [workflowId]="workflowId"></app-executions-duration-chart-card>
    </div>
  `
})

export class WorkflowDetailsOverviewComponent implements OnInit {
  @Input() workflowId!: string;
  constructor() {
  }

  ngOnInit(): void {
  }
}
