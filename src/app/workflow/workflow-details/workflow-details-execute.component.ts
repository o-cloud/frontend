import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Block, Workflow } from '../workflow.model';
import { WorkflowService } from '../workflow.service';
import { Router } from '@angular/router';
import { InputExecutionType } from '../../@core/models/cwl-document/cwl-command-line-tool';
import { CommandInputType } from '../../@core/models/cwl-document/cwl-types';

@Component({
  selector: 'app-workflow-details-execute',
  templateUrl: 'workflow-details-execute.component.html',
  styles: [
    `
      :host {
        display: block;
        margin-top: 36px;
      }

      :host .block-name {
        margin: 0.4rem 0;
        font-size: 1.2rem;
      }

      :host .button {
        min-width: 85px;
        min-height: 40px;
      }
    `,
  ],
})
export class WorkflowDetailsExecuteComponent implements OnInit, OnChanges {
  @Input() workflow!: Workflow;
  @Output() workflowChange = new EventEmitter<Workflow>();
  error = '';
  parameters: Parameters[] = [];
  isLoading = false;

  constructor(private workflowService: WorkflowService, private router: Router) {}

  submit(): void {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.error = '';
    this.parameters.forEach((p) => {
      const block = this.workflow.blocks.find((b) => b.index === p.block.index);
      if (block) {
        p.inputs.forEach((i) => (block.payload.parameters[i.name] = i.value));
      }
    });
    this.workflowChange.emit(this.workflow);
    this.workflowService.startWorkflow(this.workflow).subscribe(
      (res) => {
        console.log('spinner status on close: ', res.isLoading);
        this.isLoading = false;
        if (res.id) {
          this.router.navigate(['/executions/' + res.id]);
        }
      },
      (e) => {
        console.log(e);
        this.isLoading = false;
        this.error = e.message;
      }
    );
  }

  onValueChanged(input: ParameterInput, value: any): void {
    input.value = value;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.workflow) {
      this.parseWorkflow();
    }
  }

  ngOnInit(): void {
    this.parseWorkflow();
  }

  parseWorkflow(): void {
    this.parameters = [];
    if (!this.workflow.blocks) {
      return;
    }
    for (const block of this.workflow.blocks) {
      const p: Parameters = {
        block,
        inputs: [],
      };
      for (const inputName of Object.keys(block.payload.cwl.inputs)) {
        if (block.payload.cwl.inputs[inputName].inputType === InputExecutionType.execution) {
          p.inputs.push({
            name: inputName,
            type: block.payload.cwl.inputs[inputName].type,
            value: block.payload.parameters[inputName] || null,
          });
        }
      }
      if (p.inputs.length > 0) {
        this.parameters.push(p);
      }
    }
  }
}

interface Parameters {
  block: Block;
  inputs: ParameterInput[];
}

interface ParameterInput {
  type: CommandInputType;
  name: string;
  value: any;
}
