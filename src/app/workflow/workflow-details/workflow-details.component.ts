import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Workflow } from '../workflow.model';
import { Subscription } from 'rxjs';
import { TabsController } from '../../@shared/tabs/tabs-controller';
import { WorkflowService } from '../workflow.service';

type TabName = 'overview' | 'edit' | 'executions' | 'execute';
const allTabs: TabName[] = ['overview', 'edit', 'executions', 'execute'];
function isValidTab(name: string | null): name is TabName {
  if (!name) {
    return false;
  }
  return allTabs.includes(name as TabName);
}

@Component({
  selector: 'app-workflow-details',
  template: `
    <app-page-layout>
      <app-page-layout-header>
        <app-breadcrumbs>
        <app-breadcrumbs-item [routerLink]="['/dashboard']" [queryParams]="{tab: 'dashboard'}">
            <nb-icon icon="home-outline"></nb-icon>
          </app-breadcrumbs-item>
          <app-breadcrumbs-item [routerLink]="['/dashboard']" [queryParams]="{tab: 'workflows'}">
            Workflows
          </app-breadcrumbs-item>
          <app-breadcrumbs-item >
            {{workflow.name}}
          </app-breadcrumbs-item>
        </app-breadcrumbs>
        <app-page-layout-header-title>{{ workflow.name }}</app-page-layout-header-title>
        <app-tabs-group>
          <app-tab *ngFor="let tab of allTabs" [isActive]="selectedTab === tab" (click)="tabController.openTab(tab)">
            {{ tab }}
          </app-tab>
        </app-tabs-group>
      </app-page-layout-header>
      <app-page-layout-body>
        <app-designer-wrapper-component
          (workflowChange)="updateWorkflow($event)"
          [workflow]="workflow"
          *ngIf="selectedTab === 'edit'"
        >
        </app-designer-wrapper-component>

        <app-workflow-details-executions *ngIf="selectedTab === 'executions'" [workflowId]="workflow.id">
        </app-workflow-details-executions>

        <app-workflow-details-execute
          (workflowChange)="updateWorkflow($event)"
          [workflow]="workflow"
          *ngIf="selectedTab === 'execute'"
        >
        </app-workflow-details-execute>

        <app-workflow-details-overview *ngIf="selectedTab === 'overview'" [workflowId]="workflow.id"></app-workflow-details-overview>
      </app-page-layout-body>
    </app-page-layout>
  `,
})
export class WorkflowDetailsComponent implements OnInit, OnDestroy {
  url = this.router.url;
  workflow!: Workflow;
  allTabs = allTabs;
  selectedTab?: TabName;
  subscription = new Subscription();
  tabController: TabsController<TabName>;

  constructor(private route: ActivatedRoute, private router: Router, private workflowService: WorkflowService) {
    route.data.pipe(take(1)).subscribe((d) => (this.workflow = d.workflow));
    this.tabController = new TabsController<TabName>(route, router, 'overview', isValidTab);
    this.subscription.add(
      this.tabController.currentTab().subscribe((t) => {
        this.selectedTab = t;
        this.url = this.router.url;
      })
    );
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.tabController.teardown();
  }

  updateWorkflow(workflow: Workflow): void {
    this.workflowService.updateWorkflow(this.workflow.id, workflow).subscribe(() => {
      this.workflow = workflow;
    });
  }
}
