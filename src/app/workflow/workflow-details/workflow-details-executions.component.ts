import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-workflow-details-executions',
  template: `
    <nb-card id="global-card">
      <nb-card-header>Executions</nb-card-header>
      <nb-card-body>
        <app-executions-component [workflowId]="workflowId"></app-executions-component>
      </nb-card-body>
    </nb-card>
  `,
  styles: [
    `
      :host {
        display: block;
        margin-top: 36px;
      }
    `,
  ],
})
export class WorkflowDetailsExecutionsComponent implements OnInit {
  @Input() workflowId!: string;

  constructor() {}

  ngOnInit(): void {}
}
