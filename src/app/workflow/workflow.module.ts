import { NgModule } from '@angular/core';

import { WorkflowRoutingModule } from './workflow-routing.module';
import {
  NbAccordionModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule, NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule, NbSpinnerModule, NbToggleModule, NbTooltipModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { DesignerWrapperComponent } from './designer-wrapper.component';
import { WorkflowDesignerModule } from '../workflow-designer/workflow-designer.module';
import { WorkflowResolver } from './workflow.resolver';
import { DetailsPanelComponent } from './details-panel.component';
import { CommonModule } from '@angular/common';
import { InputFieldComponent } from './inputs/input-field.component';
import { StringInputFieldComponent } from './inputs/string-input-field.component';
import { BooleanInputFieldComponent } from './inputs/boolean-input-field.component';
import { IntegerInputFieldComponent } from './inputs/integer-input-field.component';
import { FloatInputFieldComponent } from './inputs/float-input-field.component';
import { EnumInputFieldComponent } from './inputs/enum-input-field.component';
import { UnionInputFieldComponent } from './inputs/union-input-field.component';
import { RecordInputFieldComponent } from './inputs/record-input-field.component';
import { ArrayInputFieldComponent } from './inputs/array-input-field.component';
import { CreateWorkflowDialogComponent } from './create-workflow-dialog.component';
import { DateRangeInputFieldComponent } from './inputs/date-range-input-field.component';
import { BoundingBoxInputFieldComponent } from './inputs/bounding-box-input-field.component';
import { EditBoundingBoxDialogComponent } from './inputs/edit-bounding-box-dialog.component';
import { WorkflowDetailsComponent } from './workflow-details/workflow-details.component';
import { SharedModule } from '../@shared/shared.module';
import { WorkflowDetailsOverviewComponent } from './workflow-details/workflow-details-overview.component';
import { WorkflowDetailsExecuteComponent } from './workflow-details/workflow-details-execute.component';
import { WorkflowDetailsExecutionsComponent } from './workflow-details/workflow-details-executions.component';
import { ExecutionsModule } from '../executions/executions.module';
import { MinioNamePipe } from './minio-name.pipe';

@NgModule({
  imports: [
    WorkflowRoutingModule,
    NbCardModule,
    Ng2SmartTableModule,
    FormsModule,
    WorkflowDesignerModule,
    CommonModule,
    NbSelectModule,
    NbInputModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbAlertModule,
    NbAccordionModule,
    NbDialogModule.forChild(),
    NbDatepickerModule,
    NbTooltipModule,
    NbToggleModule,
    NbSpinnerModule,
    SharedModule,
    ExecutionsModule
  ],
  exports: [],
  declarations: [
    DesignerWrapperComponent,
    DetailsPanelComponent,
    MinioNamePipe,
    InputFieldComponent,
    StringInputFieldComponent,
    BooleanInputFieldComponent,
    IntegerInputFieldComponent,
    FloatInputFieldComponent,
    EnumInputFieldComponent,
    UnionInputFieldComponent,
    RecordInputFieldComponent,
    ArrayInputFieldComponent,
    CreateWorkflowDialogComponent,
    DateRangeInputFieldComponent,
    BoundingBoxInputFieldComponent,
    EditBoundingBoxDialogComponent,
    WorkflowDetailsComponent,
    WorkflowDetailsOverviewComponent,
    WorkflowDetailsExecuteComponent,
    WorkflowDetailsExecutionsComponent
  ],
  providers: [WorkflowResolver],
})
export class WorkflowModule {}
