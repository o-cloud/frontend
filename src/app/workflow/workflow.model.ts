import { CwlCommandLineTool } from '../@core/models/cwl-document/cwl-command-line-tool';
import { Favorite } from '../@core/models/favorite.model';
import { ExecutionStatus, TaskExecutionStatus } from '../@core/models/execution.model';

export interface Workflow {
  name: string;
  id: string;
  blocks: Block[];
  runConfig: {
    computeProvider: Favorite,
  }
}

export interface Block {
  name: string;
  index: number;
  payload: {
    cwl: CwlCommandLineTool,
    parameters: {[key: string]: any}
  }
}

export interface AllWorkflowResponseContainer {
  workflows: AllWorkflowResponseItem[]
}

export interface AllWorkflowResponseItem {
  id: string;
  name: string;
  executions: {
    id: string;
    namespace: string;
    argoName: string;
    workflowName: string;
    status: ExecutionStatus;
    workflowId: string;
    startedAt?: string;
    finishedAt?: string;
    tasks: {
      name: string;
      blockId: string;
      status: TaskExecutionStatus;
      startedAt?: string;
      finishedAt?: string;
      costLimit: number;
      podName: string;
    }[]
  }[]
}
