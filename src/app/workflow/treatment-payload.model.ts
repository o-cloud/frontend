import { BlockPayload } from '../workflow-designer/models/block.model';
import { CwlCommandLineTool } from '../@core/models/cwl-document/cwl-command-line-tool';
import { Favorite } from '../@core/models/favorite.model';

export interface TreatmentPayload extends BlockPayload {
  kind: 'treatment-payload';
  cwl: CwlCommandLineTool;
  id: number;
  parameters: { [key: string]: any };
  providerType: string;
  endpointLocation: string;
  computeProvider: Favorite;
  s3Provider: Favorite;
  clusterName: string;
  costLimitBlock: number;
  costLimitCluster: number;
}
