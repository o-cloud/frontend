import { Component, OnDestroy, OnInit } from '@angular/core';
import { BaseDetailsPanelComponent } from '../workflow-designer/details-panel/base-details-panel-component.component';
import { TreatmentPayload } from './treatment-payload.model';
import { cloneDeep } from 'lodash';
import { Input as CwlInput, InputExecutionType } from '../@core/models/cwl-document/cwl-command-line-tool';
import { Favorite } from '../@core/models/favorite.model';

@Component({
  selector: 'app-details-panel-component',
  template: `
    <div class="form-group">
      <label class="label">Runs on</label>
      <nb-select class="input-full-width" [selected]="selectedComputeProvider" (selectedChange)="onComputeProviderChanged($event)">
        <nb-option *ngFor="let p of computeProviders"
                   [value]="p">{{p.isLocal ? "Local (" + p.originClusterName + ")" : p.originClusterName}}</nb-option>
      </nb-select>
    <div class="form-group">
      <label class="label">Cost limit on target cluster</label>
      <input nbInput class="input-full-width" type="number" [value]="costLimitCluster" (change)="onCostLimitClusterChanged($event)"/>
    </div>
    <div class="form-group" *ngIf="block.outputPorts.length">
      <label class="label">Saves output to</label>
      <nb-select class="input-full-width"  [selected]="selectedS3Provider" (selectedChange)="onS3Changed($event)">
        <nb-option *ngFor="let p of s3Providers"
                   [value]="p">{{p | appMinioName}}</nb-option>
      </nb-select>
    </div>
    <app-input-field-component
      *ngFor="let v of values"
      [name]="v.name"
      [type]="v.input.type"
      [value]="v.value"
      (valueChanged)="onValueChanged(v, $event)"
    ></app-input-field-component>

    <div class="form-group">
      <label class="label">Cost limit</label>
      <input nbInput class="input-full-width" type="number" [value]="costLimitBlock" (change)="onCostLimitChanged($event)"/>
    </div>
  `,
  styles: [
    `
      h2 {
        font-size: 1em;
      }

      label {
        display: inline-block;
        margin-bottom: 0.5em;
        font-size: 0.9em !important;
      }

      .input-full-width {
        display: block;
      }

      .form-group {
        margin-bottom: 1em;
      }
    `
  ]
})
export class DetailsPanelComponent extends BaseDetailsPanelComponent implements OnInit, OnDestroy {
  payload!: TreatmentPayload;
  values: ValueUpdater<any>[] = [];
  computeProviders: Favorite[] = [];
  s3Providers: Favorite[] = []
  selectedS3Provider: Favorite | undefined = undefined;
  selectedComputeProvider: Favorite | undefined = undefined;
  costLimitBlock = 0
  costLimitCluster = 0

  ngOnInit(): void {
    this.payload = cloneDeep(this.block.payload as TreatmentPayload);
    this.costLimitBlock = this.payload.costLimitBlock
    this.s3Providers = this.computeProviders.filter(c => c.metadata.s3 && c.metadata.s3.accessKey && c.metadata.s3.secretKey)
    this.selectedS3Provider = this.s3Providers.find(p => p.providerId === this.payload.s3Provider.providerId)
    this.selectedComputeProvider = this.computeProviders.find(p => p.providerId === this.payload.computeProvider.providerId)
    this.costLimitCluster = this.payload.computeProvider.costLimitCluster
    if (this.payload.cwl.inputs) {
      for (const key in this.payload.cwl.inputs) {
        if (this.payload.cwl.inputs[key].inputType === InputExecutionType.definition) {
          const updater: ValueUpdater<string> = {
            name: key,
            input: this.payload.cwl.inputs[key],
            value: this.payload.parameters[key] || null,
          };
          this.values.push(updater);
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.sendValue();
    super.ngOnDestroy();
  }

  onValueChanged(updater: ValueUpdater<any>, value: any): void {
    console.log('new value',  value, updater)
    updater.value = value;
    this.sendValue();
  }

  onS3Changed(s3: Favorite): void {
    this.selectedS3Provider = s3
    this.sendValue()
  }

  onComputeProviderChanged(cp: Favorite): void {
    this.selectedComputeProvider = cp
    this.sendValue()
    // Reload the compute provider cost limit
    this.costLimitCluster = this.payload.computeProvider.costLimitCluster
  }

  onCostLimitClusterChanged(cl: Event): void {
    this.costLimitCluster = parseFloat((cl.target as HTMLInputElement).value)
    this.sendValue()
  }
  onCostLimitChanged(e: Event): void {
    this.costLimitBlock = parseFloat((e.target as HTMLInputElement).value)
    this.sendValue()
  }

  sendValue(): void {
    if (this.payload) {
      this.payload.costLimitBlock = this.costLimitBlock
      this.payload.computeProvider.costLimitCluster = this.costLimitCluster
      for (const value of this.values.filter((v) => v.value)) {
        this.payload.parameters[value.name] = value.value;
      }
      if (this.selectedComputeProvider) {
        this.payload.computeProvider = this.selectedComputeProvider
      }
      if (this.selectedS3Provider) {
        this.payload.s3Provider = this.selectedS3Provider
      }
      this.updatePayload.emit({ b: this.block, p: this.payload });
    }
  }
}

interface ValueUpdater<T> {
  name: string;
  input: CwlInput;
  value: T;
}
