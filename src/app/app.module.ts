import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbDatepickerModule, NbMenuModule, NbSidebarModule } from '@nebular/theme';
import { CoreModule } from './@core/core.module';
import { NgxAdminModule } from './@ngx-admin/ngx-admin.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResourcesModule } from './resources/resources.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ServicesProviderModule } from './services-provider/services-provider.module';
import { WorkflowDesignerModule } from './workflow-designer/workflow-designer.module';
import { WorkflowModule } from './workflow/workflow.module';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { environment } from 'src/environments/environment';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { PrometheusModule } from './prometheus/prometheus.module';
import { ExecutionsModule } from './executions/executions.module';
import { DataProvidersModule } from './data-providers/data-providers.module';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { StorageProvidersModule } from './storage-providers/storage-providers.module';
import { ComputingProvidersModule } from './computing-providers/computing-providers.module';
import { SharedModule } from './@shared/shared.module';
import { DynamicConfigurationService } from './@core/services/dynamic-configuration.service';
import { PricingModule } from './pricing/pricing.module';


@NgModule({
  declarations: [AppComponent],
  imports: [
    KeycloakAngularModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgxAdminModule.forRoot(),
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbDatepickerModule.forRoot(),
    CoreModule,
    ResourcesModule,
    DashboardModule,
    PrometheusModule,
    PricingModule,
    WorkflowDesignerModule,
    FormsModule,
    MonacoEditorModule.forRoot(),
    WorkflowModule,
    DataProvidersModule,
    StorageProvidersModule,
    ServicesProviderModule,
    ComputingProvidersModule,
    NbEvaIconsModule,
    ExecutionsModule,
    SharedModule,
    NbDatepickerModule.forRoot(),
    AppRoutingModule, // must be the last import
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: (confService: DynamicConfigurationService) => () => confService.load(),
    multi: true,
    deps: [DynamicConfigurationService] }],
  bootstrap: [AppComponent],
})
export class AppModule {}
