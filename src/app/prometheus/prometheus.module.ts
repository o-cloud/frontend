import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbButtonModule, NbCalendarRangeModule, NbCardModule, NbDatepickerModule, NbIconModule, NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PrometheusRoutingModule as PrometheusRoutingModule } from './prometheus-routing.module';
import {  PrometheusCostComponent } from './prometheus.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {ChartComponent} from './chart.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../@core/core.module';
import { SharedModule } from '../@shared/shared.module';
import { DateRangePrometheusComponent } from './prometheus-date-range';

@NgModule({
  imports: [
    PrometheusRoutingModule,
    NbSelectModule,
    NbCardModule,
    CommonModule,
    Ng2SmartTableModule,
    NgxChartsModule,
    FormsModule,
    CoreModule,
    SharedModule,
    NbButtonModule,
    NbIconModule,
    NbDatepickerModule,
    NbCalendarRangeModule
  
  ],
  declarations: [PrometheusCostComponent, ChartComponent, DateRangePrometheusComponent],
  exports: [],
})
export class PrometheusModule {}
