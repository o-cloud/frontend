import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PrometheuscostCluster, ResponsePrometheus } from '../@core/models/prometheus.model';

export interface PrometheuscostResponse {
  nodeCosts: PrometheuscostCluster;
}

@Injectable({
  providedIn: 'root',
})
export class PrometheusService {
  constructor(private http: HttpClient) {}

  private REST_API_SERVER = environment.urlJsonServer;

  public getNodCost(queryParams: HttpParams): Observable<any> {
    return this.http.get<ResponsePrometheus>(`${this.REST_API_SERVER}/cost/cluster/remote/metrics`, { params: queryParams });
  }

  public getWorkflows(queryParams: HttpParams): Observable<any> {
    return this.http.get<PrometheuscostResponse>(`${this.REST_API_SERVER}/workflows/users`, { params: queryParams });
  }
}
