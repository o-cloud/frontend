import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrometheusCostComponent } from './prometheus.component';

const routes: Routes = [
  {
    path: 'prometheus',
    component: PrometheusCostComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrometheusRoutingModule {}

