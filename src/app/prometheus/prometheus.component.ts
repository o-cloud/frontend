
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbCalendarRange } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { CharCost, PrometheuscostCluster, SerieChart } from '../@core/models/prometheus.model';
import { TabsController } from '../@shared/tabs/tabs-controller';
import { DateRangePrometheusComponent } from './prometheus-date-range';
import { PrometheusService } from './prometheus-service';
import { MetricType, TimeType, StepType} from './prometheus.models';

type TabName = 'filters' | 'detailed costs';
const allTabs: TabName[] = ['filters', 'detailed costs'];
function isValidTab(name: string | null): name is TabName {
  if (!name) {
    return false;
  }
  return allTabs.includes(name as TabName);
}

@Component({
  selector: 'app-prometheus',
  templateUrl: './prometheus.component.html',
  styleUrls: ['./prometheus.component.scss'],
})
export class PrometheusCostComponent implements OnInit {
  stepValue = new Map<string, any>([
        ["Minute", 60],
        ["Heure",3600 ],
        ["Day", 86400],
        ["Mois", 2592000]      
    ]);
  
  valuerange :any
  range: DateRangePrometheusComponent = new DateRangePrometheusComponent;
  allTabs = allTabs;
  selectedTab?: TabName;
  typeType = TimeType;
  metricType = MetricType;
  stepType= StepType;
  multi:  SerieChart[] = [];
  multiTable: any;
  nodecosts: PrometheuscostCluster[] = [];
  workflows: any;
  clusters: any;
  selectedWorkflow: any;
  selectedCluster: any;
  selectedMetric: any;
  stepInput: any;
  pods: any;
  selectedPod: any;


  initdate: any;
  endate: any;
  initdateEnd!: Date;
  initdateStart!: Date;
  timestampStart: any;
  timestampEnd: any;
  startdate: any;
  metric: any;
  step: any;

  subscription = new Subscription();
  tabController: TabsController<TabName>;

  settings = {
    mode: 'external',
    noDataMessage: 'No data found',
    columns: {
      name: {
        title: 'Time',
      },
      value: {
        title: 'Consumation',
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      positon: 'right',
      columnTitle: '',
    },
    hideSubHeader: true,
  };
 
  hasData: boolean = false;
 

  constructor(private prometheusService: PrometheusService, private router: Router, private route: ActivatedRoute) {
    this.tabController = new TabsController<TabName>(route, router, 'filters', isValidTab);
    this.subscription.add(this.tabController.currentTab().subscribe((t) => (this.selectedTab = t)));
  }

  ngOnInit(): void {
    const params = new HttpParams();
    this.prometheusService.getWorkflows(params).subscribe((data) => {
      this.workflows = data;
      if (data && data.length) {
        this.initdateEnd = new Date()
        this.initdateStart= new Date()
        this.initdateStart= new Date(this.initdateStart.setDate(this.initdateStart.getDate() -1))

        this.timestampStart = convertTimestamp(this.initdateStart)
        this.timestampEnd= convertTimestamp(this.initdateEnd)
    
        this.selectedWorkflow = data[0].id;
        this.clusters = data[0]["cluster"];
        this.selectedCluster = data[0].cluster[0]
        this.selectedMetric = MetricType.Cpusage;
        this.stepInput = convetToStep(StepType.Minute, this.stepValue);
        let paramsCluster = new HttpParams();
        paramsCluster = paramsCluster.append('idWorkflow', this.selectedWorkflow);
        paramsCluster = paramsCluster.append('end',this.timestampEnd);
        paramsCluster = paramsCluster.append('start', this.timestampStart);
        paramsCluster = paramsCluster.append('namespace', "workflow");
        paramsCluster = paramsCluster.append('step', this.stepInput);
        paramsCluster = paramsCluster.append('metric', this.selectedMetric);

        this.prometheusService.getNodCost(paramsCluster).subscribe((cost) => {
          this.nodecosts = cost;
          this.pods = extractPods(this.nodecosts)
          console.log("pods ", this.pods )
          if(this.pods){
          this.selectedPod = this.pods[0]
          this.multi = buildMulti(cost, this.selectedCluster, this.selectedPod, this.selectedMetric);
          this.multiTable= this.multi[0]["series"]
          console.log(this.multi);
          this.hasData=true;
          }
        });
      }else{
        this.hasData=false;
      }
    });
  }
  searchResult(): void {
    this.multi = [];
    this.hasData=false;
    console.log(this.valuerange)
    this.startdate= convertTimestamp(this.range.date.start)
    this.endate=convertTimestamp(this.range.date.end)

    
    let paramsCluster = new HttpParams();
    paramsCluster = paramsCluster.append('idWorkflow', this.selectedWorkflow);
    paramsCluster = paramsCluster.append('end', this.endate);
    paramsCluster = paramsCluster.append('start', this.startdate);
    paramsCluster = paramsCluster.append('namespace', "workflow");
    paramsCluster = paramsCluster.append('step',convetToStep(this.stepInput, this.stepValue))
    paramsCluster = paramsCluster.append('metric', this.selectedMetric);

    this.prometheusService.getNodCost(paramsCluster).subscribe((data) => {
      this.nodecosts = data;
      this.pods = extractPods(this.nodecosts)
      this.multi = buildMulti(data, this.selectedCluster, this.selectedPod, this.selectedMetric);
      this.multiTable= this.multi[0]["series"]
      console.log("multi ", this.multiTable)
      this.hasData=true;
    });
  }

  searchCluster(): void {
    let paramsCluster = new HttpParams();
    paramsCluster = paramsCluster.append('idWorkflow', this.selectedWorkflow);
    paramsCluster = paramsCluster.append('end', this.endate);
    paramsCluster = paramsCluster.append('start', this.startdate);
    paramsCluster = paramsCluster.append('namespace', "workflow");
    paramsCluster = paramsCluster.append('step', this.step);
    paramsCluster = paramsCluster.append('metric', this.metric);
    this.prometheusService.getNodCost(paramsCluster).subscribe((data) => {
      this.nodecosts = data;
      this.selectedCluster = data[0].cluster;
    });
  }

  changeRange(newRange: NbCalendarRange<any>){
    this.range.date.end=newRange.end
    this.range.date.start=newRange.start
  }
}
function buildMulti(data: any, selectedCluster: any, selectedPod: any, selectMetric: any): any {
  let results: CharCost[] = [];
  let serieChart: SerieChart[]= [];

  for (const metricValue of data) {
    const arrayMetric = metricValue["pod"]["data"]["result"]
    if(arrayMetric!= null){
    for (const metric of arrayMetric) {
      if (metric["metric"]["pod"] === selectedPod || selectedPod===undefined)
        for (const value of metric["values"]) {
          const temp: CharCost = {
            value: value[1],
            name: convertTodate(value[0]),
          };
          results.push(temp)
        }
    }
    }
  }
  const serie: SerieChart = {
      name: selectMetric,
      series: results
  }
  serieChart.push(serie);
  console.log("serie data ", serieChart)
  return serieChart;
}



function convertTimestamp(date: Date): number {
  date.setMilliseconds(0)
  return date.getTime() / 1000;
}

function convertTodate(timestamp:any): string{
  return new Date(timestamp*1000).toISOString()
}
function extractPods(nodecosts: any): any {
  let arrayPods: any[] = []
  for (const metricValue of nodecosts) {
    let arrayMetric = metricValue["pod"]["data"]["result"]
    if(arrayMetric!= null){
    for (const metric of arrayMetric)
      arrayPods.push(metric["metric"]["pod"])
  }
}
  return arrayPods
}
function convetToStep(step: StepType, mapType: Map<string, any>){
  return mapType.get(step)
}

