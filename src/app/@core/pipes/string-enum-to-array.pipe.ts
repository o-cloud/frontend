import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'stringEnumToArray' })
export class StringEnumToArrayPipe implements PipeTransform {
  transform(value: any): string[] {
    return Object.keys(value).map(v => value[v])
  }
}
