import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Identity } from '../models/identity.model';
import { Peer } from '../models/peer.model';
import { Swarmpool } from '../models/swarmpools.model';

export interface PeersResponse {
  peers: Peer[];
}

export interface IdentitiesResponse {
  identities: Identity[];
}

export interface IdentityResponse {
  identity: Identity;
}

export interface SwarmpoolsResponse {
  swarmpools: Swarmpool[];
}

@Injectable({
  providedIn: 'root',
})
export class DiscoveryService {
  constructor(private http: HttpClient) {}

  private REST_API_SERVER = environment.urlDiscovery;

  public getPeers(): Observable<Peer[]> {
    return this.http.get<PeersResponse>(`${this.REST_API_SERVER}/peers`).pipe(map((data) => data.peers));
  }

  public getIdentity(): Observable<Identity> {
    return this.http.get<IdentityResponse>(`${this.REST_API_SERVER}/identity`).pipe(map((data) => data.identity));
  }

  public addIdentity(swarmpool: string, identity: Identity): Observable<Identity> {
    return this.http
      .post<IdentityResponse>(`${this.REST_API_SERVER}/swarmpools/${swarmpool}/identity`, { identity })
      .pipe(map((data) => data.identity));
  }

  public updateIdentity(swarmpool: string, identity: Identity): Observable<Identity> {
    return this.http
      .put<IdentityResponse>(`${this.REST_API_SERVER}/swarmpools/${swarmpool}/identity`, { identity })
      .pipe(map((data) => data.identity));
  }

  public deleteIdentity(swarmpool: string): Observable<any> {
    return this.http.delete<any>(`${this.REST_API_SERVER}/swarmpools/${swarmpool}/identity`);
  }

  public getSwarmpools(): Observable<Swarmpool[]> {
    return this.http.get<SwarmpoolsResponse>(`${this.REST_API_SERVER}/swarmpools`).pipe(map((data) => data.swarmpools));
  }
}
