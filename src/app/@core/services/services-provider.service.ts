import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Service } from '../models/service.model';

export interface ServicesResponse {
  services: Service[];
}

@Injectable({
  providedIn: 'root',
})
export class ServicesProviderService {
  constructor(private http: HttpClient) {}

  private REST_API_SERVER = environment.urlServiceProvider;

  public getServices(): Observable<Service[]> {
    return this.http.get<ServicesResponse>(`${this.REST_API_SERVER}/services`).pipe(map((data) => data.services));
  }

  public deleteService(name: string, version: string): Observable<void> {
    return this.http.delete<void>(`${this.REST_API_SERVER}/services/${name}/${version}`);
  }

  public addService(service: Service): Observable<any> {
    return this.http.post<any>(`${this.REST_API_SERVER}/services`, { service });
  }

  public updateService(name: string, version: string, service: Service): Observable<any> {
    return this.http.put<any>(`${this.REST_API_SERVER}/services/${name}/${version}`, { service });
  }
}
