import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Execution, ExecutionStatusCount } from '../models/execution.model';

@Injectable({ providedIn: 'root' })
export class ExecutionsService {
  private EXECUTION_BASE_ROUTE = environment.urlJsonServer + '/executions'

  constructor(private httpClient: HttpClient) {
  }

  getAllExecutions({workflowId, startTime}: {workflowId?: string, startTime?: string} = {}): Observable<Execution[]> {
    const params: any = {}
    if (workflowId && workflowId.length) {
      params.workflow = workflowId
    }
    if (startTime && startTime.length) {
      params.startTime = startTime
    }
    return this.httpClient.get<Execution[]>(this.EXECUTION_BASE_ROUTE, {params})
  }

  getExecutionById(id: string): Observable<Execution> {
    return this.httpClient.get<Execution>(this.EXECUTION_BASE_ROUTE + '/' + id)
  }

  getExecutionStatusCount({workflowId, startTime}: {workflowId?: string, startTime?: string} = {}): Observable<ExecutionStatusCount> {
    const params: any = {}
    if (workflowId && workflowId.length) {
      params.workflow = workflowId
    }
    if (startTime && startTime.length) {
      params.startTime = startTime
    }
    return this.httpClient.get<ExecutionStatusCount>(this.EXECUTION_BASE_ROUTE + '/statusCount', {params})
  }
}
