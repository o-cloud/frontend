import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Favorite } from '../models/favorite.model';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  constructor(private http: HttpClient) {
  }

  private REST_API_SERVER = environment.urlJsonServer;

  public getFavorites(): Observable<Favorite[]> {
    return this.http.get<Favorite[]>(this.REST_API_SERVER + '/favorites/');
  }

  public deleteFavorite(providerId: string): Observable<void> {
    return this.http.delete<void>(`${this.REST_API_SERVER}/favorites/${providerId}`);
  }

  public addFavorite({
                       providerId,
                       name,
                       providerType,
                       description,
                       endpointLocation,
                       workflowStructure,
                       originClusterName,
                       isLocal,
                       metadata
                     }: {
    providerId: string;
    name: string;
    providerType: string;
    description: string;
    endpointLocation: string;
    workflowStructure: string;
    originClusterName: string;
    isLocal: boolean;
    metadata: any;
  }): Observable<Favorite> {
    return this.http.post<Favorite>(`${this.REST_API_SERVER}/favorites/`, {
      providerId,
      name,
      providerType,
      description,
      endpointLocation,
      workflowStructure,
      originClusterName,
      isLocal,
      metadata
    });
  }
}
