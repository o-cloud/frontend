export interface Execution {
  id: string;
  namespace: string;
  argoName: string;
  workflowName: string;
  status: ExecutionStatus;
  workflowId: string;
  startedAt?: string;
  finishedAt?: string;
  tasks: Task[];
}

export interface Task {
  name: string
  blockId: number
  status: TaskExecutionStatus
  startedAt?: string;
  finishedAt?: string;
  logs: {
    [container: string]: string[]
  }
  nodeStatus: {
    [container: string]: string
  }

  computeProvider: {
    originClusterName: string,
    isLocal: boolean
  }
}

export interface ExecutionStatusCount {
  succeeded: number,
  failed: number,
  running: number
}

export interface ExecutionChartDuration {
  id: string,
  duration: number,
  status: string
}

export type ExecutionStatus = 'Created' | 'Pending' | 'Running' | 'Succeeded' | 'Failed' | 'Error'
export type TaskExecutionStatus = 'Pending' | 'Running' | 'Succeeded' | 'Skipped' | 'Failed' | 'Error' | 'Omitted' | 'Not Started'
