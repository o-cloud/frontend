export interface Peer {
    id: string,
    network: string,
    name: string,
    url: string,
    description: string,
    categories: string[],
    status: string,
}
