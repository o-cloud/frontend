import { CommandInputType, CommandOutputType } from './cwl-types';

export interface CwlCommandLineTool {
  label: string;
  class: string;
  id: string;
  stdin: string;
  stdout: string;
  stderr: string;
  doc: string;
  successCodes: string;
  temporaryFailCodes: number[];
  permanentFailCodes: number[];
  baseCommand: string[];
  arguments: CommandLineBinding[];
  inputs: { [key: string]: Input };
  outputs: { [key: string]: Output };
  requirements: Requirement[];
}

export interface CommandLineBinding {
  position: number;
  prefix: string;
  separate: boolean;
  itemSeparator: string;
  valueFrom: string;
  shellQuote: boolean;
}

export interface Input {
  label: string;
  doc: string;
  streamable: boolean;
  loadContents: boolean;
  inputBinding: CommandLineBinding;
  format: string[];
  type: CommandInputType;
  inputType:
    | typeof InputExecutionType.execution
    | typeof InputExecutionType.pipeline
    | typeof InputExecutionType.definition
    | typeof InputExecutionType.orchestrator;
}

export interface Output {
  label: string;
  streamable: boolean;
  doc: string;
  format: string[];
  binding: CommandOutputBinding;
  type: CommandOutputType;
}

export interface CommandOutputBinding {
  loadContent: boolean;
  glob: string[];
  outputEval: string;
}

export const InputExecutionType = {
  pipeline: 'pipeline',
  definition: 'definition',
  execution: 'execution',
  orchestrator: 'orchestrator',
};

export type Requirement = DockerRequirement;

export interface DockerRequirement {
  type: 'DockerRequirement';
  dockerPull: string;
  dockerLoad: string;
  dockerFile: string;
  dockerImport: string;
  dockerImageId: string;
  dockerOutputDirectory: string;
}
