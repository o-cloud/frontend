import { CwlCommandLineTool, Input, InputExecutionType } from './cwl-command-line-tool';

export default {
  countPipelineOutputs: (c: CwlCommandLineTool): number => {
    if (!c.outputs) {
      return 0;
    }
    return Object.keys(c.outputs).length;
  },
  countPipelineInputs: (c: CwlCommandLineTool): number => {
    if (!c.inputs) {
      return 0;
    }
    const shouldBeDisplayed = (i: Input): boolean => {
      return i.inputType === InputExecutionType.pipeline;
    };
    return Object.keys(c.inputs).filter((k) => shouldBeDisplayed(c.inputs[k])).length;
  },
};
