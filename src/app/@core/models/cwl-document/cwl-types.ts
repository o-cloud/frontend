import { CommandLineBinding, CommandOutputBinding } from './cwl-command-line-tool';

export interface DateRangeType {
  type: 'dateRange'
}

export interface BoundingBoxType {
  type: 'boundingBox'
}

export interface BooleanType {
  type: 'boolean'
}

export interface IntegerType {
  type: 'integer'
}

export interface LongType {
  type: 'long'
}

export interface FloatType {
  type: 'float'
}

export interface DoubleType {
  type: 'double'
}

export interface StringType {
  type: 'string'
}

export interface FileType {
  type: 'file'
}

export interface DirectoryType {
  type: 'directory'
}

export interface CommandInputEnumSchema {
  type: 'enum'
  symbols: string[];
  label: string;
  doc: string;
  name: string;
  inputBinding: CommandLineBinding;
}

export interface CommandOutputEnumSchema {
  type: 'enum'
  symbols: string[];
  label: string;
  doc: string;
  name: string;
}

export interface CommandInputArraySchema {
  type: 'array'
  label: string;
  doc: string;
  name: string;
  items: CommandInputType;
  inputBinding: CommandLineBinding;
}

export interface CommandOutputArraySchema {
  type: 'array'
  label: string;
  doc: string;
  name: string;
  items: CommandOutputType;
}

export interface CommandInputRecordSchema {
  inputBinding: CommandLineBinding;
  fields: {[key: string]: CommandInputRecordField}
  label: string
  doc: string
  name: string
  type: 'record'
}

export interface CommandOutputRecordSchema {
  fields: {[key: string]: CommandOutputRecordField}
  label: string
  doc: string
  name: string
  type: 'record'
}

export interface CommandInputRecordField {
  doc: string;
  label: string;
  streamable: boolean;
  format: string[];
  type: CommandInputType;
  loadContents: boolean;
  inputBinding: CommandLineBinding;
}

export interface CommandOutputRecordField {
  doc: string;
  label: string;
  streamable: boolean;
  format: string[];
  type: CommandOutputType;
  outputBinding: CommandOutputBinding;
}

export interface CommandInputUnionSchema {
  type: 'union';
  types: CommandInputType[];
}

export interface CommandOutputUnionSchema {
  type: 'union';
  types: CommandOutputType[];
}

type Scalar =
  | BooleanType
  | IntegerType
  | LongType
  | FloatType
  | DoubleType
  | StringType
  | FileType
  | DirectoryType
  | DateRangeType
  | BoundingBoxType

export type CommandOutputType =
  | Scalar
  | CommandOutputArraySchema
  | CommandOutputEnumSchema
  | CommandOutputRecordSchema
  | CommandOutputUnionSchema

export type CommandInputType =
  | Scalar
  | CommandInputArraySchema
  | CommandInputEnumSchema
  | CommandInputRecordSchema
  | CommandInputUnionSchema
