export interface PricingResponse {
 pricing: Pricing[];
}

/*export interface Pricing {
  idPrice: string;
  providerIp: string;
  resourceRates: ResourceRate[];
  dataRates: DataRate[];
  serviceRates: ServiceRate[];
}

export interface ResourceRate{
cpu: string
memory: string
networkTransmitted: string
networkReceived: string
}

export interface DataRate{
dataName: string
dataRates: string
}

export interface ServiceRate{
serviceName: string
serviceRates: string
}
*/
export interface TablePricing {
  idpartenaire: string,
  idprice: string,
  type: string, 
  name: string,
  value: number
}


export interface Pricing {
  name: string,
  value: number,
  type: string,
  idprice: string
}
export interface PricingRemote{
  name: string,
  value: number,
  type: string,
  idprice: string,
  origin : string
}

export interface CreatePricing {
  name: string,
  value: number,
  type: string
}

export interface EditPricing {
  name: string,
  value: number,
  type: string
}
