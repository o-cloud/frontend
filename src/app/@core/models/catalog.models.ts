import { CwlCommandLineTool } from './cwl-document/cwl-command-line-tool';

export interface CatalogueResponse {
  originClusterName: string;
  description: string;
  endpointLocation: string;
  name: string;
  providerId: string;
  providerType: string;
  version: string;
  workflowStructure: string;
  isFavorite: boolean;
  isLocal: boolean;
  metadata: any;
}

export interface CatalogueSearchResult {
  name: string;
  originClusterName: string;
  providerId: string;
  description: string;
  providerType: string;
  isFavorite: boolean;
  endpointLocation: string;
  workflowStructure: string;
  isLocal: boolean;
  metadata: any;
  version: string;
}

export interface Provider {
  name: string;
  providerId: string;
  description: string;
  type: ProviderType;
  cwl: CwlCommandLineTool;
  endpointLocation: string;
  clusterName: string;
}

export enum ProviderType {
  Data = 'data',
  Service = 'service',
  Computing = 'computing',
  Storage = 'storage',
}
