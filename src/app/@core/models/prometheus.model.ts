export interface ResponsePrometheus {
 prometheuscostCluster: PrometheuscostCluster[];
}

export interface PrometheuscostCluster {
  urlcluster: string;
  datestart: string
  datend: string
  step: number
  limitcluster : number
  pod : NodeCost []
  islimitexceed : boolean
}

export interface NodeCost {
  namepod: string;
  coutmoyen: number;
  coutotalpod: number;
  limitpod: number;
  islimitexceed: boolean;
  metrics: CharCost []
}

export interface CharCost {
  name: string;
  value: number;
}

export interface SerieChart {
  name: string;
  series: CharCost[];
}
