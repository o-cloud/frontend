export interface Swarmpool {
    network: string,
    name: string,
    entrypoint: string,
}
