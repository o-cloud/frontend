export interface Identity {
  name: string;
  url: string;
  description: string;
  categories: string[];
  network: string;
  swarm: Swarmpool;
}

export interface Swarmpool {
  network: string;
  name: string;
}
