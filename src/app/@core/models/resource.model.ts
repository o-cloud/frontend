export interface Resource {
  name: string;
  id: number;
  keywords: string[];
  clusterId: number;
  confidentiality: string;
  type: ResourceType;
  favorite: boolean;
}

export enum ResourceType {
  Data,
  Treatment,
  Storage,
  Compute,
}
