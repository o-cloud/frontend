import { CwlCommandLineTool } from './cwl-document/cwl-command-line-tool';
import { ProviderType } from './catalog.models';

export interface Favorite {
  name: string;
  providerId: string;
  providerType: ProviderType;
  description: string;
  endpointLocation: string;
  workflowStructure: CwlCommandLineTool;
  originClusterName: string;
  isLocal: boolean;
  metadata: {[key: string]: any}
  version: string
  costLimitCluster: number
}
