export interface Cluster {
  name: string;
  id: number;
  url: string;
  description: string;
  category: string;
}
