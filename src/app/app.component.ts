import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { KeycloakService } from 'keycloak-angular';
import { DynamicConfigurationService } from './@core/services/dynamic-configuration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  menu: NbMenuItem[] = [
    {
      title: 'Dashboard',
      link: '/dashboard',
      queryParams: {
        tab: 'dashboard',
      },
      icon: 'home-outline',
    },
    {
      title: 'Resources',
      link: '/resources',
      icon: 'book-open-outline',
    },
    {
      title: 'Cost',
      link: '/prometheus',
      icon: 'globe-outline',
    },
    {
      title: 'Pricing',
      link: '/price',
      icon: 'shopping-cart-outline',
    },

    {
      title: 'Local resources',
      icon: 'shopping-bag-outline',
      children: [
        {
          title: 'Data',
          link: '/data-providers',
          icon: 'log-out-outline',
        },
        {
          title: 'Storages',
          link: '/storage-providers',
          icon: 'log-in-outline',
        },
        {
          title: 'Services',
          link: '/services-provider',
          icon: 'sync-outline',
        },
        {
          title: 'Hardware',
          link: '/computing-providers',
          icon: 'flash-outline',
        },
      ],
    },
  ];

  constructor(
    protected readonly keycloak: KeycloakService,
    private confService: DynamicConfigurationService
  ) {}

  async ngOnInit() {
    if(this.confService.authConfiguration.enableKeycloakAuth){
      if (!await this.keycloak.isLoggedIn()) {
        this.keycloak.login()
      }
    }
  }
}
