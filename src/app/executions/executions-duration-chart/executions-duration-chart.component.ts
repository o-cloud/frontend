import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  ApexChart,
  ChartComponent,
  ApexAxisChartSeries,
  ApexDataLabels,
  ApexPlotOptions,
  ApexYAxis,
  ApexXAxis,
  ApexFill,
  ApexTooltip,
  ApexGrid,
} from 'ng-apexcharts';
import { ExecutionChartDuration } from 'src/app/@core/models/execution.model';
import { formatDuration } from '../utils';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  grid: ApexGrid;
  tooltip: ApexTooltip;
};

@Component({
  selector: 'app-executions-duration-chart',
  template: `
    <div id="chart" *ngIf="!isLoading">
      <apx-chart
        [series]="chartOptions.series"
        [chart]="chartOptions.chart"
        [dataLabels]="chartOptions.dataLabels"
        [plotOptions]="chartOptions.plotOptions"
        [yaxis]="chartOptions.yaxis"
        [fill]="chartOptions.fill"
        [xaxis]="chartOptions.xaxis"
        [tooltip]="chartOptions.tooltip"
        [grid]="chartOptions.grid"
      ></apx-chart>
    </div>
  `,
})
export class ExecutionsDurationChartComponent implements OnInit, OnChanges {
  @Input()
  executions: ExecutionChartDuration[] = [];
  @Input()
  isLoading = false;

  @ViewChild('chart') chart!: ChartComponent;
  chartOptions: ChartOptions;

  constructor(private router: Router) {
    const self = this;
    this.chartOptions = {
      series: [
        {
          name: 'Execution duration',
          data: [10, 20, 30],
        },
      ],
      chart: {
        type: 'bar',
        height: 350,
        events: {
          click: (event: any, chart: any, config: any) => {
            if (config.dataPointIndex !== -1) {
              self.router.navigate(['/executions/' + self.executions[config.dataPointIndex].id], {
                queryParams: {
                  tab: 'overview',
                },
              });
            }
          },
        },
        toolbar: {
          show: false,
        },
      },
      grid: {
        show: false,
        xaxis: {
          lines: {
            show: false,
          },
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '55%',
        },
      },
      dataLabels: {
        enabled: false,
      },
      xaxis: {
        categories: [],
        labels: {
          show: false,
        },
      },
      yaxis: {
        labels: {
          show: false,
        },
      },
      fill: {
        opacity: 1,
        colors: [
          ({ dataPointIndex }: { dataPointIndex: any }) => {
            const status = self.executions[dataPointIndex].status;
            if (status === 'Running' || status === 'Created' || status === 'Pending') {
              return '#008FFB';
            }
            if (status === 'Succeeded') {
              return '#00d68f';
            }
            if (status === 'Error' || status === 'Failed') {
              return '#FF3D71';
            }
            return 0;
          },
        ],
      },
      tooltip: {
        x: {
          formatter: (val: any, opts: any) => {
            return self.executions[opts.dataPointIndex].id;
          },
        },
        y: {
          formatter: (val: number) => {
            return formatDuration(val);
          },
        },
      },
    };
  }

  update(): void {
    this.chartOptions.series[0].data = this.executions.map((e) => e.duration);
    this.chartOptions.xaxis.categories = this.executions.map((e) => e.id);
  }

  ngOnInit(): void {
    this.update();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.executions) {
      this.update();
    }
  }
}
