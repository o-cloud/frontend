import { Component, Input, OnInit } from '@angular/core';
import { ExecutionChartDuration } from 'src/app/@core/models/execution.model';

import { ExecutionsService } from '../../@core/services/executions.service';

@Component({
  selector: 'app-executions-duration-chart-card',
  template: `
    <nb-card size="medium">
      <nb-card-header>
        <div style="display: flex;align-items: center">
          <img
            style="height:1.4rem; width: 1.4rem;"
            src="/assets/images/icons/hourglass_empty_black.svg"
            class="item-icon"
          />Executions duration
        </div>
      </nb-card-header>
      <nb-card-body [nbSpinner]="isLoading">
        <app-executions-duration-chart
          [executions]="executions"
          [isLoading]="isLoading"
        ></app-executions-duration-chart>
      </nb-card-body>
    </nb-card>
  `,
})
export class ExecutionsDurationChartCardComponent implements OnInit {
  executions: ExecutionChartDuration[] = [];
  isLoading = true;

  @Input() workflowId?: string;
  constructor(private executionService: ExecutionsService) {}

  update(): void {
    const params: any = {};
    if (this.workflowId && this.workflowId.length) {
      params.workflowId = this.workflowId;
    }
    this.executionService.getAllExecutions(params).subscribe((c) => {
      this.isLoading = false;
      this.executions = c.map((e) => {
        let duration = 0;
        if (e.startedAt && e.finishedAt) {
          duration = Math.floor(Math.abs(new Date(e.finishedAt).getTime() - new Date(e.startedAt).getTime()) / 1000);
        }
        else if (e.startedAt) {
          duration = Math.floor(Math.abs(new Date().getTime() - new Date(e.startedAt).getTime()) / 1000);
        }
        return { duration , id: e.id, status: e.status }
      });
    });
  }

  ngOnInit(): void {
    this.update();
  }
}
