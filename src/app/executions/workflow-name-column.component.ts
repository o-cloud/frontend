import { Component, Input, OnInit } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { Execution } from '../@core/models/execution.model';

@Component({
  selector: 'app-workflow-name-column',
  template: `
    <div style="display: flex; align-items: center">

      <div [className]="'circle ' + className"></div>
      {{rowData.workflowName}}
    </div>
  `,
  styles: [`
    .running {
      background-color: #0095ff;
    }
    .failed {
      background-color: #ff3d71;
    }
    .succeeded {
      background-color: #00d68f;
    }
    .circle {
      width: 0.6rem;
      height: 0.6rem;
      border-radius: 9999px;
      margin-right: 0.5rem
    }
  `]
})

export class WorkflowNameColumnComponent implements OnInit, ViewCell {
  @Input()
  value!: string | number;
  @Input() rowData!: Execution;
  className = ''

  constructor() {
  }

  ngOnInit(): void {
    if (this.rowData.status === 'Created' || this.rowData.status === 'Running' || this.rowData.status === 'Pending') {
      this.className = 'running'
    } else if (this.rowData.status === 'Succeeded') {
      this.className = 'succeeded'
    } else {
      this.className = 'failed'
    }
  }
}
