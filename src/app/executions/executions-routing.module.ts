import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ExecutionsComponent } from './executions.component';
import { ExecutionDetailsComponent } from './execution-details/execution-details.component';

const routes: Routes = [
  {
    path: 'executions',
    component: ExecutionsComponent
  },
  {
    path: 'executions/:id',
    component: ExecutionDetailsComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutionsRoutingModule{}
