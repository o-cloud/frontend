import { NgModule } from '@angular/core';
import { ExecutionsRoutingModule } from './executions-routing.module';
import { ExecutionsComponent } from './executions.component';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbListModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTagModule,
  NbTooltipModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ExecutionDetailsComponent } from './execution-details/execution-details.component';
import { CommonModule } from '@angular/common';
import { TaskListItemComponent } from './execution-details/task-list-item.component';
import { ExecutionStatusTagComponent } from './execution-details/execution-status-tag.component';
import { ExecutionDetailsRowComponent } from './execution-details/execution-details-row.component';
import { LogsViewComponent } from './execution-details/logs-view.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../@shared/shared.module';
import { ExecutionsStatusChartComponent } from './executions-status-chart/executions-status-chart.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ExecutionsStatusChartCardComponent } from './executions-status-chart/executions-status-chart-card.component';
import { CoreModule } from '../@core/core.module';
import { WorkflowNameColumnComponent } from './workflow-name-column.component';
import { RecentFailuresComponentsComponent } from './recent-failures/recent-failures.components';
import { RecentFailuresTableComponent } from './recent-failures/recent-failures-table.component';
import { ExecutionsDurationChartCardComponent } from './executions-duration-chart/executions-duration-chart-card.component';
import { ExecutionsDurationChartComponent } from './executions-duration-chart/executions-duration-chart.component';

@NgModule({
  imports: [
    ExecutionsRoutingModule,
    NbCardModule,
    Ng2SmartTableModule,
    CommonModule,
    NbListModule,
    NbTabsetModule,
    NbIconModule,
    NbTagModule,
    NbSpinnerModule,
    NbSelectModule,
    FormsModule,
    NbButtonModule,
    NbTooltipModule,
    SharedModule,
    NgApexchartsModule,
    CoreModule
  ],
  exports: [
    ExecutionsComponent,
    ExecutionsStatusChartComponent,
    ExecutionsStatusChartCardComponent,
    ExecutionsDurationChartCardComponent,
    ExecutionsDurationChartComponent,
    RecentFailuresComponentsComponent
  ],
  declarations: [
    ExecutionsComponent,
    ExecutionsStatusChartComponent,
    ExecutionDetailsComponent,
    TaskListItemComponent,
    ExecutionStatusTagComponent,
    ExecutionDetailsRowComponent,
    LogsViewComponent,
    ExecutionsStatusChartCardComponent,
    WorkflowNameColumnComponent,
    RecentFailuresComponentsComponent,
    RecentFailuresTableComponent,
    ExecutionsDurationChartCardComponent,
    ExecutionsDurationChartComponent
  ],
  providers: []
})
export class ExecutionsModule {

}
