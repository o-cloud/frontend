import { Execution } from '../@core/models/execution.model';

const S = 1;
const M = S * 60;
const H = M * 60;
const D = H * 24;
export function duration(from: Date, to: Date): string {
  const durationInSec = Math.floor(Math.abs(to.getTime() - from.getTime()) / 1000);
  return formatDuration(durationInSec);
}

export function formatDuration(durationInSec: number): string {
  const days = Math.floor(durationInSec / D);
  durationInSec = durationInSec - days * D;
  const hours = Math.floor(durationInSec / H);
  durationInSec = durationInSec - hours * H;
  const minutes = Math.floor(durationInSec / M);
  durationInSec = durationInSec - minutes * M;
  if (days > 0) {
    return `${days}d ${hours}h ${minutes}m ${durationInSec}s`;
  } else if (hours > 0) {
    return `${hours}h ${minutes}m ${durationInSec}s`;
  } else if (minutes > 0) {
    return `${minutes}m ${durationInSec}s`;
  } else {
    return `${durationInSec}s`;
  }
}

export function isCompleted(execution: Execution): boolean {
  return execution.status === 'Succeeded' || execution.status === 'Failed' || execution.status === 'Error';
}
