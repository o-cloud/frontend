import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart, ApexLegend
} from 'ng-apexcharts';
import { ExecutionStatusCount } from '../../@core/models/execution.model';

type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-executions-status-chart',
  template: `
    <div id="chart">
      <apx-chart
        *ngIf="!isEmpty"
        [series]="chartOptions.series"
        [chart]="chartOptions.chart"
        [labels]="chartOptions.labels"
        [responsive]="chartOptions.responsive"
        [colors]="['#0095ff', '#00d68f', '#ff3d71']"
        [dataLabels]="{enabled: false}"
        [legend]="legendOption"
      ></apx-chart>
      <div *ngIf="isEmpty && !isLoading">
        No execution found
      </div>
    </div>
  `
})

export class ExecutionsStatusChartComponent implements OnInit, OnChanges {
  @Input()
  executionStatusCount!: ExecutionStatusCount
  @Input()
  isLoading = false;
  chartOptions: ChartOptions;
  legendOption: ApexLegend;
  isEmpty = false
  constructor() {
    this.chartOptions = {
      series: [44, 55, 13],
      chart: {
        type: 'donut'
      },
      responsive: [],
      labels: ['Running', 'Succeeded', 'Failed'],
    };
    this.legendOption = {
      position: 'bottom'
    }
  }

  update(): void {
    this.chartOptions.series = [this.executionStatusCount.running, this.executionStatusCount.succeeded, this.executionStatusCount.failed]
    this.isEmpty = (this.executionStatusCount.failed + this.executionStatusCount.succeeded + this.executionStatusCount.running) === 0
  }

  ngOnInit(): void {
    this.update()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.executionStatusCount) {
      this.update()
    }
  }
}
