import { Component, Input, OnInit } from '@angular/core';
import { ExecutionStatusCount } from 'src/app/@core/models/execution.model';
import { ExecutionsService } from '../../@core/services/executions.service';
import { getDateFromTimeFrame, TimeFrame } from '../../@shared/time-frame.model';

@Component({
  selector: 'app-executions-status-chart-card',
  template: `
    <nb-card size="medium">
      <nb-card-header>
        <div style="position: relative;display: flex;align-items: center">
          <div style="display: flex;align-items: center">
            <nb-icon icon="play-circle-outline" style="font-size: 25px;padding-right: 5px"></nb-icon>Executions
          </div>
          <nb-select [selected]="selectedTimeFrame" (selectedChange)="timeFrameChanged($event)" style="min-width: 120px;position: absolute;right: 0" size="tiny">
            <nb-option *ngFor="let time of timeFrameEnum | stringEnumToArray" [value]="time">{{time}}</nb-option>
          </nb-select>
        </div>
      </nb-card-header>
      <nb-card-body [nbSpinner]="isLoading">
        <app-executions-status-chart [executionStatusCount]="executionStatusCount" [isLoading]="isLoading"></app-executions-status-chart>
      </nb-card-body>
    </nb-card>
  `
})
export class ExecutionsStatusChartCardComponent implements OnInit {
  timeFrameEnum = TimeFrame
  selectedTimeFrame: TimeFrame = TimeFrame.OneDay
  executionStatusCount: ExecutionStatusCount = {failed: 0, running: 0, succeeded: 0};
  isLoading = true

  @Input() workflowId?: string
  constructor(private executionService: ExecutionsService) {}

  update(): void {
    const date = getDateFromTimeFrame(this.selectedTimeFrame)
    const params: any = {
      startTime: date.toISOString(),
    }
    if (this.workflowId && this.workflowId.length) {
      params.workflowId = this.workflowId
    }
    this.executionService.getExecutionStatusCount(params)
      .subscribe(c => {
        this.executionStatusCount = c
        this.isLoading = false
      })
  }

  ngOnInit(): void {
    this.update()
  }

  timeFrameChanged(timeFrame: TimeFrame): void {
    this.selectedTimeFrame = timeFrame
    this.update()
  }
}
