import { Component, Input, OnInit } from '@angular/core';
import { Execution } from '../../@core/models/execution.model';
import { WorkflowNameColumnComponent } from '../workflow-name-column.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recent-failures-table',
  template: `
    <app-table-layout>
      <ng2-smart-table class="clickable"
                       (userRowSelect)="onRowClicked($event)"
                       [settings]="tableSettings"
                       [source]="failures"
      ></ng2-smart-table>
    </app-table-layout>
  `
})

export class RecentFailuresTableComponent implements OnInit {
  @Input()
  failures!: Execution[]
  tableSettings = {
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      workflowName: {
        title: 'Workflow',
        type: 'custom',
        renderComponent: WorkflowNameColumnComponent,
      },
      startedAt: {
        title: 'Started On',
        sortDirection: 'desc',
        valuePrepareFunction: (date: string) => {
          if (date) {
            return new Date(date).toLocaleString();
          } else {
            return '';
          }
        },
      },
    }
  }

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  onRowClicked({ data }: { data: Execution }): void {
    console.log(data);
    this.router.navigate(['/executions/' + data.id], {
      queryParams: {
        tab: 'overview',
      },
    });
  }
}
