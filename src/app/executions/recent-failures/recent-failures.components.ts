import { Component, Input, OnInit } from '@angular/core';
import { getDateFromTimeFrame, TimeFrame } from '../../@shared/time-frame.model';
import { ExecutionsService } from '../../@core/services/executions.service';
import { Execution } from '../../@core/models/execution.model';

@Component({
  selector: 'app-recent-failures',
  template: `
    <nb-card size="medium">
      <nb-card-header>
        <div style="position: relative;display: flex;align-items: center">
          <div style="display: flex;align-items: center">
            <nb-icon icon="backspace-outline" style="font-size: 25px;padding-right: 5px"></nb-icon>Recent Failures
          </div>
          <nb-select [selected]="selectedTimeFrame" (selectedChange)="timeFrameChanged($event)"
                     style="min-width: 120px;position: absolute;right: 0" size="tiny">
            <nb-option *ngFor="let time of timeFrameEnum | stringEnumToArray" [value]="time">{{time}}</nb-option>
          </nb-select>
        </div>
      </nb-card-header>
      <nb-card-body [nbSpinner]="isLoading" style="padding: 0">
        <div *ngIf="failures.length === 0 && !isLoading" style="padding: 1rem 1.5rem">No recent failure</div>
        <app-recent-failures-table *ngIf="failures.length > 0 && !isLoading" [failures]="failures">

        </app-recent-failures-table>
      </nb-card-body>
    </nb-card>
  `
})

export class RecentFailuresComponentsComponent implements OnInit {
  @Input() workflowId?: string
  timeFrameEnum = TimeFrame;
  selectedTimeFrame: TimeFrame = TimeFrame.OneDay;
  failures: Execution[] = [];
  isLoading = true;

  constructor(private executionService: ExecutionsService) {

  }

  update(): void {
    const date = getDateFromTimeFrame(this.selectedTimeFrame);
    const params: any = {
      startTime: date.toISOString()
    };
    if (this.workflowId && this.workflowId.length) {
      params.workflowId = this.workflowId
    }
    this.executionService.getAllExecutions(params)
      .subscribe(r => {
        this.failures = r.filter(exec => {
          return exec.status === 'Failed' || exec.status === 'Error';
        });
        this.isLoading = false
      });
  }

  ngOnInit(): void {
    this.update()
  }

  timeFrameChanged(timeFrame: TimeFrame): void {
    this.selectedTimeFrame = timeFrame;
    this.update()
  }
}
