import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { duration, isCompleted } from './utils';
import { Router } from '@angular/router';
import { Execution } from '../@core/models/execution.model';
import { ExecutionsService } from '../@core/services/executions.service';
import { WorkflowNameColumnComponent } from './workflow-name-column.component';

@Component({
  selector: 'app-executions-component',
  template: `
  <app-table-layout>
    <ng2-smart-table class="clickable"
      (userRowSelect)="onRowClicked($event)"
      [settings]="tableSettings"
      [source]="executions"
    ></ng2-smart-table>
    </app-table-layout>
  `,
})
export class ExecutionsComponent implements OnInit, OnDestroy {
  @Input() workflowId: string | undefined;
  executions: Execution[] = [];
  timeOut: any = undefined;
  tableSettings = {
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      workflowName: {
        title: 'Workflow',
        type: 'custom',
        renderComponent: WorkflowNameColumnComponent,
      },
      status: {
        title: 'Status',
      },
      startedAt: {
        title: 'Started On',
        sortDirection: 'desc',
        valuePrepareFunction: (date: string) => {
          if (date) {
            return new Date(date).toLocaleString();
          } else {
            return '';
          }
        },
      },
      duration: {
        title: 'Duration',
        valuePrepareFunction: (_: string, row: Execution) => {
          if (row.startedAt && row.finishedAt) {
            return duration(new Date(row.startedAt), new Date(row.finishedAt));
          } else if (row.startedAt) {
            return duration(new Date(row.startedAt), new Date());
          } else {
            return '';
          }
        },
      },
    },
  };
  constructor(private executionsService: ExecutionsService, private router: Router) {}

  ngOnInit(): void {
    this.fetchExecutions();
  }

  ngOnDestroy(): void {
    if (this.timeOut) {
      clearTimeout(this.timeOut);
    }
  }

  fetchExecutions(): void {
    this.executionsService
      .getAllExecutions({workflowId: this.workflowId})
      .subscribe((e) => {
        this.executions = e;
        if (this.executions.some((exec) => !isCompleted(exec))) {
          this.timeOut = setTimeout(this.fetchExecutions.bind(this), 5000);
        }
      });
  }

  onRowClicked({ data }: { data: Execution }): void {
    console.log(data);
    this.router.navigate(['/executions/' + data.id], {
      queryParams: {
        tab: 'overview',
      },
    });
  }
}
