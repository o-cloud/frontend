import { Component, Input, OnInit } from '@angular/core';
import { NbComponentSize } from '@nebular/theme';
import { ExecutionStatus } from '../../@core/models/execution.model';

@Component({
  selector: 'app-execution-status-tag',
  template: `
  <nb-tag *ngIf="status === 'Succeeded'" [text]="status" status="success" appearance="filled" [size]="size"></nb-tag>
  <nb-tag *ngIf="status === 'Created'" [text]="status" status="info" appearance="outline" [size]="size"></nb-tag>
  <nb-tag *ngIf="status === 'Running'" [text]="status" status="info" appearance="outline" [size]="size"></nb-tag>
  <nb-tag *ngIf="status === 'Pending'" [text]="status" status="info" appearance="outline" [size]="size"></nb-tag>
  <nb-tag *ngIf="status === 'Error'" [text]="status" status="danger" appearance="filled" [size]="size"></nb-tag>
  <nb-tag *ngIf="status === 'Failed'" [text]="status" status="danger" appearance="filled" [size]="size"></nb-tag>
  `,
})

export class ExecutionStatusTagComponent implements OnInit {

  @Input()
  status?: ExecutionStatus
  @Input()
  size: NbComponentSize = 'medium'

  constructor() {
  }

  ngOnInit(): void {
  }

}
