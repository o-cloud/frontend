import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Task } from 'src/app/@core/models/execution.model';

@Component({
  selector: 'app-logs-view',
  styles: [`
    .log-container {
      font-family: "Menlo", "DejaVu Sans Mono", "Liberation Mono", "Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace;
      font-size: 13px;
      word-break: break-all;
      word-wrap: break-word;
      color: #fff;
      min-height: 42px;
      background-color: #111;
    }
    .line{
      padding: 1px 8px 1px 55px;
      min-height: 1.25rem;
    }
    .line-number{
      color: #666;
      padding: 0 8px;
      min-width: 50px;
      margin-left: -43px;
      padding-right: 1em;
      -webkit-user-select: none;
      user-select: none;
      text-align: right;
    }
    .log-text{
      white-space: pre-wrap;
    }
    .label {
      font-size: 0.85rem;
    }
  `],
  template: `
  <nb-card>
    <nb-card-body *ngIf="!filteredTasks.length">
      <div style="color: #8f9bb3; font-weight: 600">No task runed yet</div>
    </nb-card-body>
    <nb-card-body style="padding: 0" *ngIf="filteredTasks.length">
      <div style="display: flex; padding: 1rem 1.5rem; gap: 30px">
        <div class="form-group">
          <label class="label">Task</label>
          <nb-select [selected]="selectedTaskId" (selectedChange)="selectTask($event)" style="display: block">
            <nb-option *ngFor="let task of filteredTasks" [value]="task.blockId">{{task.name}}</nb-option>
          </nb-select>
        </div>
        <div class="form-group" *ngIf="selectedTask">
          <label  class="label">Container</label>
          <nb-select [(ngModel)]="selectedContainer"  style="display: block">
            <nb-option *ngFor="let item of selectedTask.logs | keyvalue" [value]="item.key">{{item.key}}</nb-option>
          </nb-select>
        </div>
      </div>
      <div class="log-container" *ngIf="selectedTask && selectedContainer">
        <div class="line" *ngFor="let log of this.selectedTask.logs[this.selectedContainer]; let index = index">
          <span class="line-number">{{index + 1}}</span>
          <span class="log-text">{{log}}</span>
        </div>
      </div>
    </nb-card-body>
  </nb-card>
  `,
})

export class LogsViewComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  tasks: Task[] = []
  filteredTasks: Task[] = []
  selectedTask?: Task
  selectedTaskId?: number
  selectedContainer?: string
  subscription: Subscription = new Subscription()



  constructor(private router: Router, private route: ActivatedRoute) {
    console.log(router, route)
    this.subscription.add(route.queryParams.subscribe(params => {
      if (params.task) {
        this.selectedTaskId = parseInt(params.task, 10)
      }
      this.update()
    }))
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tasks) {
      this.update()
    }
  }

  update(): void {
    this.filteredTasks = this.tasks.filter(t => Object.keys(t.logs).length)
    if (this.selectedTask) {
      this.selectedTask = this.filteredTasks.find(t => t.blockId === this.selectedTask?.blockId)
    }
    if (!this.selectedTask && this.filteredTasks.length) {
      if (this.selectedTaskId) {
        this.selectedTask = this.filteredTasks.find(t => t.blockId === this.selectedTaskId)
      }
      if (!this.selectedTask) {
        this.selectTask(this.filteredTasks[0].blockId)
      }
    }
    if (!this.selectedContainer && this.selectedTask) {
      if (this.selectedTask.logs.main) {
        this.selectedContainer = 'main'
      } else if (Object.keys(this.selectedTask.logs).length) {
        this.selectedContainer = Object.keys(this.selectedTask.logs)[0]
      }
    }
  }

  selectTask(taskId: number): void {
    this.selectedTask = this.filteredTasks.find(t => t.blockId === taskId)
    this.router.navigate([], {
      queryParamsHandling: 'merge',
      queryParams: {
        task: taskId
      }
    })
  }
}
