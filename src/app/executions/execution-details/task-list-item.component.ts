import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Task } from 'src/app/@core/models/execution.model';
import { duration } from '../utils';

@Component({
  selector: 'app-task-list-item',
  template: `
    <div style="display: flex; align-items: center; gap: 1rem">
      <nb-icon icon="clock-outline" style="height: 24px; width: 24px" *ngIf="task.status === 'Not Started'"></nb-icon>
      <nb-icon icon="checkmark-circle-2" status="success" style="height: 24px; width: 24px" *ngIf="task.status === 'Succeeded'"></nb-icon>
      <nb-icon icon="alert-circle" status="danger" style="height: 24px; width: 24px" *ngIf="task.status === 'Failed' || task.status === 'Error'"></nb-icon>
      <nb-icon icon="alert-circle" status="warning" style="height: 24px; width: 24px" *ngIf="task.status === 'Omitted' || task.status === 'Skipped'"></nb-icon>
      <div style="height: 24px; width: 24px" [nbSpinner]="true" nbSpinnerStatus="info" *ngIf="task.status === 'Running' || task.status === 'Pending' "></div>
      <div style="flex-grow: 1">
        <div >{{task.name}}</div>
        <div class="caption" style="padding-top: .25rem">Running on
          <span style="font-weight: 600;font-size: 0.80rem;">
            {{task.computeProvider.isLocal ? 'Local (' + task.computeProvider.originClusterName + ')': task.computeProvider.originClusterName}}
          </span>
        </div>
      </div>
      <div>{{status}}</div>
      <div>{{duration}}</div>
      <button nbButton ghost status="info" nbTooltip="See Logs" *ngIf="canSeeLogs" (click)="clickOnLogs()"><nb-icon icon="file-text-outline"></nb-icon></button>
      <div nbTooltip="This task has no logs" *ngIf="!canSeeLogs">
        <button nbButton ghost class="disabled-icon-button" disabled><nb-icon icon="file-text-outline"></nb-icon></button>
      </div>
    </div>
  `,
  styles: [
    `
      .nb-theme-default :host ::ng-deep nb-spinner {
        background-color: transparent;
      }
    `
  ]

})

export class TaskListItemComponent implements OnInit {
  @Input()
  task!: Task
  @Output()
  logsClick: EventEmitter<Task> = new EventEmitter<Task>()
  duration = ''
  status  = 'None running'
  canSeeLogs = false

  constructor() {
  }

  ngOnInit(): void {
    if (this.task.startedAt && this.task.finishedAt) {
      this.duration = duration(new Date(this.task.startedAt), new Date(this.task.finishedAt))
    } else if (this.task.startedAt) {
      this.duration = duration(new Date(this.task.startedAt), new Date())
    }
    let statusMap = new Map<string, number>()
    for (var el in this.task.nodeStatus) {
      let s = this.task.nodeStatus[el]
      if (!statusMap.has(s)) {
        statusMap.set(s,1)
      } else {
        statusMap.set(s,statusMap.get(s)!+1)
      }
    }
    if (statusMap.size === 0) {
      this.status = 'None running'
    } else {
      this.status = ''
      statusMap.forEach((c,el) => {
        this.status += el + ': ' + c + ' '
      })
    }
    this.canSeeLogs = Object.keys(this.task.logs).length > 0
  }

  clickOnLogs(): void {
    this.logsClick.emit(this.task)
  }
}
