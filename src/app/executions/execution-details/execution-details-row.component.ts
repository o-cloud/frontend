import { Component, Input, OnInit } from '@angular/core';
import { NbComponentStatus } from '@nebular/theme';
import { Execution } from 'src/app/@core/models/execution.model';
import { duration } from '../utils';

@Component({
  selector: 'app-execution-details-row-component',
  template: `
    <nb-card class="detailsRow" [accent]="detailsAccent" *ngIf="_execution">
      <nb-card-body style="padding: 0; display: flex; align-items: center">
          <div class="row-item">
            <div style="display: flex; align-items: center">
              <nb-icon icon="share-outline" class="item-icon"></nb-icon>
              <div>
                <div style="padding-bottom: .75em">{{_execution.workflowName}}</div>
                <div class="caption">Workflow</div>
              </div>
            </div>
          </div>
          <div class="row-item">
            <div style="display: flex; align-items: center">
              <nb-icon icon="checkmark-circle-2-outline" class="item-icon"></nb-icon>
              <div>
                <div style="padding-bottom: .75em">{{_execution.status}}</div>
                <div class="caption">Status</div>
              </div>
            </div>
          </div>
          <div class="row-item">
            <div style="display: flex; align-items: center">
              <nb-icon icon="clock-outline" class="item-icon"></nb-icon>
              <div>
                <div style="padding-bottom: .75em">{{startTime}}</div>
                <div class="caption">Started on</div>
              </div>
            </div>
          </div>
          <div class="row-item">
            <div style="display: flex; align-items: center">
              <img src="/assets/images/icons/hourglass_empty_black.svg" class="item-icon">
              <div>
                <div style="padding-bottom: .75em">{{duration}}</div>
                <div class="caption">Duration</div>
              </div>
            </div>
          </div>
          <div class="row-item" *ngIf="endTime && endTime.length">
            <div style="display: flex; align-items: center">
              <nb-icon icon="clock-outline" class="item-icon"></nb-icon>
              <div>
                <div style="padding-bottom: .75em">{{endTime}}</div>
                <div class="caption">Finished on</div>
              </div>
            </div>
          </div>
          <div class="row-item">
            <div style="display: flex; align-items: center">
              <img src="/assets/images/icons/euro_black_48dp.svg" class="item-icon">
              <div>
                <div style="padding-bottom: .75em">NA <!--<span class="caption" style="font-size: 0.95em">€</span>--></div>
                <div class="caption" >Cost</div>
              </div>
            </div>
          </div>
      </nb-card-body>
    </nb-card>
  `,
  styles: [`
    .row-item {
      padding: 1em;
      font-size: 1.25em;
      min-width: 180px;
      border-right: 1px solid #e9e9e9;
    }
    .row-item:last-child {
      border-right: none;
    }
    .item-icon {
      height: 35px;
      width: auto;
      padding-right: 0.75em;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent.detailsRow {
      border-left-style: solid;
      border-left-width: .25rem;
      border-top: none;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-success.detailsRow {
      border-left-color: #00d68f;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-danger.detailsRow {
      border-left-color: #ff3d71;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-primary.detailsRow {
      border-left-color: #36f;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-basic.detailsRow {
      border-left-color: #f7f9fc;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-control.detailsRow {
      border-left-color: #fff;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-info.detailsRow {
      border-left-color: #0095ff;
    }
    .nb-theme-default :host ::ng-deep nb-card.accent-warning.detailsRow {
      border-left-color: #fa0;
    }
    .caption {
      font-size: 0.75em
    }
  `]
})

export class ExecutionDetailsRowComponent implements OnInit {
  endTime = ''
  duration = ''
  startTime = ''
  // tslint:disable-next-line:variable-name
  _execution?: Execution
  detailsAccent: NbComponentStatus = 'control'

  @Input()
  set execution(e: Execution) {
    this._execution = e
    if (e.startedAt) {
      this.startTime = new Date(e.startedAt).toLocaleString()
    }
    if (e.finishedAt) {
      this.endTime = new Date(e.finishedAt).toLocaleString()
    }
    if (e.startedAt && e.finishedAt) {
      this.duration =  duration(new Date(e.startedAt), new Date(e.finishedAt))
    } else if (e.startedAt) {
      this.duration = duration(new Date(e.startedAt), new Date())
    }
    if (e.status === 'Created' || e.status === 'Pending' || e.status === 'Running') {
      this.detailsAccent = 'info'
    } else if (e.status === 'Failed' || e.status === 'Error') {
      this.detailsAccent = 'danger'
    } else if (e.status === 'Succeeded') {
      this.detailsAccent = 'success'
    }
  }

  constructor() {
  }

  ngOnInit(): void {
  }
}
