import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { isCompleted } from '../utils';
import { Subscription } from 'rxjs';
import { Execution, Task } from '../../@core/models/execution.model';
import { ExecutionsService } from '../../@core/services/executions.service';
import { TabsController } from '../../@shared/tabs/tabs-controller';

type TabName = 'overview' | 'logs';
const allTabs: TabName[] = ['overview', 'logs'];
function isValidTab(name: string | null): name is TabName {
  if (!name) {
    return false;
  }
  return allTabs.includes(name as TabName);
}

@Component({
  selector: 'app-execution-details',
  template: `
    <ng-template [ngIf]="execution">
      <app-page-layout>
        <app-page-layout-header>
          <app-breadcrumbs>
            <app-breadcrumbs-item [routerLink]="['/dashboard']" [queryParams]="{tab: 'dashboard'}">
              <nb-icon icon="home-outline"></nb-icon>
            </app-breadcrumbs-item>
            <app-breadcrumbs-item [routerLink]="['/dashboard']" [queryParams]="{tab: 'workflows'}">
              Workflows
            </app-breadcrumbs-item>
            <app-breadcrumbs-item [routerLink]="['/workflow/' + execution.workflowId]" [queryParams]="{tab: 'overview'}">
              {{execution.workflowName}}
            </app-breadcrumbs-item>
            <app-breadcrumbs-item [routerLink]="['/workflow/' + execution.workflowId]" [queryParams]="{tab: 'executions'}">
              Executions
            </app-breadcrumbs-item>
            <app-breadcrumbs-item>
              {{execution.id}}
            </app-breadcrumbs-item>

          </app-breadcrumbs>
          <app-page-layout-header-title>
            <span style="margin-right: 0.5em">{{ execution.workflowName }}</span>
            <app-execution-status-tag [status]="execution.status" size="large"></app-execution-status-tag>
          </app-page-layout-header-title>
          <app-tabs-group>
            <app-tab *ngFor="let tab of allTabs" [isActive]="selectedTab === tab" (click)="tabController.openTab(tab)">
              {{ tab }}
            </app-tab>
          </app-tabs-group>
        </app-page-layout-header>
        <app-page-layout-body style="margin-top: 36px">
          <div *ngIf="selectedTab === 'overview'" class="grid">
            <app-execution-details-row-component
              style="grid-area: detailsInRow;"
              [execution]="execution"
            ></app-execution-details-row-component>
            <nb-card style="grid-area: tasks">
              <nb-card-header>Tasks</nb-card-header>
              <nb-card-body style="padding: 0">
                <nb-list>
                  <nb-list-item *ngFor="let task of execution.tasks" style="display: block">
                    <app-task-list-item [task]="task" (logsClick)="openTaskLogs($event)"></app-task-list-item>
                  </nb-list-item>
                </nb-list>
              </nb-card-body>
            </nb-card>
          </div>
          <div *ngIf="selectedTab === 'logs'">
            <app-logs-view [tasks]="execution.tasks"></app-logs-view>
          </div>
        </app-page-layout-body>
      </app-page-layout>
    </ng-template>
  `,
  styles: [
    `
      .grid {
        display: grid;
        grid-template-areas:
          'detailsInRow'
          'tasks';
        gap: 30px;
      }
      .keyValuePair {
        display: flex;
        padding-bottom: 1rem;
        line-height: inherit;
      }
      .keyLabel.label {
        width: 45%;
        font-size: 0.8rem;
      }
    `,
  ],
})
export class ExecutionDetailsComponent implements OnInit, OnDestroy {
  id = '';
  execution?: Execution;
  timeout: any;
  selectedTab?: TabName;
  subscription: Subscription = new Subscription();
  allTabs = allTabs;
  tabController: TabsController<TabName>;

  constructor(private route: ActivatedRoute, private router: Router, private executionService: ExecutionsService) {
    this.tabController = new TabsController<TabName>(route, router, 'overview', isValidTab);
    this.subscription.add(this.tabController.currentTab().subscribe((t) => (this.selectedTab = t)));
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(take(1)).subscribe((params) => {
      this.id = params.get('id') ?? '';
      this.fetch();
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.tabController.teardown();
  }

  fetch(): void {
    if (this.id && this.id.length) {
      this.executionService.getExecutionById(this.id).subscribe((e) => {
        this.execution = e;
        if (!isCompleted(e)) {
          this.timeout = setTimeout(this.fetch.bind(this), 5000);
        }
      });
    }
  }

  openTaskLogs(task: Task): void {
    this.tabController.openTab('logs', { task: '' + task.blockId });
  }
}
