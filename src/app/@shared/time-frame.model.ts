export enum TimeFrame {
   OneDay = 'Last 24H',
  OneWeek = 'Last Week',
  OneMonth = 'Last Month'
}

export function getDateFromTimeFrame(t: TimeFrame): Date{
  const d = new Date()
  switch (t) {
    case TimeFrame.OneDay:
      d.setDate(d.getDate() - 1)
      break
    case TimeFrame.OneMonth:
      d.setDate(d.getDate() - 30)
      break
    case TimeFrame.OneWeek:
      d.setDate(d.getDate() - 7)
      break
  }
  return d;
}
