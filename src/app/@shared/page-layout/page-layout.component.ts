import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-page-layout',
  template: `
    <ng-content select="app-page-layout-header"></ng-content>
    <ng-content select="app-page-layout-body"></ng-content>
  `,
  styleUrls: ['page-layout.scss']
})

export class PageLayoutComponent {
  @HostBinding('class.page-layout') pageLayoutClass = true
  constructor() {
  }
}

@Component({
  selector: 'app-page-layout-body',
  template: `<ng-content></ng-content>`,
  styleUrls: ['page-layout.scss']
})

export class PageLayoutBodyComponent {
  @HostBinding('class.page-body') pageBodyClass = true;
}

@Component({
  selector: 'app-page-layout-header',
  template: `
    <ng-content></ng-content>
  `,
  styleUrls: ['page-layout.scss']
})

export class PageLayoutHeaderComponent {
  @HostBinding('class.header') headerClass = true
}

@Component({
  selector: 'app-page-layout-header-title',
  template: `
    <h5 style="padding: 0 0 1.5rem;margin: 0">
      <ng-content></ng-content>
    </h5>
  `,
  styleUrls: ['page-layout.scss']
})

export class PageLayoutHeaderTitleComponent {
}
