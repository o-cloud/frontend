import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    template: `
    <div style="text-align: center">
        <button nbButton nbTooltip="Edit" ghost (click)="onEdit()"><nb-icon icon="edit-outline" style="color: black"></nb-icon></button>
        <button nbButton nbTooltip="Delete" ghost (click)="onDelete()"><nb-icon icon="trash-2-outline" status="danger"></nb-icon></button>
    </div>
    `,
    styleUrls: ['./actions.scss'],
})
export class ActionsComponent implements ViewCell {
    constructor() {}

    @Input()
    value!: string | number;
    @Input() rowData: any;

    @Output() delete: EventEmitter<any> = new EventEmitter();
    @Output() edit: EventEmitter<any> = new EventEmitter();

    onEdit(): void {
        this.edit.emit(this.rowData);
    }

    onDelete(): void {
        this.delete.emit(this.rowData);
    }
}
