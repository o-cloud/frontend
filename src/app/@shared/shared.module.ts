import { NgModule } from '@angular/core';
import { TabComponent, TabsGroupComponent } from './tabs/tabs.component';
import { NbButtonModule, NbCardModule, NbDialogModule, NbIconModule, NbTooltipModule } from '@nebular/theme';
import {
  PageLayoutBodyComponent,
  PageLayoutComponent,
  PageLayoutHeaderComponent,
  PageLayoutHeaderTitleComponent,
} from './page-layout/page-layout.component';
import { ButtonCancelComponent, ButtonComponent } from './buttons/buttons.component';
import { CwlEditorButtonViewComponent, CwlEditorComponent } from './cwl-editor/cwl-editor.component';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { FormsModule } from '@angular/forms';
import { ValidationModalComponent } from './validation-modal/validation-modal.component';
import { ActionsComponent } from './table-layout/actions.component';
import { TableLayoutComponent } from './table-layout/table-layout.component';
import { BreadcrumbsComponent, BreadcrumbsItemComponent } from './breadcrumbs/breadcrumbs.component';
import { RouterModule } from '@angular/router';
import { ImgPreviewComponent, ImgPreviewViewComponent } from './img-preview/img-preview.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbTooltipModule,
    NbDialogModule.forChild(),
    MonacoEditorModule,
    RouterModule,
  ],
  providers: [],
  declarations: [
    TabComponent,
    TabsGroupComponent,
    PageLayoutComponent,
    PageLayoutBodyComponent,
    PageLayoutHeaderComponent,
    PageLayoutHeaderTitleComponent,
    ButtonComponent,
    ButtonCancelComponent,
    ValidationModalComponent,
    CwlEditorComponent,
    CwlEditorButtonViewComponent,
    ActionsComponent,
    TableLayoutComponent,
    BreadcrumbsComponent,
    BreadcrumbsItemComponent,
    ImgPreviewComponent,
    ImgPreviewViewComponent,
  ],
  exports: [
    TabComponent,
    TabsGroupComponent,
    PageLayoutComponent,
    PageLayoutBodyComponent,
    PageLayoutHeaderComponent,
    PageLayoutHeaderTitleComponent,
    ButtonComponent,
    ButtonCancelComponent,
    ValidationModalComponent,
    CwlEditorComponent,
    CwlEditorButtonViewComponent,
    ActionsComponent,
    TableLayoutComponent,
    BreadcrumbsComponent,
    BreadcrumbsItemComponent
  ],
})
export class SharedModule {}
