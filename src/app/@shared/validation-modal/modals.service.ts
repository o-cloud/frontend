import { Injectable } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ValidationModalComponent } from './validation-modal.component';
import { NbDialogRef } from '@nebular/theme/components/dialog/dialog-ref';

@Injectable({
  providedIn: 'root',
})
export class ModalsService {
  constructor(private modalService: NbDialogService) {}

  openModalValidation(
    text: string,
    acceptBtn: string,
    cancelBtn: string
  ): NbDialogRef<ValidationModalComponent> {
    return this.modalService.open(ValidationModalComponent, {
      hasBackdrop: true,
      autoFocus: true,
      closeOnEsc: true,
      context: {
        text,
        acceptBtn,
        cancelBtn,
      },
    });
  }
}
