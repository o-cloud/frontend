import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-validation-modal',
  templateUrl: './validation-modal.component.html',
  styleUrls: ['./validation-modal.component.scss'],
})
export class ValidationModalComponent implements OnInit {
  text = '';
  cancelBtn = '';
  acceptBtn = '';

  constructor(protected dialogRef: NbDialogRef<ValidationModalComponent>) {}

  ngOnInit(): void {}

  cancel(): void {
    this.dialogRef.close();
  }

  validate(): void {
    this.dialogRef.close(true);
  }
}
