import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  NbAccordionModule, NbButtonGroupModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule, NbDatepickerModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbSpinnerModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ResourcesRoutingModule } from './resources-routing.module';
import { ResourcesComponent } from './resources.component';
import { CoreModule } from '../@core/core.module';
import { SharedModule } from '../@shared/shared.module';
import { CatalogueTabComponent } from './catalogue-tab.component';
import { FavoritesTabComponent } from './favorites-tab.component';
import { CatalogItemComponent } from './components/catalog-item.component';
import { NgxAdminModule } from '../@ngx-admin/ngx-admin.module';

@NgModule({
  imports: [
    ResourcesRoutingModule,
    NbCardModule,
    CommonModule,
    Ng2SmartTableModule,
    FormsModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    CoreModule,
    NbIconModule,
    NbCheckboxModule,
    NbAccordionModule,
    SharedModule,
    NbDatepickerModule,
    NbSpinnerModule,
    NbButtonGroupModule,
    NbFormFieldModule,
    NbDialogModule,
    NgxAdminModule
  ],
  declarations: [
    ResourcesComponent,
    CatalogueTabComponent,
    FavoritesTabComponent,
    CatalogItemComponent,
  ],
  exports: [
    CatalogItemComponent,
  ],
  providers: [],
})
export class ResourcesModule {}
