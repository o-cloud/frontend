import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CatalogueSearchResult, ProviderType } from '../@core/models/catalog.models';
import { CatalogueService } from '../@core/services/catalogue.service';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';
import { FavoriteService } from '../@core/services/favorite.service';

const listAnimation = trigger('listAnimation', [
  transition('* <=> *', [
    query(':enter',
      [style({ opacity: 0 }), stagger('60ms', animate('600ms ease-out', style({ opacity: 1 })))],
      { optional: true }
    ),
    query(':leave',
      animate('200ms', style({ opacity: 0 })),
      { optional: true}
    )
  ])
]);

@Component({
  selector: 'app-catalogue-tab-component',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./resources.component.scss'],
  animations: [listAnimation],
})
export class CatalogueTabComponent implements OnInit, OnDestroy  {
  searchResults: CatalogueSearchResult[] = [];
  subscriptions = new Subscription();
  query = '';
  providerType = ProviderType.Data;
  providerTypeEnum = ProviderType;

  constructor(
    private catalogueService: CatalogueService,
    private favoriteService: FavoriteService
  ) {}

  ngOnInit(): void {
      this.search();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  search(): void {
      this.searchResults = [];
      this.subscriptions.add(
          this.catalogueService.search(this.query, this.providerType).subscribe((res) => {
              this.searchResults = [...this.searchResults, ...res];
          })
      );
  }

  addToFavorite(item: CatalogueSearchResult): void {
    this.favoriteService.addFavorite(item).subscribe(() => (item.isFavorite = true));
  }

  removeFavorite(item: CatalogueSearchResult): void {
    this.favoriteService.deleteFavorite(item.providerId).subscribe(() => (item.isFavorite = false));
  }

}
