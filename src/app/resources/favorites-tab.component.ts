import { Component, OnDestroy, OnInit } from '@angular/core';
import { FavoriteService } from '../@core/services/favorite.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';
import { map } from 'rxjs/internal/operators/map';
import { CatalogueSearchResult } from '../@core/models/catalog.models';

const listAnimation = trigger('listAnimation', [
  transition('* <=> *', [
    query(':enter',
      [style({ opacity: 0 }), stagger('60ms', animate('600ms ease-out', style({ opacity: 1 })))],
      { optional: true }
    ),
    query(':leave',
      animate('200ms', style({ opacity: 0 })),
      { optional: true}
    )
  ])
]);

@Component({
  selector: 'app-favorites-tab',
  templateUrl: './favorites.component.html',
  styleUrls: ['./resources.component.scss'],
  animations: [listAnimation],
})
export class FavoritesTabComponent implements OnInit, OnDestroy {
  loading = false;
  searchResults: CatalogueSearchResult[] = [];
  filteredSearchResults: CatalogueSearchResult[] = [];
  subscriptions: Subscription = new Subscription();
  constructor(
    private favoriteService: FavoriteService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.loading = true
    this.favoriteService.getFavorites().pipe(map(data => {
      return data.map(
        f => ({...f, isFavorite: true, workflowStructure: JSON.stringify(f.workflowStructure, null, 2)} as CatalogueSearchResult)
      )
    })).subscribe((data) => {
      this.searchResults = data;
      this.filteredSearchResults = data;
      this.loading = false;
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  goToPage(pageName: string): void {
    this.router.navigate([`${pageName}`]);
  }

  search(event: Event): any {
    const searchQuery = (event.target as HTMLInputElement).value.toLowerCase();
    this.filteredSearchResults = this.searchResults.filter((fav) => {
      return fav.name.toLowerCase().includes(searchQuery);
    });
  }

  removeFavorite(item: CatalogueSearchResult): void {
    this.favoriteService.deleteFavorite(item.providerId).subscribe(() => {
      this.searchResults = this.searchResults.filter(s => s !== item)
      this.filteredSearchResults = this.filteredSearchResults.filter(s => s !== item)
    });
  }
}
