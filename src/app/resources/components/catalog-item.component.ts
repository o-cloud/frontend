import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { CatalogueSearchResult } from 'src/app/@core/models/catalog.models';
import { CwlEditorComponent } from 'src/app/@shared/cwl-editor/cwl-editor.component';

@Component({
  selector: 'app-catalog-item',
  templateUrl: './catalog-item.component.html',
  styleUrls: ['./catalog-item.component.scss'],
})
export class CatalogItemComponent {
  @Input() item!: CatalogueSearchResult;
  @Input() cwlType = 'yaml';
  @Output() handleAdd = new EventEmitter<void>();
  @Output() handleRemove = new EventEmitter<void>();

  constructor(protected dialogService: NbDialogService) {}

  calculateRam(ram: number): number {
    return Math.round(ram / 100000) / 10;
  }

  seeCwl(item: CatalogueSearchResult): void {
    let cwl = '';
    try {
      cwl = atob(item.workflowStructure);
    } catch (e) {
      cwl = item.workflowStructure;
    }
    this.dialogService.open(CwlEditorComponent, {
      closeOnBackdropClick: true,
      context: { cwl, type: this.cwlType },
    });
  }

  addToFavorites(): void {
    this.handleAdd.emit();
  }

  removeFromFavorites(): void {
    this.handleRemove.emit();
  }
}
