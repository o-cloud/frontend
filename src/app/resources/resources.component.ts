import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CatalogueService } from '../@core/services/catalogue.service';
import { Subscription } from 'rxjs';
import { CatalogueSearchResult, ProviderType } from '../@core/models/catalog.models';
import { ActivatedRoute, Router } from '@angular/router';
import { FavoriteService } from '../@core/services/favorite.service';
import { TabsController } from '../@shared/tabs/tabs-controller';

type TabName = 'catalog' | 'favorites';
const allTabs: TabName[] = ['catalog', 'favorites'];
function isValidTab(name: string | null): name is TabName {
  if (!name) {
    return false;
  }
  return allTabs.includes(name as TabName);
}

@Component({
  selector: 'app-resources-page',
  template: `
    <app-page-layout>
      <app-page-layout-header>
        <app-page-layout-header-title><nb-icon icon="book-open-outline"></nb-icon> Resources</app-page-layout-header-title>
        <app-tabs-group>
          <app-tab *ngFor="let tab of allTabs" [isActive]="selectedTab === tab" (click)="tabController.openTab(tab)">
            {{ tab }}
          </app-tab>
        </app-tabs-group>
      </app-page-layout-header>
      <app-page-layout-body>
        <app-catalogue-tab-component *ngIf="selectedTab === 'catalog'"></app-catalogue-tab-component>
        <app-favorites-tab *ngIf="selectedTab === 'favorites'"></app-favorites-tab>
      </app-page-layout-body>
    </app-page-layout>
  `,
})
export class ResourcesComponent implements OnInit, OnDestroy {
  allTabs = allTabs;
  selectedTab?: TabName;
  subscription = new Subscription();
  tabController: TabsController<TabName>;

  @Input()
  searchResults: CatalogueSearchResult[] = [];
  subscriptions = new Subscription();
  query = '';
  providerType = ProviderType.Data;
  providerTypeEnum = ProviderType;

  constructor(
    private route: ActivatedRoute,
    private catalogueService: CatalogueService,
    private router: Router,
    private favoriteService: FavoriteService
  ) {
    this.tabController = new TabsController<TabName>(route, router, 'catalog', isValidTab);
    this.subscription.add(this.tabController.currentTab().subscribe((t) => (this.selectedTab = t)));
  }

  ngOnInit(): void {
    this.search();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this.tabController.teardown()
  }

  search(): void {
    this.searchResults = [];
    this.subscriptions.add(
      this.catalogueService.search(this.query, this.providerType).subscribe((res) => {
        this.searchResults = [...this.searchResults, ...res];
      })
    );
  }

  goToPage(pageName: string): void {
    this.router.navigate([`${pageName}`]);
  }

  addToFavorites(result: CatalogueSearchResult): void {
    this.favoriteService.addFavorite(result).subscribe(() => (result.isFavorite = true));
  }

  removeFromFavorites(result: CatalogueSearchResult): void {
    this.favoriteService.deleteFavorite(result.providerId).subscribe(() => (result.isFavorite = false));
  }
}
