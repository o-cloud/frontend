import { RouterModule, Routes } from '@angular/router';
import { ResourcesComponent } from './resources.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'resources',
    component: ResourcesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourcesRoutingModule {}
