import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

import { filter, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserService } from '../../../@core/services/user.service';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { environment } from 'src/environments/environment';
import { DynamicConfigurationService } from 'src/app/@core/services/dynamic-configuration.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  public isLoggedIn = false;
  public userProfile: KeycloakProfile | null = null;
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserService,
    private breakpointService: NbMediaBreakpointsService,
    private confService: DynamicConfigurationService,
    private readonly keycloak: KeycloakService
  ) {}

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
  ];
  currentTheme = 'default';
  userMenu = [{ title: 'Profile' }, { title: 'Logout' }];


  async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    this.currentTheme = this.themeService.currentTheme;

    this.user = this.userService.getCurrentUser();

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService
      .onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (isLessThanXl: boolean) => (this.userPictureOnly = isLessThanXl)
      );

    this.themeService
      .onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$)
      )
      .subscribe((themeName) => (this.currentTheme = themeName));
      this.menuService.onItemClick().pipe(filter(({tag})=> tag==='context-menu'), filter(({item: {title}}) => title === 'Logout')).subscribe(()=>  this.login())
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  public async login() {
    if(this.confService.authConfiguration.enableKeycloakAuth){
      if(this.isLoggedIn){
        this.keycloak.logout()
        this.isLoggedIn=false
      } else {
        this.keycloak.login()
        this.isLoggedIn=true
      }
    }
  }
}
