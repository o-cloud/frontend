import { Component, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { AllWorkflowResponseItem } from '../../workflow/workflow.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-workflow-executions-line',
  template: `
    <div>
      <span [class]="'dot ' + execution.status.toLowerCase()"
            (click)="executionClicked(execution)"
            *ngFor="let execution of rowData.executions.slice(Math.max(rowData.executions.length - 10, 0))"
            [nbTooltip]="execution.status"
      ></span>
    </div>
  `,
  styleUrls: ['./workflow-executions-line.component.scss']
})

export class WorkflowExecutionsLineComponent implements ViewCell {
  @Input()
  value!: string | number;
  @Input() rowData!: AllWorkflowResponseItem;
  Math = Math

  constructor(private router: Router) {
  }

  executionClicked(exec: {id: string}): void {
    this.router.navigate(['/executions/' + exec.id], {queryParams: {tab: 'overview'}})
  }

}

