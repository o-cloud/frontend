import { Component, OnDestroy, OnInit } from '@angular/core';
import { AllWorkflowResponseItem, Workflow } from '../workflow/workflow.model';
import { Subscription } from 'rxjs';
import { WorkflowService } from '../workflow/workflow.service';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { CreateWorkflowDialogComponent } from '../workflow/create-workflow-dialog.component';
import { ActionsComponent } from '../@shared/table-layout/actions.component';
import { WorkflowExecutionsLineComponent } from './workflow-executions-line/workflow-executions-line.component';

@Component({
  selector: 'app-workflow-tab-component',
  template: `

    <nb-card id="global-card">
      <nb-card-header class="header-wf">Workflow <app-button class="btn" (click)="createWorkflow()"><nb-icon icon="plus-circle-outline"></nb-icon>New Workflow</app-button></nb-card-header>
      <nb-card-body>
        <app-table-layout>
        <ng2-smart-table [settings]="settings" [source]="workflows"></ng2-smart-table>
        </app-table-layout>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./dashboard.component.scss'],
})

export class WorkflowTabComponent implements OnInit, OnDestroy {
  workflows: AllWorkflowResponseItem[] = [];
  subscriptions = new Subscription();
  settings = {
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Name'
      },
      executions: {
        title: 'Last Executions',
        type: 'custom',
        renderComponent: WorkflowExecutionsLineComponent,
      },
      actions: {
        type: 'custom',
        renderComponent: ActionsComponent,
        onComponentInitFunction: (c: ActionsComponent) => {
          this.subscriptions.add(c.delete.asObservable().subscribe(this.deleteWorkflow));
          this.subscriptions.add(c.edit.asObservable().subscribe(this.editWorkflow));
        }
      }
    }
  };

  constructor(
    private workflowService: WorkflowService,
    private router: Router,
    private dialogService: NbDialogService
  ) {
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.workflowService.getAllWorkflows().subscribe((d) => (this.workflows = d.workflows));
  }

  createWorkflow(): void {
    this.subscriptions.add(
      this.dialogService.open(CreateWorkflowDialogComponent).onClose.subscribe((r: Omit<Workflow, 'id'>) => {
        this.workflowService.createWorkflow(r).subscribe(w => {
          const newWorkflow: AllWorkflowResponseItem = {
            ...w,
            executions: []
          }
          this.workflows = [...this.workflows, newWorkflow]
        });
      })
    );
  }

  deleteWorkflow = (w: AllWorkflowResponseItem): void => {
    this.workflowService.deleteWorkflow(w.id).subscribe(() => {
      this.workflows = this.workflows.filter((i) => w.id !== i.id);
    });
  };

  editWorkflow = (w: AllWorkflowResponseItem): void => {
    this.router.navigate(['/workflow/' + w.id], {
      queryParams: {
        tab: 'overview'
      }
    });
  };

}
