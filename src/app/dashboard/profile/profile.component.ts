import { Component, Input, OnInit } from '@angular/core';
import { Identity } from 'src/app/@core/models/identity.model';
import { DiscoveryService } from 'src/app/@core/services/discovery.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./../dashboard.component.scss'],
})
export class ProfileComponent implements OnInit {
  @Input()
  identity: Identity = {
    name: '',
    url: '',
    description: '',
    categories: [''],
    network: '',
    swarm: {
      network: '',
      name: '',
    },
  };

  constructor(private discoveryService: DiscoveryService) {}

  ngOnInit(): void {
    this.discoveryService.getIdentity().subscribe((data) => {
      this.identity = data;
      console.log(this.identity)
    });
  }
}
