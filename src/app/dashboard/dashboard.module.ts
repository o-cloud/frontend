import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbButtonModule, NbCardModule, NbIconModule, NbSelectModule, NbTooltipModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NewsComponent } from './news/news.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home.component';
import { SharedModule } from '../@shared/shared.module';
import { DashboardTabComponent } from './dashboard-tab.component';
import { WorkflowTabComponent } from './workflow-tab.component';
import { ExecutionsTabComponent } from './executions-tab.component';
import { ExecutionsModule } from '../executions/executions.module';
import { CoreModule } from '../@core/core.module';
import { ResourcesModule } from '../resources/resources.module';
import { WorkflowExecutionsLineComponent } from './workflow-executions-line/workflow-executions-line.component';

@NgModule({
  imports: [
    DashboardRoutingModule,
    NbCardModule,
    CommonModule,
    Ng2SmartTableModule,
    NbIconModule,
    SharedModule,
    NbButtonModule,
    ExecutionsModule,
    NbSelectModule,
    CoreModule,
    ResourcesModule,
    NbTooltipModule
  ],
  declarations: [
    NewsComponent,
    ProfileComponent,
    HomeComponent,
    DashboardTabComponent,
    WorkflowTabComponent,
    ExecutionsTabComponent,
    WorkflowExecutionsLineComponent
  ],
  exports: [],
})
export class DashboardModule {}
