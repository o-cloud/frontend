import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TabsController } from '../@shared/tabs/tabs-controller';

type TabName = 'dashboard' | 'workflows' | 'executions';
const allTabs: TabName[] = ['dashboard', 'workflows', 'executions'];
function isValidTab(name: string | null): name is TabName {
  if (!name) {
    return false;
  }
  return allTabs.includes(name as TabName);
}

@Component({
  selector: 'app-home',
  template: `
    <app-page-layout>
      <app-page-layout-header>
        <app-page-layout-header-title>Home</app-page-layout-header-title>
        <app-tabs-group>
          <app-tab *ngFor="let tab of allTabs" [isActive]="selectedTab === tab" (click)="tabController.openTab(tab)">
            {{ tab }}
          </app-tab>
        </app-tabs-group>
      </app-page-layout-header>
      <app-page-layout-body>
        <app-dashboard-tab-component *ngIf="selectedTab === 'dashboard'"></app-dashboard-tab-component>
        <app-workflow-tab-component *ngIf="selectedTab === 'workflows'"></app-workflow-tab-component>
        <app-executions-tab-component *ngIf="selectedTab === 'executions'"></app-executions-tab-component>
      </app-page-layout-body>
    </app-page-layout>
  `,
})
export class HomeComponent implements OnInit, OnDestroy {
  allTabs = allTabs;
  selectedTab?: TabName;
  subscription = new Subscription();
  tabController: TabsController<TabName>;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.tabController = new TabsController<TabName>(route, router, 'dashboard', isValidTab);
    this.subscription.add(
      this.tabController.currentTab().subscribe((t) => {
        this.selectedTab = t;
      })
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.tabController.teardown();
  }
}
