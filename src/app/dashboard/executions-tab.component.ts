import { Component } from '@angular/core';

@Component({
  selector: 'app-executions-tab-component',
  template: `
    <nb-card id="global-card">
      <nb-card-header>Executions</nb-card-header>
      <nb-card-body>
        <app-executions-component></app-executions-component>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./dashboard.component.scss'],
})
export class ExecutionsTabComponent {}
