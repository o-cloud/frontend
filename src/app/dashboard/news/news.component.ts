import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/@core/models/news.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./../dashboard.component.scss'],
})
export class NewsComponent implements OnInit {
  constructor() {}

  news: News[] = [
    {
      id: 1,
      name: 'Your application update is available Version 2.2.0 More sentinel images available with this new version. ',
    },
    {
      id: 2,
      name:
        'Sentinel2 :       Seems like you are using an older version.       Upgrade now to use our latest satelite images. ',
    },
    {
      id: 3,
      name: 'IAlgoApp : A new version with 95% precision available now.  Click here to upgrade. ',
    },
    {
      id: 4,
      name:
        'Adementis DataCenterV2 Available You can now use the Adementis Data Center inside your SB workflow In order to save and secure your data. ',
    },
    {
      id: 5,
      name:
        'Ovea offers new services Upgrade your SB plugin in order to use our latest services. More secure, more space with a human centric relation.      ',
    },
  ];

  ngOnInit(): void {}
}
