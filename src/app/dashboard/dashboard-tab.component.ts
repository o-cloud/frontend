import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CatalogueSearchResult, ProviderType } from '../@core/models/catalog.models';
import { Identity } from '../@core/models/identity.model';
import { News } from '../@core/models/news.model';
import { CatalogueService } from '../@core/services/catalogue.service';
import { FavoriteService } from '../@core/services/favorite.service';

@Component({
  selector: 'app-dashboard-tab-component',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})

export class DashboardTabComponent implements OnInit {
  news: News[] = [];
  identity: Identity = {
    name: '',
    url: '',
    description: '',
    categories: [''],
    network: '',
    swarm: {
      network: '',
      name: '',
    },
  };
  searchResults: CatalogueSearchResult[] = [];
  subscriptions = new Subscription();
  query = '';
  providerType = ProviderType.Data;

  constructor(
    private router: Router,
    private catalogueService: CatalogueService,
    private favoriteService: FavoriteService,
  ) {}

  ngOnInit(): void {
    this.search();
  }


  search(): void {
    this.searchResults = [];
    this.subscriptions.add(
      this.catalogueService.search(this.query, this.providerType).subscribe((res) => {
        this.searchResults = [...this.searchResults, ...res];
      })
    );
  }

  removeFavorite(item: CatalogueSearchResult): void {
    this.favoriteService.deleteFavorite(item.providerId).subscribe(() => (item.isFavorite = false));
  }

  addFavorite(item: CatalogueSearchResult): void {
    this.favoriteService.addFavorite(item).subscribe(() => (item.isFavorite = true));
  }
}
