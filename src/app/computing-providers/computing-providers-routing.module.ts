import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ComputingProvidersComponent } from './computing-providers.component';

const routes: Routes = [
  {
    path: 'computing-providers',
    component: ComputingProvidersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComputingProvidersRoutingModule {}
