import { Component, OnInit } from '@angular/core';
import { CatalogueService } from '../@core/services/catalogue.service';
import { LocalDataSource } from 'ng2-smart-table';
import { NbDialogService } from '@nebular/theme';
import { ProviderType } from '../@core/models/catalog.models';
import { ResourcesComponent } from './resources.components';

@Component({
  selector: 'app-computing-providers',
  templateUrl: './computing-providers.component.html',
  styleUrls: ['./computing-providers.component.scss'],
})
export class ComputingProvidersComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();

  settings = {
    noDataMessage: 'No provider found...',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Name',
        width: '300px',
      },
      version: {
        title: 'Version',
        width: '100px',
      },
      description: {
        title: 'Description',
      },
      metadata: {
        title: 'Resources',
        width: '250px',
        type: 'custom',
        renderComponent: ResourcesComponent,
      },
    },
  };

  constructor(private catalogueService: CatalogueService, protected dialogService: NbDialogService) {}

  ngOnInit(): void {
    this.catalogueService.getLocalProviders(ProviderType.Computing).subscribe((data) => {
      this.source.load(data);
    });
  }
}
