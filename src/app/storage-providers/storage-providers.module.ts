import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageProvidersComponent } from './storage-providers.component';
import { StorageProvidersRoutingModule } from './storage-providers-routing.module';
import { NbCardModule, NbIconModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SharedModule } from '../@shared/shared.module';



@NgModule({
  declarations: [StorageProvidersComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule,
    StorageProvidersRoutingModule,
    NbIconModule,
    SharedModule,
  ]
})
export class StorageProvidersModule { }
