import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbButtonModule, NbCalendarRangeModule, NbCardModule, NbDatepickerModule, NbIconModule, NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {PricingChartComponent} from './prichingchart.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../@core/core.module';
import { SharedModule } from '../@shared/shared.module';
import { PricingComponent } from './pricing.component';
import { PricingRoutingModule } from './pricing-routing.module';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
  imports: [
    NbSelectModule,
    PricingRoutingModule,
    NbCardModule,
    CommonModule,
    Ng2SmartTableModule,
    NgxChartsModule,
    FormsModule,
    CoreModule,
    SharedModule,
    NbButtonModule,
    NbIconModule,
    NbDatepickerModule,
    NbCalendarRangeModule
  
  ],
  declarations: [PricingComponent, PricingChartComponent],
  exports: [],
})
export class PricingModule {}
