import { AfterViewInit, Component, ElementRef, HostListener, Input, ViewChild } from '@angular/core';
import { ScaleType } from '@swimlane/ngx-charts';
import * as _ from 'lodash';

@Component({
  selector: 'app-chart-component',
  template: `
    <div style="width: 100%; height: 100%;" #chartcontainer>
      <ngx-charts-line-chart
        [view]="lineChartView"
        [scheme]="colorScheme"
        [results]="data"
        [gradient]="lineChartGradient"
        [xAxis]="lineChartShowXAxis"
        [yAxis]="lineChartShowYAxis"
        [legend]="lineChartShowLegend"
        [showXAxisLabel]="lineChartShowXAxisLabel"
        [showYAxisLabel]="lineChartShowYAxisLabel"
        [xAxisLabel]="lineChartXAxisLabel"
        [yAxisLabel]="lineChartYAxisLabel"
      >
      </ngx-charts-line-chart>
    </div>
  `,
})
export class PricingChartComponent implements AfterViewInit {
  @Input() data: any;
  @ViewChild('chartcontainer') chartcontainer!: ElementRef<HTMLElement>;
  lineChartShowXAxis = true;
  lineChartShowYAxis = true;
  lineChartGradient = false;
  lineChartShowLegend = false;
  lineChartShowXAxisLabel = true;
  lineChartXAxisLabel = 'TIME';
  lineChartShowYAxisLabel = true;
  lineChartYAxisLabel = 'Consumation';
  colorScheme = {
    name: 'test',
    selectable: true,
    group: ScaleType.Linear,
    domain: ['#0095ff', '#ff708d', '#00d68f', '#3366ff'],
  };
  lineChartAutoScale = true;
  lineChartView: [number, number] = [600, 600];

  ngAfterViewInit(): void {
    setTimeout(this.resizeChart, 10);
  }

  resizeChart = (): void => {
    const boundig = this.chartcontainer.nativeElement.getBoundingClientRect();
    this.lineChartView = [boundig.width, boundig.height];
  };

  // tslint:disable
  resizecharttrottle = _.throttle(this.resizeChart, 500);

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.resizecharttrottle();
  }
}
