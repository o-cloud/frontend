
import { TablePricing } from "../@core/models/pricing.model";

export enum AggregationType {
  Namespace = 'Namespace',
  Controller = 'Controller',
  Deployment = 'Deployment',
  Service = 'Service',
  Pod = 'Pod',
  Container = 'Container',
}

export enum TimeType {
  Today = 'Today',
  Yesterday = 'Yesterday',
  Week = 'Week',
  Monthh = 'Month',
  Lastweek = 'Lastweek',
  Lastmonth = 'Lastmonth',
}

export enum MetricType {
  Cpusage = 'cpu_usage',
  CpuRequest = 'cpu_request',
  Networkreceive = 'network_transmit',
  Networktransmit = 'network_receveid',
  Memoryusagetotal = 'memory_usage',
}

export enum StepType{
  Minute= "Minute",
  Heure="Heure",
  Day="Day",
  Mois = "Mois"
}
