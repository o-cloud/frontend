import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CreatePricing, EditPricing, Pricing, PricingResponse, TablePricing } from '../@core/models/pricing.model';


@Injectable({
  providedIn: 'root',
})
export class PricingService {
  constructor(private http: HttpClient) {}

  private REST_API_SERVER = environment.urlPricing;

  public getPricing(httpHeaders: HttpHeaders): Observable<any> {
    return this.http.get<PricingResponse>(`${this.REST_API_SERVER}/rates/resources/MyResources`, { headers: httpHeaders });
  }

  //modifie call to pricing and add body list clusters
  public getPricingRemote(): Observable<any> {
    return this.http.get<PricingResponse>(`${this.REST_API_SERVER}/rates/resources/RemoteResources`);
  }

  public createPricingResource(newRow: CreatePricing, queryParams:HttpParams): Observable<any>{
    return this.http.post<PricingResponse>(`${this.REST_API_SERVER}/rates/resources/MyResources`, newRow, { params: queryParams });
  }

  public updatePricingResource(updateRow : EditPricing , idprice: string, queryParams:HttpParams): Observable<any>{
    return this.http.put<PricingResponse>(`${this.REST_API_SERVER}/rates/resources/MyResources/${idprice}`,updateRow, { params: queryParams });
  }

  public deletePricingResource(idprice: string, queryParams:HttpParams): Observable<any>{
    return this.http.delete<PricingResponse>(`${this.REST_API_SERVER}/rates/resources/MyResources/${idprice}`, { params: queryParams });
  }

}
