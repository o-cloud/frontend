
import { HttpHeaders, HttpParams} from '@angular/common/http';
import { TmplAstBoundAttribute } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { update } from 'lodash';
import { Subscription } from 'rxjs';
import { CreatePricing, EditPricing, Pricing, PricingRemote, TablePricing } from '../@core/models/pricing.model';
import { TabsController } from '../@shared/tabs/tabs-controller';
import { PricingService } from './pricing-service';


type TabName = 'My Resources' | 'Remote Resources';
const allTabs: TabName[] = ['My Resources', 'Remote Resources'];
function isValidTab(name: string | null): name is TabName {
  if (!name) {
    return false;
  }
  return allTabs.includes(name as TabName);
}

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss'],
})
export class PricingComponent implements OnInit {
 
  allTabs = allTabs;
  selectedTab?: TabName;
 
  subscription = new Subscription();
  tabController: TabsController<TabName>;
  rows : TablePricing[]= [];
  rowsRemote: TablePricing[]= [];

  settings = {
    mode: 'inline',
    noDataMessage: 'No data found',
    columns: {
      name: {
        title: 'Name',
        editable: true,
      },
      type: {
        title: 'Type',
        editable: true,
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'resources', title: 'Resources' },
              { value: 'data', title: 'Data' },
              { value: 'service', title: 'Service' },
            ],

          },
        },
        },
      value: {
        title: 'Value',
        editable: true,
      }
    },
    actions: {
      add: true,
      edit: true,
      delete: true,
      positon: 'right',
      columnTitle: 'Actions',
      custom: []
    },
    hideSubHeader: false,
    edit: {
      inputClass: '',
      editButtonContent: '<img src="/assets/images/icons/edit-outline.svg" width="20" height="20" >',
      saveButtonContent: '<img src="/assets/images/icons/checkmark-circle-2-outline.svg" width="20" height="20" >',
      cancelButtonContent: '<img src="/assets/images/icons/close-square-outline.svg" width="20" height="20" >',
      confirmSave: true,
    },
    add: {
      inputClass: '',
      addButtonContent: '<img src="/assets/images/icons/plus-outline.svg" width="20" height="20" >',
      createButtonContent: '<img src="/assets/images/icons/checkmark-circle-2-outline.svg" width="20" height="20" >',
      cancelButtonContent: '<img src="/assets/images/icons/close-square-outline.svg" width="20" height="20" >',
      confirmCreate: true,
    },
    delete: {
      deleteButtonContent: '<img src="/assets/images/icons/trash-outline.svg" width="20" height="20" >',
      confirmDelete: true,
    },
    attr: {
      id: '',
      class: '',
    },
  };


  settingsRemote = {
    mode: 'external',
    noDataMessage: 'No data found',
    columns: {
      name: {
        title: 'Name',
      },
      type: {
        title: 'Type',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'computing', title: 'Computing'},
              { value: 'data', title: 'Data' },
              { value: 'service', title: 'Service' },
              { value: 'storage', title: 'Storage'}
            ],

          },
        },
        },
      value: {
        title: 'Value'
      },
      idpartenaire: {
        title: 'Origin Cluster Name'
      }
    },
    actions: {
      add: false,
      edit: false,
      delete: false,
      positon: 'right',
      columnTitle: '',
    },
    hideSubHeader: false,
    attr: {
      id: '',
      class: '',
    },
  };
  multyRows: TablePricing[] = [];
  multyRowsRemote: TablePricing[] = [];

 
 

  constructor(private pricingService: PricingService, private router: Router, private route: ActivatedRoute) {
    this.tabController = new TabsController<TabName>(route, router, 'My Resources', isValidTab);
    this.subscription.add(this.tabController.currentTab().subscribe((t) => (this.selectedTab = t)));
  }

  ngOnInit(): void {
    let headersValue = new HttpHeaders()
    this.pricingService.getPricing(headersValue).subscribe((data)=> {
      buildRow(data, this.rows);
      this.multyRows= this.rows
      console.log("rows", this.rows)
    });
    this.pricingService.getPricingRemote().subscribe((data)=>{
      buildRowRemote(data, this.rowsRemote);
      this.multyRowsRemote=this.rowsRemote
      console.log("remoteRows",this.rowsRemote)
    });
  }

  create(event: { newData: TablePricing; confirm: { resolve: (arg0: any) => void; reject: () => void; }; }){
    let headersValue = new HttpParams()
    let newObject: TablePricing
    let rows : TablePricing[]= [];
    newObject = event.newData
    let createObject: CreatePricing={
        name: newObject.name,
        type: newObject.type,
        value: +newObject.value
    } 
   console.log("newEvent ", createObject)
   this.pricingService.createPricingResource(createObject, headersValue).subscribe((data)=>{
   console.log("data ", data)
   if (data){
   buildRow(data, rows)
   this.rows= rows
   this.multyRows= rows
   console.log("new rates", rows)
   event.confirm.resolve(event.newData)
  } else {
    event.confirm.reject();
  }
   })
  }

  edit(event: { data: any; newData: TablePricing; confirm: { resolve: (arg0: any) => void; }; }){
    console.log("edit", event)
    let headersValue = new HttpParams()
    let editObject: TablePricing
    let oldObject= event.data
    let tempsModel : EditPricing
    editObject= event.newData
    tempsModel= updatePricings( editObject);
    console.log("newEvent", tempsModel)
    this.pricingService.updatePricingResource(tempsModel,editObject.idprice, headersValue).subscribe((data)=>{
      oldObject.type=  editObject.type
      oldObject.name= editObject.name
      oldObject.value= editObject.value
      event.confirm.resolve(oldObject)
      console.log("rows update ", this.rows)
      console.log("result edit",data)
    })
   
  }
  delete(event: { data: TablePricing; confirm: { resolve: (arg0: any) => void; }; source: { data: any; }; }){
    console.log("deleteEvent ", event)
    let headersValue = new HttpParams()
    let deleteObject: TablePricing
    deleteObject= event.data
    console.log("deleteObject ", deleteObject)
     this.pricingService.deletePricingResource(deleteObject.idprice, headersValue).subscribe((data)=>{
      console.log("delete result: ",data )
      this.rows = deleteRow(this.rows, deleteObject)
      this.multyRows= this.rows
      event.confirm.resolve(event.source.data)
    })   
  }
}

function buildRow(data: any, rows: TablePricing[]){
for (const priceValue of data) {
  let priceVar: Pricing= {
    name:  priceValue["name"],
    type: priceValue["type"],
    value: priceValue["value"],
    idprice: priceValue["_id"]
  }
 addRows(rows, priceVar.name, priceVar.type, priceVar.value, priceVar.idprice, "idpartnaire")
  }
}

function addRows(rows: TablePricing[], name: string, type: string,  value: number, idprice: string, idpartenaire: string) : TablePricing[]{
  let row : TablePricing= {
    idpartenaire: idpartenaire,
    idprice: idprice,
    type: type,
    name: name,
    value: value
  }
  rows.push(row); 
return rows
}

function updatePricings( editObject: TablePricing): EditPricing {
  let tempsModel : EditPricing ={
   name: editObject.name,
   type : editObject.type,
   value: +editObject.value
  }
 return tempsModel;
}

function deleteRow(rows: TablePricing[], deleteObject: TablePricing) : TablePricing[] {
  let index = rows.findIndex(tmp=> tmp.idprice=== deleteObject.idprice)
  console.log("index delete: ", index)
  if (index!==-1){
    rows.splice(index,1)
  }
  console.log("rows after delete ", rows)
  return rows
}

function buildRowRemote(data: any, rowsRemote: TablePricing[]) {
  for (const priceValue of data) {
    let priceVar: PricingRemote= {
        name: priceValue["rates"]["name"],
        value: priceValue["rates"]["value"],
        type: priceValue["rates"]["type"],
        idprice: priceValue["_id"],
        origin: priceValue["originClusterName"]
    }
 addRows(rowsRemote, priceVar.name, priceVar.type, priceVar.value, priceVar.idprice, priceVar.origin)
  }
}
