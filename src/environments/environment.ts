// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const ip = location.host
export const environment = {
  production: false,
  urlJsonServer: `http://${ip}/api/generic-backend`,
  webSocketSearchCatalog: `ws://${ip}/api/generic-backend/catalog/data/search`,
  urlCatalog: `http://${ip}/api/catalog/public`,
  urlServiceProvider: `http://${ip}/api/service-provider`,
  urlDiscovery: `http://${ip}/api/discovery`,
  urlPipelineOrchestrator: `http://${ip}/api/pipeline-orchestrator`,
  urlPricing: `http://${ip}/api/pricing-manager`,
}
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
