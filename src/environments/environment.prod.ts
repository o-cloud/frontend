export const environment = {
  production: true,
  urlJsonServer: `${location.protocol}//${location.host}/api/generic-backend`,
  webSocketSearchCatalog: `${location.protocol === 'https:' ? 'wss:' : 'ws:'}//${location.host}/api/generic-backend/catalog/data/search`,
  urlCatalog: `${location.protocol}//${location.host}/api/catalog/public`,
  urlServiceProvider: `${location.protocol}//${location.host}/api/service-provider`,
  urlDiscovery: `${location.protocol}//${location.host}/api/discovery`,
  urlPipelineOrchestrator: `${location.protocol}//${location.host}/api/pipeline-orchestrator`,
  urlPricing: `${location.protocol}//${location.host}/api/pricing-manager`
}
